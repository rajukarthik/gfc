# To Run:
# docker pull asheibani/gfc && docker stop gfc  && docker rm gfc  && docker run --name gfc -d -p 8180:8080 asheibani/gfc:latest && docker logs -f gfc

FROM java:7-jre

MAINTAINER Amir Sheibani "asheibani87@yahoo.com"

ADD build/unpacked/lib/ /app/lib/

ADD build/unpacked/jar/ /app/

CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-cp", "/app/", "org.springframework.boot.loader.JarLauncher"]