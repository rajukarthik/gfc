var gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('css', function () {
    gulp.src('main/resources/static/src/assets/stylesheets/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest('main/resources/static/public/css'))
});

gulp.task('watch', function() {
  gulp.watch('main/resources/static/src/assets/stylesheets/**/*', ['css']);
});

gulp.task('images', function() {
    gulp.src(['main/resources/static/src/assets/images/*'])
        .pipe(gulp.dest('main/resources/static/public/images'))
});

gulp.task('default', ['css', 'images', 'watch']);
