package com.gfc.controllers;

import com.GoFetchCodeApplication;
import com.gfc.models.domain.SearchHistory;
import com.gfc.repositories.SearchHistoryRepository;
import com.gfc.users.accounts.enums.PersonType;
import com.gfc.users.accounts.models.entities.LoginActivity;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.LoginActivityRepository;
import com.gfc.users.accounts.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by gurinder on 13/1/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@SpringApplicationConfiguration(classes = GoFetchCodeApplication.class)
public class SearchHistoryControllerTest {

    @Value("${local.server.port}")
    private int port;

    @Autowired
    LoginActivityRepository loginActivityRepository;

    @Autowired
    UserRepository personRepository;

    @Autowired
    SearchHistoryRepository searchHistoryRepository;

    private RestTemplate template;


    @Before
    public void setUp() {
        template = new TestRestTemplate();
    }

    @Test
    public void checkHistoryResult() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("AUTH-TOKEN", "123");

        User peopleEntity = new User();
        peopleEntity.setEmail("satan@satan.com");
        peopleEntity.setId(1);
        peopleEntity.setFirstName("master");
        peopleEntity.setPassword("123");
        peopleEntity.setType(PersonType.INDIVIDUAL);

        personRepository.save(peopleEntity);

        LoginActivity activity = new LoginActivity();
        activity.setId(1);
        activity.setAuthToken("123");
        activity.setPeople(peopleEntity);

        loginActivityRepository.save(activity);

        SearchHistory searchHistoryEntity = new SearchHistory();
        searchHistoryEntity.setPeople(peopleEntity);
        searchHistoryEntity.setId(1L);
        searchHistoryEntity.setQuestion("what is java?");

        searchHistoryRepository.save(searchHistoryEntity);

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        final ResponseEntity<List> response = template.exchange("http://localhost:" + port + "/api/history/question", HttpMethod.GET, entity, List.class);
        System.out.println(response);
    }
}
