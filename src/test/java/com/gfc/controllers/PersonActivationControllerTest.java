package com.gfc.controllers;

import com.GoFetchCodeApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

/**
 * Created by gurinder on 25/12/15.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@SpringApplicationConfiguration(classes = GoFetchCodeApplication.class)
public class PersonActivationControllerTest {

    @Value("${local.server.port}")
    private int port;

    private RestTemplate template;

//    @Autowired
//    GfcUserActivationRepository gfcUserActivationRepository;
//
//    @Autowired
//    GfcUserRepository gfcUserRepository;

    @Before
    public void setUp() {
        template = new TestRestTemplate();
//        gfcUserRepository.deleteAll();
//        gfcUserActivationRepository.deleteAll();
    }

    @Test
    public void testUserActivationController() throws Exception {
//        GfcUser gfcUser = new GfcUser();
//        gfcUser.setEmail("gurindersingh@gfc.com");
//        gfcUser.setFirstName("Gurinder");
//        gfcUser.setId(123);
//        gfcUserRepository.create(gfcUser);
//        String token = TokenGenerator.getToken();
//        GfcUserActivation gfcUserActivation = new GfcUserActivation(gfcUser, token, "pending");
//        gfcUserActivationRepository.create(gfcUserActivation);
//        final ResponseEntity<String> result = template.getForEntity("http://localhost:" + port + "/activation/confirm?token=123" , String.class);
//        System.out.println(result);
//        RegistrationDto registrationRequestDto = new RegistrationDto();
//        registrationRequestDto.setFirstName("Gurinder");
//        registrationRequestDto.setLastName("singh");
//        registrationRequestDto.setEmail("gsingh.2090@gmail.com");
//        registrationRequestDto.setPassword("");
//        registrationRequestDto.setType(SubscriptionType.INDIVIDUAL);
//        registrationRequestDto.setRepeatPassword("master");
//        HashMap hashMap = new HashMap();
//        final String s = template.postForObject("http://localhost:" + port + "/api/register/", registrationRequestDto, String.class, hashMap);
//        System.out.println(s);
////        template.getForEntity( , String.class);
    }


}