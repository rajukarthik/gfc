package com.gfc.controllers;

import com.gfc.users.accounts.controllers.RegistrationController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;

/**
 * Created by abhinav on 25/2/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class RegistrationControllerTest {

    @InjectMocks
    private RegistrationController controller;


    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testRegister() throws Exception {
       given().standaloneSetup(controller).when()
               .post("/api/register")
               .then()
               .statusCode(HttpStatus.CREATED.value());
    }
}