ALTER TABLE gfc_clarification DROP selected_text;
ALTER TABLE gfc_clarification ADD COLUMN section_sequence_id varchar(100) DEFAULT NULL;
ALTER TABLE gfc_clarification ADD COLUMN section_sequence_title varchar(100) DEFAULT NULL;
ALTER TABLE gfc_clarification ADD COLUMN guest_email VARCHAR(300);
ALTER TABLE gfc_clarification ADD COLUMN guest_token varchar(255) DEFAULT NULL;

ALTER TABLE gfc_clarification_chat ADD COLUMN is_read BOOLEAN NOT NULL;

RENAME TABLE gfc_clarification_chat TO gfc_clarification_chat_history;

DROP TABLE IF EXISTS `gfc_clarification_guest_user`;
