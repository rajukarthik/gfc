update gfc_people set subscription_type =
case
  when type = 'INDIVIDUAL' then 'INDIVIDUAL'
  when type = 'TEAM_MEMBER' then 'TEAM_ADMIN'
  when type = 'ORGANISATION_OWNER' then 'ORGANISATION'
  else subscription_type end;

update gfc_people set type = 'TEAM_ADMIN' where type = 'TEAM_MEMBER';