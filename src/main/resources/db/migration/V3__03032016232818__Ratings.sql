DROP TABLE IF EXISTS gfc_ratings;

CREATE TABLE gfc_ratings(
id INT(11) NOT NULL AUTO_INCREMENT,
value INT(5) NOT NULL,
question VARCHAR(4096) DEFAULT NULL,
section_id VARCHAR(100) NOT NULL,
user_id INT(11) NOT NULL,
PRIMARY KEY (`id`),
FOREIGN KEY (`user_id`) REFERENCES gfc_people(`id`)
);
