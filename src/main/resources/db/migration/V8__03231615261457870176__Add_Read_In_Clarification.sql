ALTER TABLE gfc_clarification ADD COLUMN is_read boolean DEFAULT false;
ALTER TABLE gfc_clarification_chat_history DROP COLUMN is_read;