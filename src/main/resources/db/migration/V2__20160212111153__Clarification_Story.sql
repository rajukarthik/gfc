DROP TABLE IF EXISTS `gfc_clarification`;

CREATE TABLE gfc_clarification(
id BIGINT(20) NOT NULL AUTO_INCREMENT,
selected_text longtext,
status VARCHAR(100),
people_id int(11) NOT NULL,
created_by VARCHAR(100),
created_dt DATETIME,
updated_by VARCHAR(100),
updated_dt DATETIME,
PRIMARY KEY (id),
FOREIGN KEY (`people_id`) REFERENCES gfc_people(`id`)
);

DROP TABLE IF EXISTS `gfc_clarification_chat`;

CREATE TABLE gfc_clarification_chat(
id BIGINT(20) NOT NULL AUTO_INCREMENT,
comment varchar(4096) DEFAULT NULL,
user_Name VARCHAR(100),
clarification_id BIGINT(20) NOT NULL,
created_by VARCHAR(100),
created_dt DATETIME,
updated_by VARCHAR(100),
updated_dt DATETIME,
PRIMARY KEY (id),
FOREIGN KEY (`clarification_id`) REFERENCES gfc_clarification(`id`)
);

DROP TABLE IF EXISTS `gfc_clarification_guest_user`;

CREATE TABLE gfc_clarification_guest_user(
id BIGINT(20) NOT NULL AUTO_INCREMENT,
status VARCHAR(100),
guest_email VARCHAR(100),
guest_token VARCHAR(100),
clarification_id BIGINT(20) NOT NULL,
created_by VARCHAR(100),
created_dt DATETIME,
updated_by VARCHAR(100),
updated_dt DATETIME,
PRIMARY KEY (id),
FOREIGN KEY (`clarification_id`) REFERENCES gfc_clarification(`id`)
);