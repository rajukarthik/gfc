angular.module('gfc-service-user', []).service('UserService', function($http, $q) {

    function doGet(dataCallback, errorCallBack, url, param1, param2) {
        if(param1) url += '/' + param1
        if(param2) url += '/' + param2
        $http.get(url).
            success(function(data, status, headers, config) {
                dataCallback(data);
            }).
            error(function(data, status, headers, config) {
                var message = data.message ? data.message : 'Could not GET ' + url + ': ' + status;
                console.error(JSON.stringify(data));
                errorCallBack(message, data, status, headers, config)
            });
    }

    function doPost(dataCallback, errorCallBack, url, data) {
        $http.post(url, data, { headers : {'Content-Type' : 'application/json' } }).
            success(function(data, status, headers, config) {
                dataCallback(data);
            }).
            error(function(data, status, headers, config) {
                var message = data.message ? data.message : 'Could not POST ' + url + ': ' + status;
                console.error(JSON.stringify(data));
                errorCallBack(message, data, status, headers, config)
            });
    }


    var getCurrent = function(dataCallback, errorCallBack) {
        doGet(dataCallback, errorCallBack, '/secure/user/current');
    };

    var list = function(active, dataCallback, errorCallBack) {
        doGet(dataCallback, errorCallBack, '/secure/user/list?active=' + active);
    };

    var findById = function(id, dataCallback, errorCallBack) {
        doGet(dataCallback, errorCallBack, '/secure/user/fetch', id);
    };

    var update = function(user, dataCallback, errorCallBack) {
        doPost(dataCallback, errorCallBack, '/secure/user/update', user);
    };

    return {
        getCurrent: getCurrent,
        list: list,
        findById: findById,
        update: update
    };
});
