angular.module('gfc-service-response-summary', []).service('ResponseSummaryService', function($http, $q) {

    function doGet(dataCallback, errorCallBack, url, param1, param2) {
        if(param1) url += '/' + param1
        if(param2) url += '/' + param2
        $http.get(url).
        success(function(data, status, headers, config) {
            dataCallback(data);
        }).
        error(function(data, status, headers, config) {
            var message = data.message ? data.message : 'Could not GET ' + url + ': ' + status;
            console.error(JSON.stringify(data));
            errorCallBack(message, data, status, headers, config)
        });
    }

    function doPost(dataCallback, errorCallBack, url, data) {
        $http.post(url, data, { headers : {'Content-Type' : 'application/json' } }).
        success(function(data, status, headers, config) {
            dataCallback(data);
        }).
        error(function(data, status, headers, config) {
            var message = data.message ? data.message : 'Could not POST ' + url + ': ' + status;
            console.error(JSON.stringify(data));
            errorCallBack(message, data, status, headers, config)
        });
    }


    var searchResonseSummaries = function(startTime, endTime, index, reviewed, ratingUpTo, dataCallback, errorCallBack) {
        var url = '/watson/search?startTime=' + startTime + '&endTime=' + endTime + '&index=' + index;
        if(reviewed != null && reviewed != undefined) url += '&reviewed=' + reviewed;
        if(ratingUpTo != null && ratingUpTo != undefined) url += '&ratingUpTo=' + ratingUpTo;

        doGet(dataCallback, errorCallBack, url);
    };

    var getResponseSummaries = function(watsonInteractionId, dataCallback, errorCallBack) {
        var url = '/watson/response-summary/' + watsonInteractionId;
        doGet(dataCallback, errorCallBack, url);
    };

    var setReviewed = function(watsonInteractionId, reviewed, dataCallback, errorCallBack) {
        var url = '/watson/response-summary/reviewed/' + watsonInteractionId + '?reviewed=' + reviewed;
        doPost(dataCallback, errorCallBack, url);
    };

    return {
        searchResonseSummaries: searchResonseSummaries,
        getResponseSummaries: getResponseSummaries,
        setReviewed: setReviewed
    };
});
