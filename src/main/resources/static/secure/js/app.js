var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'ui.grid', 'ui.grid.selection', 'ui.grid.autoResize', 'jsonFormatter', 'validation.match', 'gfc-service-user', 'gfc-service-response-summary']);

gfcApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                title: 'GoFetchCode | Home',
                templateUrl: '/secure/partials/home.html',
                controller: 'mainController'
            }).
            when('/users', {
                title: 'GoFetchCode | Users',
                templateUrl: '/secure/partials/users.html',
                controller: 'usersController'
            }).
            when('/user/:id', {
                title: 'GoFetchCode | User',
                templateUrl: '/secure/partials/user.html',
                controller: 'userController'
            }).
            when('/user', {
                title: 'GoFetchCode | User',
                templateUrl: '/secure/partials/user.html',
                controller: 'userController'
            }).
            when('/search', {
                title: 'GoFetchCode | Search',
                templateUrl: '/secure/partials/search.html',
                controller: 'searchController'
            }).
            otherwise({
                redirectTo: '/'
            });
    }
]);

gfcApp.controller('mainController', ['$scope', '$rootScope', '$location', '$modal', '$interval', '$timeout', '$log', 'UserService',  function($scope, $rootScope, $location, $modal, $interval, $timeout,  $log, UserService) {


    $scope.activePath = null;

    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxPerPage = 10;
    $scope.maxPerPageTemp = $scope.maxPerPage;
    $scope.table = {};
    $scope.table.sort = {column: null, ascending: true};

    $scope.$on('$routeChangeSuccess', function(event, current, previous){
        $scope.activePath = $location.path();
        console.log("Path: " +  $scope.activePath);
        $rootScope.title = current.$$route.title;
        $rootScope.templateUrl = current.$$route.templateUrl;
    });

    $rootScope.fetchCurrentUser =  function(callback) {
        if($rootScope.currentUser) {
            if(callback) callback($rootScope.currentUser)
        } else {
            UserService.getCurrent(function (data) {
                $rootScope.currentUser = data;
                if (callback) callback($rootScope.currentUser);
            });
        }
    };

    $rootScope.fetchCurrentUser();

    $rootScope.handleError =  function handleError(message) {
        $log.log('handleError: ' + message);
        $rootScope.errorMessage = message;
    };

}]);

angular.module('gfcApp').filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});

gfcApp.filter('encodeURIComponent', function() {
    return window.encodeURIComponent;
});

gfcApp.config(['$compileProvider',
    function ($compileProvider) {
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|local|data):/);
    }]);