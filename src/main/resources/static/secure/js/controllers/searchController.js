gfcApp.controller('searchController', ['$scope', '$routeParams', '$location', '$log', '$window', '$modal', 'ResponseSummaryService', function ($scope, $routeParams, $location, $log, $window, $modal, ResponseSummaryService) {

    $scope.index = 0;

    $scope.reviewedList = [
        {id: null, name: 'Any Reviewed'},
        {id: true, name: 'Reviewed'} ,
        {id: false, name: 'Not Reviewed'}
    ];

    $scope.ratingList = [
        {id: null, name: 'Any Rating'},
        {id: 1, name: 'Rating up to 1'},
        {id: 2, name: 'Rating up to 2'},
        {id: 3, name: 'Rating up to 3'},
        {id: 4, name: 'Rating up to 4'},
        {id: 5, name: 'Rating up to 5'}
    ];

    $scope.onSubmit = function() {
        ResponseSummaryService.searchResonseSummaries($scope.startTime,
          $scope.endTime, $scope.index, $scope.reviewed, $scope.rating,
            function(data) {
                $scope.data = data;
            }, $scope.handleError);
    };

    $scope.openedStartTime = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.isStartTimeOpen = true;
        $scope.isEndTimeOpen = false;
    };

    $scope.openedEndTime = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.isEndTimeOpen = true;
        $scope.isStartTimeOpen = false;
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.nextPage = function() {
        $scope.onSubmit(false, $scope.index++);
    };

    $scope.previousPage = function() {
        $scope.onSubmit(false, $scope.index--);
    };

    $scope.cancel = function() {
        $location.path('/searchs');
    };

    $scope.viewJSON = function (json) {

        var modalInstance = $modal.open({
            templateUrl: '/secure/partials/json.html',
            controller: 'jsonController',
            resolve: {
                json: function () {
                    return json;
                }
            }
        });

        modalInstance.result.then(function () {
            $log.info('Modal finished at: ' + new Date());
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.viewResponse = function (watsonInteraction) {
        $scope.viewJSON(JSON.parse(watsonInteraction.response));
    };

    $scope.viewSummary = function (watsonInteraction) {
        ResponseSummaryService.getResponseSummaries(watsonInteraction.id,
            function(data) {
                $scope.viewJSON(data);
            }, $scope.handleError);
    };

    $scope.setReviewed = function (watsonInteraction) {
        ResponseSummaryService.setReviewed(watsonInteraction.id, watsonInteraction.reviewed,
            function() {
                $scope.message = "Record saved."
                setTimeout(function() {
                    $scope.message = null;
                }, 5000);
            }, $scope.handleError);
    };
}]);