gfcApp.controller('userController', ['$scope', '$routeParams', '$location', '$log', '$window', 'UserService', function ($scope, $routeParams, $location, $log, $window, UserService) {

    $scope.user = { active: true };

    if($routeParams.id) {
        UserService.findById($routeParams.id, function (data) {
            console.log("##### USER: " + JSON.stringify(data))
            $scope.user = data;
        }, $scope.handleError);
    }

    $scope.update = function(user) {
        UserService.update(user, function (data) {
            $log.log('User saved');
            $scope.user = data;
            $scope.message = 'Successfully Saved'
            $window.scrollTo(0, 0);
        }, function(message) {
            $scope.handleError(message);
            $window.scrollTo(0, 0);
        });
    };



    $scope.cancel = function() {
        $location.path('/users');
    };

}]);