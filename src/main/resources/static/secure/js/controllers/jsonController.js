gfcApp.controller('jsonController', ['$scope', '$routeParams', '$location', '$log', '$filter', '$modalInstance', 'json', function ($scope, $routeParams, $location, $log, $filter, $modalInstance, json) {

    $scope.cancel = function() {
        $modalInstance.close();
    };

    $scope.json = json;
}]);