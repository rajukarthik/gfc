gfcApp.controller('usersController', ['$scope', '$routeParams', '$location', '$window', '$log', 'UserService', function ($scope, $routeParams, $location, $window, $log, UserService) {

    $scope.gridOptions = {
        enableRowSelection: true,
        enableSelectAll: true
    };

    $scope.gridOptions =
        {
            enableFiltering: true,
            columnDefs: [
                {name: 'id', width: 60},
                {name: 'email'},
                {name: 'firstName'},
                {name: 'lastName'},
                {name: 'admin', width: 70, enableFiltering: false , cellTemplate: '<span class="glyphicon glyphicon-{{row.entity.admin ? \'check\' : \'unchecked\'}}"></span>'},
                {name: 'active', width: 70, enableFiltering: false , cellTemplate: '<span class="glyphicon glyphicon-{{row.entity.active ? \'check\' : \'unchecked\'}}"></span>'}
            ]
        }

            $scope.gridOptions.multiSelect = false;

    $scope.info = {};

    $scope.gridOptions.onRegisterApi = function(gridApi){
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
            if(row.isSelected) {
                $scope.user = row.entity;
            } else {
                $scope.user = null;
            }
        });
    };

    $scope.searchTypes = [
        {id: 1, description: 'Active', value: true},
        {id: 2, description: 'Inactive', value: false}
    ];

    $scope.searchType = $scope.searchTypes[0].id;

    $scope.listUsers = function() {
        UserService.list($scope.searchType == 1, function(data) {
            $scope.users = data;
            $scope.gridOptions.data = data;
        }, $scope.handleError);
    };

    $scope.getTableHeight = function() {
        var rowHeight = 30; // your row height
        var headerHeight = 80; // your header height
        return {
            height: ($scope.gridOptions.data.length * rowHeight + headerHeight) + "px"
        };
    };

    $scope.listUsers();

    $scope.onAddUser = function() {
        $location.path('/user/');
    };

    $scope.onEditUser = function() {
        $location.path('/user/' + $scope.user.id);
    };
}]);