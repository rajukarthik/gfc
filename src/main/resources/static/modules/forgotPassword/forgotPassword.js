gfcApp.controller('forgotPasswordController',['$scope','$log','$rootScope','userService', function($scope, $log, $rootScope, userService){

	$rootScope.invalidCredentials = false ;
	$rootScope.invalidUsername = false ;
	$rootScope.pendingUsername = false ;
	$rootScope.resetPasswdLinkSent = false ;
	$rootScope.resetPasswdResponse = false;
	$scope.resetEmail = "" ;
    $scope.formSubmitted = false ;
	$rootScope.errorMessage = "" ;

	$scope.getResetPasswordLink = function(resetEmail){
		$log.log("getResetPasswordLink..") ;
		
		$log.log("Entered Email :" + resetEmail + ":") ;
        $scope.formSubmitted = true ;
        $rootScope.resetPasswdResponse = false;
		userService.getResetPasswordLink("/api/people/resetPassword/sendResetPasswordEmail", resetEmail, successCallback, errorCallback) ;

		function successCallback(response){
                $log.log('getResetPasswordLink Success..') ;
                $log.log(response) ;
                $rootScope.dataLoading = false ;
                $rootScope.resetPasswdResponse = true;
                if(response.data.status.toUpperCase() == "SUCCESS"){
                    $rootScope.changePath = true ;
                    $rootScope.resetPasswdLinkSent = true ;
                    $rootScope.errorMessage = response.data.message ;
                    $scope.resetEmail = "";
                    $scope.form.$setPristine();
                }
                else if(response.data.status.toUpperCase() == "EMAIL_NOT_REGISTERED"){
                	$rootScope.resetPasswdLinkSent = false ;
                	$rootScope.errorMessage = response.data.message ;
                }

        }
        function errorCallback(response){
                 $log.error("getResetPasswordLink error..") ;
                 $rootScope.dataLoading = false ;
                 $rootScope.resetPasswdResponse = true;
                 $log.error(response) ;
        }
	};

    $(function () {
        $('[data-toggle="popover"]').popover();
    })
}]) ;	