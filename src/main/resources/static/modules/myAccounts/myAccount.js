gfcApp.controller('myAccountsController',['$scope', '$location','$log','$rootScope', '$cookies', 'userService', 'accountService', 'md5', function($scope, $location, $log, $rootScope, $cookies, userService, accountService, md5){

	$rootScope.invalidCredentials = false ;	
	$rootScope.invalidUsername = false ;
	$rootScope.pendingUsername = false ;
	$rootScope.isInternalError = false ;
        
	var token = "" ;

   /* $scope.myfunc = function(){
        console.log("myfunc");
    }*/

    function initChangePasswdVars(){
        $rootScope.resetPasswdLinkSent = false ;
        $scope.formSubmitted = false ;
        $scope.user = {curPasswd:"", newPasswd:"", confirmPasswd:""} ;
        $scope.isPasswordCorrect = true ;

        if(location.href.indexOf('token') != -1){
            $scope.passwordReset = true ;
            token = location.href.split("?token=")[1] ;
        }else{
            $scope.passwordReset = false ;
        }
         //call api to check if entered password if correct or not
        $scope.checkPassword = function() {            
            var userId = $rootScope.userDetails.id ;
            $rootScope.dataLoading = true ;
            accountService.checkPassword("/api/secure/users/" + userId + "/accounts/password/"+ $scope.user.curPasswd + "/validate",successCallback, errorCallback)
            
            function successCallback(response){
                $rootScope.dataLoading = false ;
                if(response && response.data){
                    if(response.data.status.toUpperCase() === 'NOT_FOUND')
                        $scope.isPasswordCorrect = false ;
                    else if(response.data.status.toUpperCase() === 'SUCCESS')
                        $scope.isPasswordCorrect = true ;
                }
            }
            function errorCallback(response){
                $scope.isPasswordCorrect = false ;
                $rootScope.dataLoading = false ;
            }
        }
    }
    function initmyAccountVars(){
        if(location.href.indexOf('passwordUpdated') != -1 || location.href.indexOf('profileUpdated') != -1){
            $scope.isProfileUpdated = true ;
        }else{
            $rootScope.errorMessage = "" ;
            $scope.isProfileUpdated = false ;
        }
        $scope.user = {firstName: $rootScope.userDetails.firstName, lastName:$rootScope.userDetails.lastName, email:$rootScope.userDetails.email, password: md5.createHash("dummyvalue" || ''), userType:$rootScope.userDetails.subscriptionType} ;
    }
    function initeditAccountVars(){
        if(!$scope.user){
            $scope.user = {firstName: $rootScope.userDetails.firstName, lastName:$rootScope.userDetails.lastName, email:$rootScope.userDetails.email, password:md5.createHash("dummyvalue" || ''), userType:$rootScope.userDetails.subscriptionType} ;
        }
        $scope.isEmailExists = false ;
        $rootScope.isInternalError = false ;
        $scope.formSubmitted = false ;

               
        $scope.userTypeChanged = function(newType){
            $scope.user.userType =  newType ;
            $log.log($scope.user.userType) ;
        }
    }

    function init(){
        if($location.path() == '/changePassword'){
            $rootScope.errorMessage = "" ;
            initChangePasswdVars() ;
        }else if($location.path() == '/myAccount'){
            initmyAccountVars() ;
        }
        else if($location.path() == '/editAccount'){
            $rootScope.errorMessage = "" ;
            initeditAccountVars() ;            
        }
    }

    function resetPassword(){
        accountService.changePassword("/api/people/resetPassword?token=",token, $scope.user, successCallback, errorCallback);
        
        function successCallback(response){
                $log.log('changePassword Success..') ;
                $log.log(response) ;
                $rootScope.dataLoading = false ;
                
                if(response.data.status.toUpperCase() == "SUCCESS"){
                    $rootScope.changePath = true ;
                    $rootScope.resetPasswdLinkSent = true ;
                    $rootScope.errorMessage = response.data.message ;
                    //location.href = "#/login" ;
                }
                else if(response.data.status.toUpperCase() == "EMAIL_NOT_REGISTERED"){
                    $rootScope.resetPasswdLinkSent = false ;
                    $rootScope.errorMessage = response.data.message ;
                }

        }
        function errorCallback(response){
                 $log.error("changePassword error..") ;
                 $rootScope.dataLoading = false ;
                 $rootScope.resetPasswdLinkSent = false ;
                 $log.error(response) ;
        }
    }

    function updatePassword(){
        var obj = {
                    "id": $rootScope.userDetails.id,
                    "currentPassword": $scope.user.curPasswd,
                    "newPassword": $scope.user.newPasswd,
                    "repeatPassword": $scope.user.confirmPasswd
                } ;

        function successCallback(response){
            $rootScope.dataLoading = false ;
            if(response && response.data && response.data.status.toUpperCase() === 'SUCCESS'){
                $rootScope.errorMessage = response.data.message ;
                $rootScope.resetPasswdLinkSent = true ;
                $scope.passwordReset = false ;
                location.href = "#/myAccount?passwordUpdated" ;
            }
        }
        function errorCallback(response){
            $rootScope.dataLoading = false ;
            $rootScope.resetPasswdLinkSent = false ;
            $scope.passwordReset = false ;
        }
        accountService.updatePassword("/api/secure/users/accounts/password/update", obj, successCallback, errorCallback) ;
    }

	$scope.changePassword = function(){
		$log.log("changePassword..") ;
        $scope.formSubmitted = true ;
        $rootScope.dataLoading = true ;

        if($scope.passwordReset){
            resetPassword() ;
        }
        else{
            updatePassword() ;
        }
		
	}

    function createJSONRequest(){
        var obj = {} ;
        obj.changeSet = {} ;
        obj.id = $rootScope.userDetails.id ;
        obj.type = $rootScope.userDetails.subscriptionType ;
        obj.changeSet.type = $scope.user.userType ;

        if($scope.user.firstName != $rootScope.userDetails.firstName)
            obj.changeSet.firstName = $scope.user.firstName;

        if($scope.user.lastName != $rootScope.userDetails.lastName)
            obj.changeSet.lastName = $scope.user.lastName;

        return obj ;
    }

    function askForConfirmation(obj){
        if(obj.type == obj.changeSet.type || obj.type.toUpperCase() == 'INDIVIDUAL')
            return true ;
        if(obj.type.toUpperCase().indexOf("TEAM") != -1){
            msg = "Your team details and data are going to be deleted. Are you sure want to proceed ?" ;
        }
        if(obj.type.toUpperCase().indexOf("ORG") != -1){
            msg = "Your organization details and data are going to be deleted. Are you sure want to proceed ?" ;
        }
        if(confirm(msg))
            return true ;

        return false ;
    }

    $scope.updateAccount = function(){
        $log.log("updateAccount..") ;
        var requestData = createJSONRequest() ;
        
        if(askForConfirmation(requestData)){
            $rootScope.isInternalError = false ;
            $scope.formSubmitted = true ;

            function successCallback(response){
                    $log.log('updateAccount Success..') ;
                    $log.log(response) ;
                    $rootScope.dataLoading = false ;
                    
                    if(response.data.status.toUpperCase() == "ACTIVE"){
                        $rootScope.changePath = true ;
                        $rootScope.isInternalError = false ;
                        $rootScope.errorMessage = response.data.message ;
                        $rootScope.userDetails = response.data;   
                        
                        //set cookie for 30 minutes
                        var thirtyMinutesLater = new Date();
                        thirtyMinutesLater.setMinutes(thirtyMinutesLater.getMinutes() + 30);
                        var expireDate = new Date(thirtyMinutesLater);

                        var userData = JSON.parse($cookies.get('user')) ;
                        userData.userDetails = response.data;
                        $cookies.put('user',JSON.stringify(userData), {'expires': expireDate}) ;
                        location.href = "#/myAccount?profileUpdated" ;
                        //location.href = "#/login" ;
                    }
                    else if(response.data.status.toUpperCase() == "ALREADY_PRESENT"){
                        $rootScope.isInternalError = true ;
                        $rootScope.errorMessage = response.data.message ;
                    }

            }
            function errorCallback(response){
                     $log.error("updateAccount error..") ;
                     $rootScope.dataLoading = false ;
                     $rootScope.isInternalError = true ;
                     $log.error(response) ;
            }
            
            accountService.updateAccount("/api/secure/users/accounts/update", requestData, successCallback, errorCallback);
        }        
    };

    $(function () {
        $('[data-toggle="popover"]').popover();
    })

    init() ;
}]) ;	