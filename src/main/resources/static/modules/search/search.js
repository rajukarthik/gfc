if (!gfcApp) {
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}
gfcApp.controller('searchController', ['$scope', '$log', '$sce', '$http', '$rootScope', 'searchService', 'bookmarkService', '$location', '$state', 'userService', function ($scope, $log, $sce, $http, $rootScope, searchService, bookmarkService, $location, $state, userService) {
    var backupSearchText = "";
    var obj;
    $rootScope.searchText = localStorage.getItem("searchText");

    if ($rootScope.selectedResultIndex) {
        $scope.currentIndex = $rootScope.selectedResultIndex;
    }
    else
        $scope.currentIndex = 0;

    $scope.IsBookmarked = false;
    $scope.message = "We're sorry but there were no results! Please try rephrasing the question and asking again.";
    document.body.scrollTop = 0;

    $scope.searchResults = JSON.parse(localStorage.getItem("chapters"));

    $scope.chaptersFound = localStorage.getItem("chaptersFound") === "true";
    $scope.message = localStorage.getItem("message");

    if ($scope.searchResults !== null && !Utils().canToggle()) {
        $('#searchedChapter').load($scope.searchResults[$scope.currentIndex].hyperlink, $scope.loadChaptercallback);
    }

    $scope.showDescription = function (event, index) {
        $log.log("index: " + index);
        $scope.currentIndex = index;
        $('#highlightDiv').hide();

        //save section number and section name in rootScope so that we can use it in clarify functionality
        $rootScope.sectionNumber = $(event.currentTarget).attr("sectionNumAttr");
        $rootScope.sectionName = $(event.currentTarget).attr("sectionNameAttr");

        if ($(event.currentTarget).attr("data-bookmarkId") != undefined) {
            $scope.IsBookmarked = true;
        }
        else {
           // checkBookmark(event.currentTarget, index);
           checkBookmark() ;
        }
        if (Utils().canToggle())
          Utils().toggleChapterActive("searchResults", true) ; //for mobile screen
        $('#searchedChapter').load($scope.searchResults[index].hyperlink, $scope.loadChaptercallback);
    }
   
    $scope.searchQuery = function (event) {
        $rootScope.selectedResultIndex = null;
        searchQueryHelper(event);
    }

    function searchQueryHelper(event) {
        //localStorage.removeItem('chapters');
        $scope.toggleReadOnly = true;
        searchService.callsearchAPI('/api/secure/ask?question=', $rootScope.searchText, successCallback, errorCallback);

        function successCallback(response) {
            $log.log('Ask Query Success..');
            $log.log(response);
            $rootScope.dataLoading = false;
            if (response && response.statusText == "OK") {
                $scope.message = "We're sorry but there were no results! Please try rephrasing the question and asking again.";
                showSearchResults(event, response);
            } else {
                $scope.message = "Server Error!  Please try again later";
                localStorage.setItem("chaptersFound", false);
            }
            localStorage.setItem("message", $scope.message);
            localStorage.setItem("searchText", $rootScope.searchText);
            $state.go('searchResult', {}, {reload: true});
        }

        function errorCallback(response) {
            $log.error("Ask Query failure..");
            $rootScope.dataLoading = false;
            $scope.message = "Server Error!  Please try again later";

            $log.error(response);
            localStorage.setItem("chaptersFound", false);
            localStorage.setItem("message", $scope.message);
            localStorage.setItem("searchText", $rootScope.searchText);
            $state.go('searchResult', {}, {reload: true});
        }
    }

    // show description in right pane when use searched for some question.
    function showSearchResults(event, obj) {
        if (obj.data.responses && obj.data.responses.length > 0) {
            $scope.searchResults = obj.data.responses;//obj.response.docs;

             //save section number and section name in rootScope so that we can use it in clarify functionality
            $rootScope.sectionNumber = $scope.searchResults[$scope.currentIndex].sectionNumber ;
            $rootScope.sectionName = $scope.searchResults[$scope.currentIndex].sectionName ;

            localStorage.setItem("chapters", JSON.stringify($scope.searchResults));
            localStorage.setItem("chaptersFound", true);
            $scope.resultsFound = true;
            console.log("showSearchResults/$scope.currentIndex: " + $scope.currentIndex);
        }
        else {
            $scope.resultsFound = false;
            localStorage.setItem("chaptersFound", false);
        }
        backupSearchText = $rootScope.searchText;
    }

    $scope.toggleInputField = function (event) {
        if ((event.target.name == "search" && !$scope.toggleReadOnly) || event.target.id == "searchBtn")
            return;

        $log.log("toggleInputField..");
        $scope.toggleReadOnly = !$scope.toggleReadOnly;
        if ($scope.toggleReadOnly) {
            event.currentTarget.style.cursor = "pointer";
            $rootScope.searchText = backupSearchText;
        } else {
            event.currentTarget.style.cursor = "default";
            backupSearchText = $rootScope.searchText;
        }
    }

    $scope.loadChaptercallback = function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success") {

            Utils().scrollTo($scope.searchResults[$scope.currentIndex].sectionNumber);

            getHighlights();
            $scope.getRateByUserAndQuestionAndSection();            
        }
        if (statusTxt == "error")
            console.log("Error: " + xhr.status + ": " + xhr.statusText);
    }

    $scope.findElementByText = function (text) {
        var elem = $(":contains(" + text + ")")
            .filter(function () {
                return $(this).children().length === 0;
            })

        return elem;
    }

    //to check if search page is called from recent searches page.
    var checkIfCalledByRecent = function () {
        if ($rootScope.askedQuestion) {
            var event = {};
            event.currentTarget = document.getElementById("searchForm");
            $rootScope.searchText = $rootScope.askedQuestion;
            $scope.currentIndex = $rootScope.selectedResultIndex;
            searchQueryHelper(event);
            $rootScope.askedQuestion = null;
        }
    }

    $scope.showPopup = function (e) {
        var highlightDiv = $('#highlightDiv');
        highlightDiv.hide();
        $scope.selection = window.getSelection();
        if ($scope.selection.toString() != "") {
            //var e = window.event;
            highlightDiv.css({'top': e.pageY - 130, 'left': e.pageX - 100});
            highlightDiv.show();
        }
    }

    $scope.saveHighlight = function (selection) {
        $('#highlightDiv').hide();

        var data = {"chapter": $scope.searchResults[$scope.currentIndex].chapterNo};
        searchService.saveHighlight("/api/secure/highlight/add", selection, data, successCallback, errorCallback);

        function successCallback(response) {
            $rootScope.dataLoading = false;
            $log.log("saveHighlight success..");
            $log.log(response);
            if (response.data.status.toUpperCase() == "SUCCESS") {
                searchService.setSelectionBGColor($scope.selection, "yellow");
            } else {
                alert(response.data.message);
            }
        }

        function errorCallback(response) {
            $rootScope.dataLoading = false;
            $log.log("saveHighlight error..");
            $log.log(response);
            alert("Could not add bookmark. Please try again!");
        }
    }

    function getHighlights() {
        $log.log("getHighlights..");
        var chapterNo = $scope.searchResults[$scope.currentIndex].chapterNo;
        searchService.getHighlights("/api/secure/highlight/list?chapter=", chapterNo, successCallback, errorCallback);

        function successCallback(response) {
            $log.log("getHighlights success..");
            $log.log(response);

            if (response && response.data) {
                for (var i = 0; i < response.data.length; i++) {
                    searchService.recreateHighlight(document.getElementById("searchedChapter"), response.data[i]);
                }
                ;
            }
        }

        function errorCallback(response) {
            $log.log("getHighlights error..");
            $log.log(response);
        }
    }

    $scope.toggleBookmark = function () {
        $scope.IsBookmarked = !$scope.IsBookmarked;

        if ($scope.IsBookmarked) {
            var sectionID = $scope.searchResults[$scope.currentIndex].sectionNumber;
            var sectionTitle = $scope.searchResults[$scope.currentIndex].sectionName;


            bookmarkService.addBookmark('/api/secure/bookmark/add', sectionID, sectionTitle, addSuccessCallback, errorCallback);
        } else {
            var id = $("#searchResults").children('a').eq($scope.currentIndex).attr("data-bookmarkId");
            bookmarkService.removeBookmark('/api/secure/bookmark/remove/', id, removeSuccessCallback, errorCallback);
        }

        function addSuccessCallback(response) {
            $log.log('addBookmark Success..');
            $log.log(response);
            if (response.data.id == undefined) {
                $scope.IsBookmarked = false;
                alert("Failed to add Bookmark! Please try again!!")
            } else {
                $("#searchResults").children('a').eq($scope.currentIndex).attr("data-bookmarkId", response.data.id);
            }
        }

        function removeSuccessCallback(response) {
            if (response.data) {
                $scope.IsBookmarked = false;
                $("#searchResults").children('a').eq($scope.currentIndex).removeAttr("data-bookmarkId");
            } else {
                $scope.IsBookmarked = true;
                alert("Bookmark deletion failed! Please try again");
            }
        }

        function errorCallback(response) {
            $scope.IsBookmarked = false;
            if ($scope.IsBookmarked)
                alert("Failed to add Bookmark! Please try again!!")
            else
                alert("Failed to remove Bookmark! Please try again!!")
        }
    }

    //check if particular section is being bookmarked or not
    var checkBookmark = function () {
        var index = $scope.currentIndex ;
        var target = $('#searchResults').children('div')[$scope.currentIndex] ;
        var sectionID = $scope.searchResults[index].sectionNumber;
        bookmarkService.checkBookmarks("/api/secure/bookmark/check?sectionSequenceId=", sectionID, successCallback, errorCallback);

        function successCallback(response) {
            if (response.data) {
                var data = response.data;
                if (data.id != -1) {
                    $(target).attr("data-bookmarkId", data.id);
                    $scope.IsBookmarked = true;
                } else {
                    $scope.IsBookmarked = false;
                }
            }
        }

        function errorCallback(response) {
            $scope.IsBookmarked = false;
        }
    }

    $scope.rateSearchResponse = function (value) {

        var ratingData = {
            'user': {
                'id': $rootScope.userDetails.id
            },
            'value': value,
            'question': localStorage.getItem('searchText'),
            'sectionId': $scope.searchResults[$scope.currentIndex].sectionNumber
        };

        searchService.rateSearchResponse(ratingData, rateSuccessCallback);

        function rateSuccessCallback() {
            $scope.responseRating = value;
        };
    };

    $scope.getRateByUserAndQuestionAndSection = function () {

        var ratingData = {
            'userId': $rootScope.userDetails.id,
            'question': localStorage.getItem('searchText'),
            'sectionId': $scope.searchResults[$scope.currentIndex].sectionNumber
        };
        
        searchService.getRateByUserAndQuestionAndSection(ratingData, rateSuccessCallback);

        function rateSuccessCallback(response) {
            $scope.responseRating = response.data.value;
            $log.log(response);
        };
    };
    
    //check if section is bookmarked only after searchUI in left pane gets completed.
    $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
       // checkBookmark($("#searchResults").children('div').eq(0), 0);
       checkBookmark() ;
    });

    $scope.logoutUser = function (event) {
        console.log("logout called..");
        userService.logout();
    }
    $scope.toggleChapterActive = function(operation){
        Utils().toggleChapterActive("searchResults", operation) ;
    } 

    if (location.hash.indexOf("ask") != -1)
        checkIfCalledByRecent();

}]);

//this directive will call ngRepeatFinished function only after ng-repeat is finished(which is loading all the search results in left pane)
gfcApp.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});