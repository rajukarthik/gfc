var gfcApp = angular.module('gfcApp', ['ui.router', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies', 'angular-md5']);

var supportsStorage = typeof(Storage) !== 'undefined';

gfcApp.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            title: 'Home GoFetchCode',
            url: "/home",
            templateUrl: "modules/home/content.html",
            controller: 'homeCtrl',
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'home';
            }
        })

        .state('about', {
            title: 'About GoFetchCode',
            url: '/about',
            templateUrl: "partials/about.html",
            controller: 'homeCtrl',
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'about';
            }
        })

        .state('termsOfService', {
            title: 'About GoFetchCode',
            url: '/termsOfService',
            templateUrl: "partials/termsOfService.html",
            controller: 'homeCtrl',
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'about';
            }
        })

        .state('privacyPolicy', {
            title: 'About GoFetchCode',
            url: '/privacyPolicy',
            templateUrl: "partials/privacyPolicy.html",
            controller: 'homeCtrl',
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'about';
            }
        })
        
        .state('dashboard', {
            title: 'Dashboard GoFetchCode',
            url: '/dashboard',
            templateUrl: "modules/dashboard/content.html",
            controller: 'dashboardCtrl',
            resolve: {
                check: function ($location, $rootScope, userService) {
                    if (!userService.isAlreadyLoggedIn()) {
                        $rootScope.redirectURL = '/ask';
                        $location.path('/login');
                    }
                }
            },
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'dashboard';
            }
        })

        .state('login', {
            title: 'Login GoFetchCode',
            url: "/login",
            templateUrl: "modules/login/content.html",
            controller: 'loginCtrl',
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'login';
            }
        })

        .state('signup', {
            title: 'Sign Up GoFetchCode',
            url: "/signup",
            templateUrl: "modules/register/signUp.html",
            controller: 'registerCtrl',
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'signup';
            }
        })

         .state('sgup', {
                    title: 'Sign Up GoFetchCode',
                    url: "/sgup",
                    params: {
                        signUpType: null,
                    },
                    templateUrl: "modules/register/content.html",
                    controller: 'registerCtrl',
                    onEnter: function ($rootScope) {
                        $rootScope.activeLink = 'sgup';
                    }
                })
         .state('createTeamMember', {
                     title: 'Create User for Team',
                     url: "/createTeamMember",
                     templateUrl: "modules/team/createTeamMember.html",
                     controller: 'createTeamMemberCtrl',
                     onEnter: function ($rootScope) {
                        $rootScope.activeLink = 'createTeamMember';
                    }
         })

        .state('editTeamMember', {
                     title: 'Create User for Team',
                     url: "/editTeamMember",
                     templateUrl: "modules/team/editTeamMember.html",
                     controller: 'editTeamMemberCtrl',
                     onEnter: function ($rootScope) {
                        $rootScope.activeLink = 'createTeamMember';
                    }
         })
        .state('signupMessage', {
            url: "/signupMessage",
            templateUrl: "modules/register/regMessage.html"
        })

        .state('team', {
            title: 'Team',
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'team';
            }
        })

        .state('userManagement', {
            title: 'UserManagement',
            url: "/userManagement",
            templateUrl: "modules/team/content.html",
            controller: 'userManagementCtrl',
            onEnter: function ($rootScope) {
                 $rootScope.activeLink = 'team';
            }
        })
        .state('organization',{
                    title:'Organization',
                    onEnter:function($rootScope){
                        $rootScope.activeLink = 'organization';
                    }
        })
        .state('teamsInOrganization', {
                    title: 'Teams in Organization',
                    url: "/teamsInOrganization",
                    templateUrl: "modules/organization/content.html",
                    controller: 'organizationCtrl',
                    onEnter: function ($rootScope) {
                         $rootScope.activeLink = 'organization';
                    }
                })
        .state('createTeam', {
                    title: 'Create Teams in Organization',
                    url: "/createTeam",
                    templateUrl: "modules/organization/team.html",
                    controller: 'registerCtrl',
                    onEnter: function ($rootScope) {
                         $rootScope.activeLink = 'organization';
                    }
                })
        .state('billing', {
            title: 'Billing',
            url: "/billing",
            templateUrl: "modules/billing/content.html",
            controller: 'billingCtrl',
            onEnter: function ($rootScope) {
                 $rootScope.activeLink = 'billing';
            }
        })


        .state('ask', {
            title: 'Ask Page',
            url: "/ask",
            templateUrl: 'modules/search/content.html',
            controller: 'searchController',
            resolve: {
                check: function ($location, $rootScope, userService) {
                    if (!userService.isAlreadyLoggedIn()) {
                        $rootScope.redirectURL = '/ask';
                        $location.path('/login');
                    }
                }
            },
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'ask';
            }
        })

        .state('searchResult', {
            title: 'Search Result',
            url: "/searchResult",
            templateUrl: 'modules/searchResult/content.html',
            controller: 'searchController',
            resolve: {
                check: function ($location, $rootScope, userService) {
                    if (!userService.isAlreadyLoggedIn()) {
                        $rootScope.redirectURL = '/ask';
                        $location.path('/login');
                    }
                }
            },
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'searchResult';
            }
        })

        .state('cbc', {
            title: 'CBC Chapters',
            url: "/cbc/:chapterId",
            templateUrl: 'modules/cbc/content.html',
            controller: 'cbcController',
            resolve: {
                check: function ($location, $rootScope, userService) {
                    if (!userService.isAlreadyLoggedIn()) {
                        $rootScope.redirectURL = '/ask';
                        $location.path('/login');
                    }
                }
            },
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'cbc';
            }
        })

        .state('bookmarks', {
            title: 'Bookmarks GeFetchCode',
            url: "/bookmarks",
            templateUrl: "modules/bookmarks/content.html",
            controller: 'bookmarksController',
            resolve: {
                check: function ($location, $rootScope, userService) {
                    if (!userService.isAlreadyLoggedIn()) {
                        $rootScope.redirectURL = '/bookmarks';
                        $location.path('/login');
                    }
                }
            },
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'bookmarks';
            }
        })

        .state('recentSearches', {
            title: 'Recent Searches GeFetchCode',
            url: '/recentSearches',
            templateUrl: "modules/recentSearches/content.html",
            controller: 'recentController',
            resolve: {
                check: function ($location, $rootScope, userService) {
                    if (!userService.isAlreadyLoggedIn()) {
                        $rootScope.redirectURL = '/recentSearches';
                        $location.path('/login');
                    }
                }
            },
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'recentSearches';
            }
        })

        .state('messages', {
            title: 'Clarification Messages GeFetchCode',
            url: '/messages?token',
            templateUrl: "modules/messages/content.html",
            controller: 'messagesController',
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'messages';
            }
        })

        .state('people/activate', {
            title: 'Activation GeFetchCode',
            url: '/people/activate',
            templateUrl: 'modules/register/activation.html',
            controller: 'ActivationController'
        })

        .state('activationSuccessful', {
            title: 'Activation GeFetchCode',
            url: '/activationSuccessful',
            templateUrl: "modules/shared/partials/activation.html"
        })

        .state('activationFailed', {
            title: 'Activation GeFetchCode',
            url: '/activationFailed',
            templateUrl: 'modules/register/activation.html'
        })

        .state('forgotPassword', {
            title: 'Forgot Password GoFetchCode',
            url: "/forgotPassword",
            templateUrl: "modules/forgotPassword/content.html",
            controller: 'forgotPasswordController'
        })

        .state('changePassword', {
            title: 'Change Password GoFetchCode',
            url: "/changePassword",
            templateUrl: "modules/myAccounts/changePassword.html",
            controller: 'myAccountsController'
        })
        .state('myAccount', {
            title: 'My Acccount GeFetchCode',
            url: '/myAccount',
            templateUrl: "modules/myAccounts/content.html",
            controller: 'myAccountsController',
            resolve: {
                check: function ($location, $rootScope, userService) {
                    if (!userService.isAlreadyLoggedIn()) {
                        $rootScope.redirectURL = '/myAccount';
                        $location.path('/login');
                    }
                }
            },
            onEnter: function ($rootScope) {
                $rootScope.activeLink = 'myAccount';
            }
        })
        .state('editAccount', {
            title: 'Edit Acccount GeFetchCode',
            url: '/editAccount',
            templateUrl: "modules/myAccounts/editAccount.html",
            controller: 'myAccountsController',
            resolve: {
                check: function ($location, $rootScope, userService) {
                    if (!userService.isAlreadyLoggedIn()) {
                        $rootScope.redirectURL = '/editAccount';
                        $location.path('/login');
                    }
                }
            }
        });

    $urlRouterProvider.otherwise('/home');

    function redirectToLogin($location, $rootScope, userService) {
        if (!userService.isAlreadyLoggedIn()) {
            $rootScope.redirectURL = '/bookmarks';
            $location.path('/login');
        }
    }
});


gfcApp.run(function($rootScope) {
    $rootScope.$on('$stateChangeStart', function () {
        $rootScope.isNavOpen = false;
    })
});

gfcApp.config(['$httpProvider', function ($httpProvider) {

    $httpProvider.interceptors.push(interceptor);

}]);

var interceptor = ['$q', '$location', '$rootScope', function ($q, $location, $rootScope) {

    return {
        responseError: function (rejection) {
            if (rejection.status === 403) {
                $rootScope.changePath = true;
                $rootScope.authToken = null;
                $rootScope.isLoggedIn = false;
                $location.path("/login");
            }
            else if (rejection.status == 401 && rejection.data.status == "LOGIN_FAILED") {
                $rootScope.dataLoading = false;
                $rootScope.invalidCredentials = true;
                $rootScope.invalidUsername = false;
                $rootScope.pendingUsername = false ;
            }
            else if (rejection.status == 401 && rejection.data.status == "USER_NOT_FOUND") {
                $rootScope.dataLoading = false;
                $rootScope.invalidCredentials = false;
                $rootScope.invalidUsername = true;
                $rootScope.pendingUsername = false ;
            }
            else if (rejection.status == 401 && rejection.data.status == "ACTIVATION_PENDING") {
                $rootScope.dataLoading = false;
                $rootScope.invalidCredentials = false;
                $rootScope.invalidUsername = false;
                $rootScope.pendingUsername = true ;
            }            
            else if (rejection.status == 500 && $location.path() == "/signup") {
                $rootScope.isInternalError = true;
                $rootScope.errorMessage = rejection.data.message;
            }
            else if ($location.path() === "/changePassword" || $location.path() == "/forgotPassword") {
                $rootScope.resetPasswdLinkSent = false;
                $rootScope.errorMessage = rejection.data.message;
            }
            else if ($location.path() === "/editAccount") {
                $rootScope.isInternalError = true;
                $rootScope.errorMessage = rejection.data.message;
            }
            else {
                $rootScope.isInternalError = true;
                $rootScope.errorMessage = rejection.data.message;
            }
        }
    };
}];

gfcApp.service('WatsonService', function ($http, $q) {

    function doGet(dataCallback, errorCallBack, url, param1) {
        if (param1) url += '/' + param1
        $http.get(url).success(function (data, status, headers, config) {
            dataCallback(data);
        }).error(function (data, status, headers, config) {
            var message = data.message ? data.message : 'Could not GET ' + url + ': ' + status;
            console.error(JSON.stringify(data));
            errorCallBack(message, data, status, headers, config)
        });
    }

    function doPost(dataCallback, errorCallBack, url, data) {
        $http.post(url, data, {headers: {'Content-Type': 'application/json'}}).success(function (data, status, headers, config) {
            dataCallback(data);
        }).error(function (data, status, headers, config) {
            var message = data.message ? data.message : 'Could not POST ' + url + ': ' + status;
            console.error(JSON.stringify(data));
            errorCallBack(message, data, status, headers, config)
        });
    }

    var ask = function (email, question, dataCallback, errorCallBack) {
        doGet(dataCallback, errorCallBack, '/watson/ask?email=' + email + '&question=' + question);
    };

    var feedback = function (id, rating, comments, dataCallback, errorCallBack) {
        doPost(dataCallback, errorCallBack, '/watson/feedback/' + id + '?rating=' + rating + '&comments=' + comments);
    };

    return {
        ask: ask,
        feedback: feedback
    };
});
