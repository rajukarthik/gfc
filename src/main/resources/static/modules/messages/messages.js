if (!gfcApp) {
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}

gfcApp.controller('messagesController', ['$scope','$rootScope', 'messageService', '$log', '$stateParams', '$state', function ($scope, $rootScope, messageService, $log, $stateParams, $state) {

 	$scope.recepientEmails = "" ;
    $scope.clarifyQuestion = "" ;
    $scope.replySent = false;
    
    $('.modal').on('hidden.bs.modal', function(){
    	$rootScope.isInternalError = false ;
       	$rootScope.errorMessage = "" ;
    	$(this).find('form')[0].reset();
	});

    $scope.sendClarifyEmail = function(){
       
        var obj = {
            "recipientEmail" : $scope.recepientEmails.replace(/ /g,'').split(","),
            "comment" : $scope.clarifyQuestion,
            "sectionId" : $rootScope.sectionNumber,
            "sectionTitle" : $rootScope.sectionName,
            "clarificationId" : "",
            "commentedBy" : $rootScope.userDetails.email
        };
        $rootScope.dataLoading = true ;
        messageService.sendClarifyEmail("/api/secure/user/clarification/send", obj, successCallback, errorCallback) ;
        
        function successCallback (response) {
           $log.log("sendClarifyEmail success..") ;
           $rootScope.dataLoading = false;
           if(response.data.toUpperCase() === "SUCCESS"){
        	   $("#modalSuccess").show().delay(1000).fadeOut();
               $('#clarifyModal').modal('hide');               
           }
        }
        function errorCallback (response) {
        	$rootScope.dataLoading = true ;
            $log.log("sendClarifyEmail error..") ;
            // $rootScope.isInternalError = true ;
        //$rootScope.errorMessage = "Something went wrong!"
        }
    };

    $scope.chatsFound = false;
    $scope.isSortAsc = false;
    $scope.isReplying = false;
    $scope.replyText = '';

    function getChats() {
    	$rootScope.dataLoading = true;
        messageService.getChats($rootScope.userDetails.id, successCallback, errorCallback);

        function successCallback(chatList) {
            $scope.chats = chatList.data;
            $scope.chatsFound = ($scope.chats.length > 0);
            if($scope.chatsFound) {
                $scope.selectedChat = $scope.chats[0];
                $scope.selectedChat.hyperlink = $scope.createHyperLink($scope.selectedChat.sectionId);
                $scope.showMessages($scope.selectedChat, false);
            }
            $rootScope.dataLoading = false;
        }
        
        function errorCallback() {
            $rootScope.dataLoading = false;
        }        
    }

    function getMessages() {
    	$rootScope.dataLoading = true;
        messageService.getMessages($stateParams.token, successCallback, errorCallback);

        function successCallback(response) {
            $scope.messages = response.data.clarificationChats;
            $scope.selectedChat = {};
            $scope.selectedChat.guestEmail = response.data.userName;
            $scope.selectedChat.sectionId = response.data.sectionId;
            $scope.selectedChat.sectionTitle = response.data.sectionTitle;
            $scope.selectedChat.id = response.data.clarificationId;
            $scope.selectedChat.commentedBy = response.data.guestUserName;
            
            $scope.selectedChat.hyperlink = $scope.createHyperLink(response.data.sectionId);
            
            $scope.chatsFound = true;
            $rootScope.dataLoading = false;
        }

        function errorCallback() {
            $scope.chatsFound = false;
            $rootScope.dataLoading = false;
        }
    }

    $rootScope.isLoggedIn ? getChats() : getMessages();

    $scope.toggleSortOrder = function(){
        $scope.isSortAsc = !$scope.isSortAsc ;

        $scope.chats.reverse();
        $scope.selectedChat = $scope.chats[0];

        $scope.showMessages($scope.selectedChat, false);
    };

    $scope.showMessages = function (chat, option) {
        $scope.selectedChat = chat;
        $scope.isReplying = false;
        $rootScope.dataLoading = true;

        function successCallback(response) {
            $scope.messages = response.data.clarificationChats;
            $rootScope.dataLoading = false;
        }

        function errorCallback() {
            $rootScope.dataLoading = false;
        }

        if($rootScope.isLoggedIn) {

            if(!$scope.selectedChat.read) {
                messageService.markAsRead($scope.selectedChat.id, function() {
                    $scope.selectedChat.read = true;
                });
            }

            messageService.getChatMessages($scope.selectedChat.id, successCallback, errorCallback);

        }
        else {
            getMessages();
        }

        if (Utils().canToggle()) {
            Utils().toggleChapterActive("messageList", option);
        }
    };

    $scope.reply = function (replyText) {

    	var replyObject;
    	$rootScope.dataLoading = true;
    	if($rootScope.isLoggedIn) {
    		replyObject = {
    	            recipientEmail: [$scope.selectedChat.guestEmail],
    	            comment: replyText,
    	            sectionId: $scope.selectedChat.sectionId,
    	            sectionTitle: $scope.selectedChat.sectionTitle,
    	            clarificationId: $scope.selectedChat.id,
    	            commentedBy: $rootScope.userDetails.email
    		};
    	} else {
    		replyObject = {
    	            recipientEmail: [$scope.selectedChat.guestEmail],
    	            comment: replyText,
    	            sectionId: $scope.selectedChat.sectionId,
    	            sectionTitle: $scope.selectedChat.sectionTitle,
    	            clarificationId: $scope.selectedChat.id,
    	            commentedBy: $scope.selectedChat.commentedBy
    	     };
    	}

        messageService.reply(replyObject, successCallback, errorCallback);

        function successCallback(response) {
            $scope.messages.push(response.config.data);
            $scope.replyText = '';
            $rootScope.dataLoading = false;
            if(!$rootScope.isLoggedIn) {
            	$scope.replySent = true;            	
            } else {
            	$scope.showMessages($scope.selectedChat, false);
            }            
            
        }
        
        function errorCallback() {
            $rootScope.dataLoading = false;
        }        
    };

    $scope.toggleChapterActive = function(operation){
        Utils().toggleChapterActive("messageList", operation) ;
    }
    
    $scope.createHyperLink = function(sectionNumber) {
        var hyperlink;
        var location1 = sectionNumber.indexOf(".");
        var chapter = sectionNumber.substring(0, location1);

        // to get chapter we need to see if it is single digit or double single
        if (chapter.length == 3) {
            hyperlink = "/chapter/chapter0" + chapter.substring(0, 1) + ".html#" + sectionNumber;
        } else if (chapter.length == 4) { // double
            hyperlink = "/chapter/chapter" + chapter.substring(0, 2) + ".html#" + sectionNumber;
        }

        return hyperlink;   	
    }
}]);
