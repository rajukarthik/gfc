if(!gfcApp){
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}

gfcApp.controller('recentController',['$scope', '$log', '$http', '$rootScope', '$sce', '$location', 'searchService', 'userService', function($scope, $log, $http, $rootScope, $sce, $location, searchService, userService){

 $scope.getRecentAnswers = function(id){
    $log.log("getRecentAnswers") ;
    searchService.getRecentAnswers('/api/secure/history/answer?id=', id, successCallback, errorCallback) ;
    function successCallback(response){
            $log.log('getRecentAnswers Success..') ;
            $log.log(response) ;
            $rootScope.dataLoading = false ;
           // $('#resultDiv').css('display', 'block') ;
           if(response.statusText == "OK" && response.data.searchStatus == "NO_HISTORY_FOUND"){
              $scope.answerFound = false ;
              $scope.resultsFound = true ; 
           }
           else if(response.statusText == "OK"){
                showSearchResults(response) ;
            }

    }
    function errorCallback(response){
                $log.error("getRecentAnswers failure..") ;
                $('#resultDiv').css('display', 'block') ;
                $rootScope.dataLoading = false ;
                $log.error(response) ;
    }
  }

  	$scope.searchQuery = function(event, index) {
  		$log.log("searchQuery") ;
  		event.preventDefault();
  		$scope.currentIndex = index ;

      if (Utils().canToggle())
        Utils().toggleChapterActive("recentQuestions", true) ; //for mobile screen
  		$scope.getRecentAnswers(event.currentTarget.getAttribute("data-index")) ;
  	}

  	function showSearchResults(obj){
          if(obj.data.length > 0){
              $scope.searchResults = obj.data ;//obj.response.docs;
              $scope.resultsFound = true ;
              $scope.answerFound = true ;
          }
          else{
              $scope.resultsFound = true ;
               $scope.answerFound = false ;
          }
    }
	
	$scope.toggleSortOrder = function(){
		$scope.isSortAsc = !$scope.isSortAsc ;
		$scope.currentIndex = 0 ;

    if($scope.isSortAsc)
      $scope.getRecentAnswers($scope.recentResults[0].id) ;
    else
		  $scope.getRecentAnswers($scope.recentResults[$scope.recentResults.length-1].id) ;
	}

	$scope.sortFunction = function(result){
		if($scope.isSortAsc)
			return result.date ;
		else
			return -result.date ;
	}

    var getRecentSearches = function(){
        searchService.getRecentSearches('/api/secure/history/question', successCallback, errorCallback) ;

        function successCallback(response) {
            $log.log('getRecentSearches Success..') ;
            $log.log(response) ;
            $rootScope.dataLoading = false ;
            $('#resultDiv').css('display', 'block') ;
            if(response.data.length > 0){
              $scope.recentResults = response.data ;
              $scope.getRecentAnswers($scope.recentResults[$scope.recentResults.length-1].id) ;
            }
            else{
              $scope.resultsFound = false ; 
            }
        }
        function errorCallback(response) {
            $log.log('getRecentSearches failure..') ;
            $log.log(response) ;
            $rootScope.dataLoading = false ;
        }
    }

    $scope.isSortAsc = false ;
  	$scope.resultsFound = false ;
  	$scope.currentIndex = 0 ;
    $scope.toggleReadOnly = true ;
  	var questionAsked = "" ;
    getRecentSearches() ;

    $scope.gotoSearchPage = function(event, index){
      if(event.currentTarget.tagName.toUpperCase() == "FORM"){
          questionAsked = $scope.searchText ;
      }else{
          event.preventDefault() ;
         questionAsked = $("#recentQuestions").children("a").eq($scope.currentIndex).children('div').children('span').html() ;
      }
    	  

    	$rootScope.askedQuestion = questionAsked ;
    	$rootScope.selectedResultIndex = index  ;
    	$location.path('/ask') ;
    }
    $scope.toggleInputField = function(event){
          if((event.target.name == "search" && !$scope.toggleReadOnly) || event.target.id == "searchBtn")
              return ;

          $log.log("toggleInputField..") ;
          $scope.toggleReadOnly = !$scope.toggleReadOnly ;
          if($scope.toggleReadOnly){
            event.currentTarget.style.cursor = "pointer" ;
            $scope.searchText = "" ;
          }else{
                event.currentTarget.style.cursor = "default" ;
          }
    }

     $scope.logoutUser = function (event) {
                  console.log("logout called..");
                  userService.logout();
     }
    $scope.toggleChapterActive = function(operation){
        Utils().toggleChapterActive("recentQuestions", operation) ;
    } 
}]) ;