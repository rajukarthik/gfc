if(!gfcApp){
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}

 var cleanDate = function() {
          return {
            link: function(scope, element, attributes) {
                    var date = new Date(parseInt(attributes.cleanDate));
                    date = date.toDateString().split(" ") ;
                    element.html(date[2] + " " + date[1] + " " + date[3]) ;
            }
          };
        };

gfcApp.directive("cleanDate", cleanDate);

gfcApp.controller('bookmarksController',['$scope', '$log', '$http', '$rootScope', '$sce', '$location', 'searchService', 'bookmarkService','userService', function($scope, $log, $http, $rootScope, $sce, $location, searchService, bookmarkService,  userService){

   //this function is getting called when sorting is changed
    $scope.toggleSortOrder = function(){
        $scope.isSortAsc = !$scope.isSortAsc ;
        $scope.currentIndex = 0 ;

        if($scope.isSortAsc){
          $('#searchedChapter').load($scope.bookmarks[$scope.bookmarks.length-1].hyperlink, $scope.loadChaptercallback) ;
          //save section number and section name in rootScope so that we can use it in clarify functionality
          $rootScope.sectionNumber = $scope.bookmarks[$scope.bookmarks.length-1].sectionSequenceId ;
          $rootScope.sectionName = $scope.bookmarks[$scope.bookmarks.length-1].sectionSequenceTitle;
        }        
        else{
          $('#searchedChapter').load($scope.bookmarks[0].hyperlink, $scope.loadChaptercallback) ;
          //save section number and section name in rootScope so that we can use it in clarify functionality
          $rootScope.sectionNumber = $scope.bookmarks[$scope.currentIndex].sectionSequenceId ;
          $rootScope.sectionName = $scope.bookmarks[$scope.currentIndex].sectionSequenceTitle;
        }          
    }

    //this function sort the results in ascending/descending order of date
    $scope.sortFunction = function(result){
        if($scope.isSortAsc)
            return result.updatedDt ;
        else
            return -result.updatedDt ;
    }

    //load complete chapter in right pane for a particular bookmark on a sectionID
    $scope.getBookmarkChapter = function(event, index) {
      event.preventDefault();
      
      if(action === "remove")
        return ;
      $log.log("getBookmarkChapter..") ;
      $scope.currentIndex = index ;

      if($scope.isSortAsc){
          $log.log($scope.bookmarks[$scope.bookmarks.length-index-1].hyperlink) ;
          $('#searchedChapter').load($scope.bookmarks[$scope.bookmarks.length-index-1].hyperlink, $scope.loadChaptercallback) ;
      }else{
          $log.log($scope.bookmarks[index].hyperlink) ;
          $('#searchedChapter').load($scope.bookmarks[index].hyperlink, $scope.loadChaptercallback) ;
      }
      if (Utils().canToggle() && action != "remove")
          Utils().toggleChapterActive("bookmarkList", true) ; //for mobile screen

      action = "" ;
      //save section number and section name in rootScope so that we can use it in clarify functionality
      $rootScope.sectionNumber = $(event.currentTarget).attr("sectionNumAttr");
      $rootScope.sectionName = $(event.currentTarget).attr("sectionNameAttr");

      $scope.selectedBookmarkId = $(event.currentTarget).attr("data-index");
    }

    var showBookmarks = function(bookmarksArray){
          $scope.bookmarks = bookmarksArray ;
          $scope.resultsFound = true ;

          //save section number and section name in rootScope so that we can use it in clarify functionality
          $rootScope.sectionNumber = bookmarksArray[$scope.currentIndex].sectionSequenceId ;
          $rootScope.sectionName = bookmarksArray[$scope.currentIndex].sectionSequenceTitle ;

          $scope.selectedBookmarkId = bookmarksArray[$scope.currentIndex].id ;

          if(!Utils().canToggle()) //for mobile screens we will not get the detail chapters on first load
            $('#searchedChapter').load($scope.bookmarks[0].hyperlink, $scope.loadChaptercallback) ;
    }

    $scope.getBookmarks = function(){
      $rootScope.dataLoading = true ;
      bookmarkService.getBookmarks('/api/secure/bookmark', successCallback, errorCallback) ;

      function successCallback(response) {
          $log.log("getBookmarks success") ;
          $rootScope.dataLoading = false ;
          $('#resultDiv').css('display', 'block') ;
          if(response.data.length >0){
             showBookmarks(response.data) ;
          }else{
            $scope.resultsFound = false ;
          }
      }
      function errorCallback(response) {
          $log.log("getBookmarks error") ;
          $rootScope.dataLoading = false ;
          $scope.resultsFound = false ;
      }
    }

    //remove element from bookmark array when a bookmark is removed by user
    var removeArrayElm = function(argument) {
      if($scope.isSortAsc)
        $scope.bookmarks.splice($scope.bookmarks.length - $scope.currentIndex -1, 1) ;
      else
        $scope.bookmarks.splice($scope.currentIndex,1) ;
    }

    //chapter of neighbour section when a bookmark is removed by user
    var loadNeighbourBookmark = function(){
       if($scope.bookmarks.length ==0)
          $scope.resultsFound = false ;
       else{
          if($scope.isSortAsc){
              $log.log($scope.bookmarks[$scope.bookmarks.length-$scope.currentIndex-1].hyperlink) ;
              $('#searchedChapter').load($scope.bookmarks[$scope.bookmarks.length-$scope.currentIndex-1].hyperlink, $scope.loadChaptercallback) ;
            }else{
              $log.log($scope.bookmarks[$scope.currentIndex].hyperlink) ;
              $('#searchedChapter').load($scope.bookmarks[$scope.currentIndex].hyperlink, $scope.loadChaptercallback) ;
            }
       }
    }

    $scope.removeBookmark = function($event){
      $log.log("removeBookmark..") ;
      action = "remove" ;
      $rootScope.dataLoading = true ;
      var id = $scope.selectedBookmarkId; //$(event.target).parent().attr("data-index") ;
      bookmarkService.removeBookmark('/api/secure/bookmark/remove/', id, successCallback, errorCallback) ;

      function successCallback(response) {
          $log.log("removeBookmark success") ;
          $rootScope.dataLoading = false ;
          if(response.data){ //remove succeeded
            //$(event.target).parent().remove() ;
            $('#bookmarkList').find('a')[$scope.currentIndex].remove() ;
            removeArrayElm() ;

            if (!Utils().canToggle()){
              if($scope.currentIndex == 0){
                loadNeighbourBookmark() ;
              }else{
                $scope.currentIndex = $scope.currentIndex -1 ;
                loadNeighbourBookmark() ;
              }
            }
          }else{
            alert("Bookmark deletion failed! Please try again") ;
          }
      }
      function errorCallback(response) {
          $log.log("removeBookmark error") ;
          $rootScope.dataLoading = false ;
          alert("Bookmark deletion failed! Please try again") ;
      }
    }

    $scope.toggleInputField = function(event){
          if((event.target.name == "search" && !$scope.toggleReadOnly) || event.target.id == "searchBtn")
              return ;

          $log.log("toggleInputField..") ;
          $scope.toggleReadOnly = !$scope.toggleReadOnly ;
          if($scope.toggleReadOnly){
            event.currentTarget.style.cursor = "pointer" ;
            $scope.searchText = "" ;
          }else{
                event.currentTarget.style.cursor = "default" ;
          }
    }

    $scope.gotoSearchPage = function(event, index){
        $rootScope.askedQuestion = $scope.searchText ;
        $rootScope.selectedResultIndex = index ;
        $location.path('/ask') ;
    }

    $scope.loadChaptercallback = function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success") {
            var searchResultElement;
            if ($scope.isSortAsc) {
                searchResultElement = $scope.bookmarks[$scope.bookmarks.length - $scope.currentIndex - 1].sectionSequenceId;
            } else {
                searchResultElement = $scope.bookmarks[$scope.currentIndex].sectionSequenceId;
            }
            Utils().scrollTo(searchResultElement);
            getHighlights();
        }
        if (statusTxt == "error")
            console.log("Error: " + xhr.status + ": " + xhr.statusText);
    }

    $scope.logoutUser = function (event) {
                  console.log("logout called..");
                  userService.logout();
    }

    function getHighlights() {
        $log.log("getHighlights..");
        var hyperlink ;
        if ($scope.isSortAsc) {
            hyperlink = $scope.bookmarks[$scope.bookmarks.length - $scope.currentIndex - 1].hyperlink ;
        } 
        else{
            hyperlink = $scope.bookmarks[$scope.currentIndex].hyperlink ;
        }

        var chapterNo = hyperlink.split(".html")[0] ;
        chapterNo = parseInt(chapterNo.match(/\d+/)[0]) ;
        $log.log('chapterNo: ' +  chapterNo) ;
        //var chapterNo = $scope.searchResults[$scope.currentIndex].chapterNo;
        searchService.getHighlights("/api/secure/highlight/list?chapter=", chapterNo, successCallback, errorCallback);

        function successCallback(response) {
            $log.log("getHighlights success..");
            $log.log(response);

            if (response && response.data) {
                for (var i = 0; i < response.data.length; i++) {
                    searchService.recreateHighlight(document.getElementById("searchedChapter"), response.data[i]);
                }
            }
        }

        function errorCallback(response) {
            $log.log("getHighlights error..");
            $log.log(response);
        }
    }

    $scope.toggleChapterActive = function(operation){
        Utils().toggleChapterActive("bookmarkList", operation) ;
    } 

    var action = "" ;
    $scope.bookmarks = null
    $scope.isSortAsc = false ;
    $scope.resultsFound = false ;
    $scope.currentIndex = 0 ;
    $scope.toggleReadOnly = true ;
    $scope.searchResults = null ;
    $scope.getBookmarks() ;
}]) ;