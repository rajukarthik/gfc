 var compareTo = function() {
                return {
                  require: "ngModel",
                  scope: {
                    otherModelValue: "=compareTo"
                  },
                  link: function(scope, element, attributes, ngModel) {

                    ngModel.$validators.compareTo = function(modelValue) {
                      return modelValue == scope.otherModelValue;
                    };

                    scope.$watch("otherModelValue", function() {
                      ngModel.$validate();
                    });
                  }
                };
              };

gfcApp.directive("compareTo", compareTo);

gfcApp.factory('userService', ['$log','$http', '$rootScope', '$location', '$cookies','$timeout',function($log, $http, $rootScope, $location, $cookies, $timeout) {
		var createUser = function(user) {
			$log.log("createUser called..") ;
			$log.log(user) ;

			$rootScope.dataLoading = true ;
		/*	$timeout(function(){
				$rootScope.dataLoading = false ;
				$rootScope.changePath = true ;
				$location.path("/signupMessage") ;
			}, 2000) ;*/
			//REST api POST call here

            var postData = {"firstName": user.firstName,
                           "lastName": user.lastName,
                           "email": user.email,
                           "password":user.password,
                           "repeatPassword":user.confirmPassword
                           };

			switch(user.userType){
			    case "individual" :
			           postData["type"] = "INDIVIDUAL" ;
			           break ;
			    case "team" :
			          postData["type"] = "TEAM" ;
			          postData["team"] = {
			                              "name" : user.teamOrOrgName
			          } ;
                break ;
                case "organization" :
                     postData["type"] = "ORGANISATION" ;
                     postData["organisation"] = {
                	                      "name" : user.teamOrOrgName
                	 } ;
                break ;
			}
			$log.log("postData") ;
			$log.log(postData) ;
			var req = {
				 method: 'POST',
				 url: '/api/register',
				 data: postData
			}

			$http(req).then(function(response){
			    $log.log('Registration Success..') ;
				$log.log(response) ;
				$rootScope.dataLoading = false ;

				if(response.data.status == "SUCCESS"){
                    $rootScope.changePath = true ;
                    $rootScope.regMessage = response.data.message ;
                    $rootScope.registrationFailed = false ;
                    $location.path("/signupMessage") ;
                 }

			}, function(response){
			     $rootScope.dataLoading = false ;
			     $rootScope.registrationFailed = true ;
			     $log.error("Registration error..") ;
				 $log.error(response) ;
			});
		}

		var loginUser = function(user, isPersistentLogin) {
        			$log.log("loginUser called..") ;
        			$log.log(user) ;

        			$rootScope.dataLoading = true ;

        			var req = {
                 method: 'POST',
                 url: '/api/people/authenticate',
                 data: {"email": user.email, "password" : user.password}
            }

            $http(req).then(function(response){
                $log.log('Login Success..') ;
                $log.log(response) ;
                $rootScope.dataLoading = false ;

                if(response && response.data && response.data.status.toUpperCase() == "LOGIN_SUCCESS"){
                    $rootScope.authToken = response.headers('Auth-Token') ;
                    $rootScope.invalidCredentials = false ;
                    $rootScope.invalidUsername = false ;  
                    $rootScope.pendingUsername = false ;
                    $rootScope.changePath = true ;
                    $rootScope.isLoggedIn = true ;
                    $rootScope.userDetails = response.data.people ;
                    $rootScope.teamDetails = response.data.team ;
                    var userData = {} ;
                    userData.userDetails = $rootScope.userDetails;
                    userData.authToken = $rootScope.authToken;
                    userData.teamDetails =$rootScope.teamDetails;
                    //save cookie
                    if(isPersistentLogin){
                       var expireDate = new Date();
                        expireDate.setDate(expireDate.getDate() + 365);
                        $cookies.put('user', JSON.stringify(userData), {'expires': expireDate}) ;
                    }else{
                        //set cookie for 30 minutes
                        var thirtyMinutesLater = new Date();
                        thirtyMinutesLater.setMinutes(thirtyMinutesLater.getMinutes() + 30);
                        var expireDate = new Date(thirtyMinutesLater);

                        $cookies.put('user',JSON.stringify(userData), {'expires': expireDate}) ;
                    }

                    if($rootScope.redirectURL)
                        $location.path($rootScope.redirectURL) ;
                    else{
                       // $location.path("/dashboard") ;
                        $location.path("/ask") ;
                    }
                 }

            }, function(response){
                 $rootScope.dataLoading = false ;
                 $rootScope.invalidCredentials = true ;
                 $log.error("Login error..") ;
                 $log.error(response) ;
            });

        			/*$timeout(function(){
        			$rootScope.dataLoading = false ;

        			if(user.email == "abc@xyz.com" && user.password == "asd"){
        				$rootScope.invalidCredentials = true ;

        			}else{
        				$rootScope.invalidCredentials = false ;
        				$rootScope.isLoggedIn = true ;

                           if(isPersistentLogin){
                               var expireDate = new Date();
                                expireDate.setDate(expireDate.getDate() + 365);
                                $cookies.put('user',JSON.stringify({name:"John", email:user.email, accessToken: "asdffadasdfa"}), {'expires': expireDate}) ;
                           }else{
                                //sessionStorage.setItem('user', JSON.stringify({name:"John", email:user.email, accessToken: "asdffadasdfa"}));
                                $cookies.put('user',JSON.stringify({name:"John", email:user.email, accessToken: "asdffadasdfa"})) ;
                           }
                 		   $rootScope.changePath = true ;
                 		   $location.path("/dashboard") ;
        				}
        			}, 2000) ;*/
        }

        var isAlreadyLoggedIn = function(){
            if($cookies.get('user')){ 
               return true ;
            }
            /*if($rootScope.authToken){
                return true ;
            }*/
            return false ;
        }

        var logout = function(){
            var req = {
                 method: 'POST',
                 url: '/api/people/secure/logout',
                  headers: {
                   'Auth-Token': $rootScope.authToken
                 },
                 data: {}
            }

            $http(req).then(function(response){
                $log.log('Logout Success..') ;
                $log.log(response) ;
                
                if(response.data.status.toUpperCase() == "LOGOUT_SUCCESS"){
                    $rootScope.changePath = true ;
                    $rootScope.authToken = null ;
                    $rootScope.isLoggedIn = false ;
                    $rootScope.userDetails = null;
                    $rootScope.teamDetails=null;


                    if($cookies.get('user')){ 
                        $cookies.remove('user') ;
                    }
                    $location.path("/login") ;
                 }

            }, function(response){
                 $log.error("Logout error..") ;
                 $log.error(response) ;
            });
        }

        var validateActivationToken = function(URL, token, successCallback, errorCallback){
        	$rootScope.dataLoading = true ;
        	var req = {
        			method: 'GET',
        			url: URL + token
        	}
        	$http(req).then(successCallback, errorCallback);
        }
        
        var getResetPasswordLink = function(URL, email, successCallback, errorCallback){
            $rootScope.dataLoading = true ;
            var req = {
                 method: 'PUT',
                 url: URL ,
                  headers: {
                   'Auth-Token': $rootScope.authToken
                 },
                 data: {
                    "email" :email
                 }
            }
            $http(req).then(successCallback, errorCallback);
        }




        var createTeamMember = function(user) {
                $log.log("createUser called..") ;
                $log.log(user) ;
                $rootScope.dataLoading = true ;
                var postData = {"firstName": user.firstName,
                           "lastName": user.lastName,
                           "email": user.email,
                           "teamId":$rootScope.teamDetails[0][0],
                           "teamName":$rootScope.teamDetails[0][1]

                           };
                			var req = {
                				 method: 'POST',
                				 url: '/api/createTeamMember',
                				 data: postData
                			}

                			$http(req).then(function(response){
                			    $rootScope.dataLoading = false ;
                				if(response.data.status == "SUCCESS"){
                                    $location.path("/userManagement") ;
                                 }

                			}, function(response){
                			     $rootScope.dataLoading = false ;
                			     $rootScope.registrationFailed = true ;
                			     $log.error("Registration error..") ;
                				 $log.error(response) ;
                			});
                		}


		return{
			createUser: createUser,
			loginUser :loginUser,
			validateActivationToken :validateActivationToken,
			//sessionStorage_transfer : sessionStorage_transfer,
			isAlreadyLoggedIn : isAlreadyLoggedIn,
			logout : logout,
            getResetPasswordLink : getResetPasswordLink,
            createTeamMember:createTeamMember
		}

	}]) ;