gfcApp.factory('accountService', ['$log','$http', '$rootScope', '$location', '$cookies','$timeout',function($log, $http, $rootScope, $location, $cookies, $timeout) {
        
        var changePassword = function(URL, token, user, successCallback, errorCallback){
            $rootScope.dataLoading = true ;
            var req = {
                 method: 'PUT',
                 url: URL + token,
                 data: {
                         "password" : user.newPasswd,
                         "repeatPassword" : user.confirmPasswd
                    }
            }
            $http(req).then(successCallback, errorCallback);
        }		

        var updateAccount = function(URL, userData, successCallback, errorCallback){
            $rootScope.dataLoading = true ;
            var req = {
                 method: 'PUT',
                 url: URL,
                 headers: {
                    'Auth-Token': $rootScope.authToken
                },
                data: userData
            }
            $http(req).then(successCallback, errorCallback);
        }   

        var checkPassword = function(URL, successCallback, errorCallback){
            $rootScope.dataLoading = true ;
            var req = {
                 method: 'GET',
                 url: URL,
                 headers: {
                    'Auth-Token': $rootScope.authToken
                }
            }
            $http(req).then(successCallback, errorCallback);
        }
        
        var updatePassword = function(URL, userData, successCallback, errorCallback){
            $rootScope.dataLoading = true ;
            var req = {
                 method: 'PUT',
                 url: URL,
                 headers: {
                    'Auth-Token': $rootScope.authToken
                },
                data: userData
            }
            $http(req).then(successCallback, errorCallback);
        }   
		return{
			changePassword : changePassword,
            updateAccount : updateAccount,
            checkPassword : checkPassword,
            updatePassword : updatePassword
		}
	}]) ;