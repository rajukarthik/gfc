if (!gfcApp) {
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}

gfcApp.factory('searchService', ['$log', '$http', '$rootScope', '$location', '$sce', function ($log, $http, $rootScope, $location, $sce) {

    var callsearchAPI = function (URL, question, successCallback, errorCallback) {
        $rootScope.dataLoading = true;
        var req = {
            method: 'GET',
            url: URL + question,
            headers: {
                'Auth-Token': $rootScope.authToken
            },
        }

        $http(req).then(successCallback, errorCallback);
    }
    var getRecentSearches = function (URL, successCallback, errorCallback) {
        $rootScope.dataLoading = true;
        var req = {
            method: 'GET',
            url: URL,
            headers: {
                'Auth-Token': $rootScope.authToken
            },
        }

        $http(req).then(successCallback, errorCallback);
    }

    var getRecentAnswers = function (URL, id, successCallback, errorCallback) {
        $rootScope.dataLoading = true;
        var req = {
            method: 'GET',
            url: URL + id,
            headers: {
                'Auth-Token': $rootScope.authToken
            },
        }

        $http(req).then(successCallback, errorCallback);
    }

    var saveHighlight = function (URL, selection, h_data, successCallback, errorCallback) {
        var startSelection, endSelection;
        startSelection = pathToChunk(selection.anchorNode) + ":" + selection.anchorOffset;
        endSelection = pathToChunk(selection.focusNode) + ":" + selection.focusOffset;
        $log.log("startSelection: " + startSelection);
        $log.log("endSelection: " + endSelection);

        h_data["startIndex"] = startSelection;
        h_data["endIndex"] = endSelection;

        $rootScope.dataLoading = true;
        var req = {
            method: 'POST',
            url: URL,
            data: h_data,
            headers: {
                'Auth-Token': $rootScope.authToken
            },
        }

        $http(req).then(successCallback, errorCallback);
    }

    var getHighlights = function (URL, chapterNo, successCallback, errorCallback) {
        var req = {
            method: 'GET',
            url: URL + chapterNo,
            headers: {
                'Auth-Token': $rootScope.authToken
            },
        }

        $http(req).then(successCallback, errorCallback);
    }


    function recreateHighlight(chunk, data) {
        var startSelection, endSelection, startNode, endNode, startIndex, endIndex, sel;
        startSelection = data.startIndex;
        endSelection = data.endIndex;

        if (!startSelection || !endSelection)
            return;

        var startSelectionArr = startSelection.split(":");
        startNode = nodeAtPathFromChunk(chunk, startSelectionArr[0]);
        startIndex = startSelectionArr[1];

        var endSelectionArr = endSelection.split(":");
        endNode = nodeAtPathFromChunk(chunk, endSelectionArr[0]);
        endIndex = endSelectionArr[1];

        var range = document.createRange();
        range.setStart(startNode, startIndex);
        range.setEnd(endNode, endIndex);

        sel = window.getSelection();
        document.designMode = "on";
        colour = "yellow"
        if (range) {
            sel.removeAllRanges();
            sel.addRange(range);
        }
        // Use HiliteColor since some browsers apply BackColor to the whole block
        if (!document.execCommand("HiliteColor", false, colour)) {
            document.execCommand("BackColor", false, colour);
        }
        sel.removeAllRanges();
        document.designMode = "off";
    }

    function isChunk(node) {
        if (node == undefined || node == null) {
            return true;
        }
        return node.id == "searchedChapter";
    }

    var pathToChunk = function (node) {
        var components = new Array();

        // While the last component isn't a chunk
        var found = false;
        while (found == false) {
            var childNodes = node.parentNode.childNodes;
            var children = new Array(childNodes.length);
            for (var i = 0; i < childNodes.length; i++) {
                children[i] = childNodes[i];
            }
            components.unshift(children.indexOf(node));

            if (isChunk(node.parentNode) == true) {
                found = true
            } else {
                node = node.parentNode;
            }
        }

        return components.join("/");
    }

    var nodeAtPathFromChunk = function (chunk, path) {
        var components = path.split("/");
        var node = chunk;
        for (i in components) {
            var component = components[i];
            node = node.childNodes[component];
        }
        return node;
    }
    var setSelectionBGColor = function (selection, colour) {
        var range, sel = selection;

        if (sel.toString() == "")
            return;

        if (sel.rangeCount && sel.getRangeAt) {
            range = sel.getRangeAt(0);
        }
        document.designMode = "on";
        if (range) {
            sel.removeAllRanges();
            sel.addRange(range);
        }
        // Use HiliteColor since some browsers apply BackColor to the whole block
        if (!document.execCommand("HiliteColor", false, colour)) {
            document.execCommand("BackColor", false, colour);
        }
        sel.removeAllRanges();
        document.designMode = "off";

    };

    var rateSearchResponse = function (ratingData, successCallback) {

        var url = '/api/secure/search/answer/ratings';

        var req = {
            method: 'POST',
            url: url,
            data: ratingData,
            headers: {
                'Auth-Token': $rootScope.authToken
            }
        };

        $http(req).then(successCallback);
    };
    
    var rateByUserAndQuestionAndSection = function (ratingData, successCallback) {

        var url = '/api/secure/search/answer/ratings';

        var req = {
            method: 'GET',
            url: url,            
            params: ratingData,
            headers: {
                'Auth-Token': $rootScope.authToken
            }
        };

        $http(req).then(successCallback);
    };

    var getSearchHistory = function(params, successCallback) {
        var url = '/api/secure/question/search/history?pageno='+params.pageNo+'&pagesize='+params.pageSize;

        var req = {
            method: 'GET',
            url: url,
            headers: {
                'Auth-Token': $rootScope.authToken
            }
        };

        $http(req).then(successCallback);
    };

    return {
        getRecentSearches: getRecentSearches,
        getRecentAnswers: getRecentAnswers,
        callsearchAPI: callsearchAPI,
        saveHighlight: saveHighlight,
        getHighlights: getHighlights,
        setSelectionBGColor: setSelectionBGColor,
        recreateHighlight: recreateHighlight,
        rateSearchResponse: rateSearchResponse,
        getRateByUserAndQuestionAndSection: rateByUserAndQuestionAndSection,
        getSearchHistory: getSearchHistory
    }
}]);