if(!gfcApp){
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}

gfcApp.factory('bookmarkService', ['$log','$http', '$rootScope', '$location', function($log, $http, $rootScope, $location){
   
    var addBookmark = function(URL, sectionID, sectionTitle, successCallback, errorCallback){
        var req = {
                    method: 'POST',
                    url: URL,
                    headers: {
                              'Auth-Token': $rootScope.authToken
                    },
                    data: {"sectionSequenceId": sectionID, "sectionSequenceTitle" : sectionTitle}
        }

        $http(req).then(successCallback, errorCallback);
    }

    var removeBookmark = function(URL, id, successCallback, errorCallback){
        var req = {
                    method: 'PUT',
                    url: URL + id,
                    headers: {
                              'Auth-Token': $rootScope.authToken
                    },
        }

        $http(req).then(successCallback, errorCallback);
    }
    var getBookmarks = function(URL, successCallback, errorCallback){
        var req = {
                    method: 'GET',
                    url: URL ,
                    headers: {
                              'Auth-Token': $rootScope.authToken
                    },
        }

        $http(req).then(successCallback, errorCallback);
    }

    var checkBookmarks = function(URL, bookmarkId, successCallback, errorCallback){
        var req = {
                    method: 'GET',
                    url: URL + bookmarkId ,
                    headers: {
                              'Auth-Token': $rootScope.authToken
                    },
        }

        $http(req).then(successCallback, errorCallback);
    }

    return{
    	addBookmark : addBookmark,
    	removeBookmark : removeBookmark,
    	getBookmarks : getBookmarks,
        checkBookmarks : checkBookmarks
    }
}]) ;