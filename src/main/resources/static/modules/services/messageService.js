if (!gfcApp) {
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}

gfcApp.factory('messageService', ['$log', '$http', '$rootScope', '$location', '$sce', function ($log, $http, $rootScope, $location, $sce) {
  
    var baseUrl = '/api/';

    var getMessages = function (token, successCallback, errorCallback) {
        $http(createGetRequest('user/clarification/guest/chat/history?token='+token)).then(successCallback, errorCallback);
    };

    var getChats = function (userId, successCallback, errorCallback) {
        $http(createSecureGetRequest('secure/user/' + userId +'/clarification/chat/all')).then(successCallback, errorCallback);
    };

    var getChatMessages = function (chatId, successCallback) {
        $http(createSecureGetRequest('secure/user/clarification/chat/history?clarificationId='+chatId)).then(successCallback);
    };

    var reply = function (data, successCallback) {
        $http(createPostRequest('user/clarification/comment', data)).then(successCallback);
    };

    var markAsRead = function (clarificationId, successCallback) {
        $http(createPutRequest('/secure/user/clarification/'+clarificationId)).then(successCallback);
    };    
    
    var sendClarifyEmail = function (URL, m_data, successCallback, errorCallback) {
        var req = {
            method: 'POST',
            url: URL,
            data: m_data,
            headers: {
                'Auth-Token': $rootScope.authToken
            }
        };
        $http(req).then(successCallback, errorCallback);
    };

    function createGetRequest(url) {
        return {
            method: 'GET',
            url: baseUrl + url
        }
    }

    function createSecureGetRequest(url) {
        return {
            method: 'GET',
            url: baseUrl + url,

            headers: {
                'Auth-Token': $rootScope.authToken
            }
        }
    }

    function createPostRequest(url, data) {
        return {
            method: 'POST',
            url: baseUrl + url,
            data: data,

            headers: {
                'Auth-Token': $rootScope.authToken
            }
        }
    }

    function createPutRequest(url) {
        return {
            method: 'PUT',
            url: baseUrl + url,

            headers: {
                'Auth-Token': $rootScope.authToken
            }
        }
    }
    
    return {
        getMessages: getMessages,
        getChats: getChats,
        getChatMessages: getChatMessages,
        reply: reply,
        sendClarifyEmail : sendClarifyEmail,
        markAsRead : markAsRead
    }
}]);