gfcApp.directive('gfcCarousel', function () {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {

            elem.revolution(
                {
                    delay: 9000,
                    startwidth: 1170,
                    startheight: 500,
                    hideThumbs: 10,
                    navigationStyle: "preview4"
                })
        }
    }
});
