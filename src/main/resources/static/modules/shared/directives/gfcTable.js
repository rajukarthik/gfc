gfcApp.directive('gfcTable', function () {

    return {
        restrict: 'E',
        templateUrl: '/modules/shared/partials/gfcTable.html',
        controller: 'gfcTableController',
        scope: {
            tableFields: '=tableFields'
        }
    }
});
