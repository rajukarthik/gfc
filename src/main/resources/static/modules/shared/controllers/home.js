gfcApp.controller('homeCtrl', ['$scope', '$rootScope', '$window', '$location', '$interval', '$modal', '$log', '$anchorScroll', '$cookies', 'WatsonService', 'userService', function ($scope, $rootScope, $window, $location, $interval, $modal, $log, $anchorScroll, $cookies, WatsonService, userService) {

    $rootScope.activePath = $location.path();
    $rootScope.showMain = $rootScope.activePath == '/home';

    $('[data-toggle="tooltip"]').tooltip(); //initializing tooltip so we can show tooltip on hover of bookmarks, CBC and Recent Searches icons

    if (userService.isAlreadyLoggedIn()) {
        $rootScope.isLoggedIn = true;
        var userData = JSON.parse($cookies.get('user')) ;
        if(userData){
            $rootScope.authToken = userData.authToken ;
            $rootScope.userDetails = userData.userDetails ;
            $rootScope.teamDetails = userData.teamDetails ;

        }
    } else {
        $rootScope.isLoggedIn = false;
    }

    $scope.busy = false;

    $scope.question = $location.search()['question'];
    $scope.email = localStorage.getItem("email");
    if (!$scope.question) {
        if (supportsStorage) {
            $scope.question = sessionStorage.getItem("question");
        }
    }

    console.log("Path: " + $rootScope.activePath);
    console.log("showMain: " + $rootScope.showMain);
    console.log("email: " + $scope.email);
    console.log("question: " + $scope.question);

    $scope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.showMain = false;
        $rootScope.activePath = $location.path();
        $rootScope.showMain = $rootScope.activePath == '/home';
        console.log("Path: " + $rootScope.activePath);
        $rootScope.title = current.$$route.title;
    });


    $scope.responses = [];

    $rootScope.handleError = function handleError(message) {
        $log.log('handleError: ' + message);
        $rootScope.errorMessage = message;
        $scope.busy = false;
    };

    $scope.submitQuestion = function (userSubmit) {
        if (userSubmit && supportsStorage) {
            sessionStorage.removeItem('question');
            localStorage.setItem('email', $scope.email);
            sessionStorage.setItem('question', $scope.question);
        }
        if ($scope.email && $scope.question) {
            $scope.busy = true;
            $scope.rating = null;
            $scope.comments = null;
            WatsonService.ask($scope.email, $scope.question, function (data) {
                $scope.busy = false;
                $scope.id = data.id;
                $scope.responses = data.responses;
            }, $scope.handleError);
        }
    };

    $scope.provideFeedback = function () {
        WatsonService.feedback($scope.id, $scope.rating, $scope.comments, function () {
            $scope.message = 'Thank you, your feedback has been saved.';
            console.log("Feedback posted");
        }, $scope.handleError);
    };


    $scope.$on('$viewContentLoaded', function () {
        if ($scope.question) {
            $window.scroll(0, 0);
        }
    });

    $scope.ratings = [
        {id: 1, label: '1 - Not helpful'},
        {id: 2, label: '2'},
        {id: 3, label: '3 - Somewhat helful'},
        {id: 4, label: '4'},
        {id: 5, label: '5 - Very helpful'},
    ];

    $scope.resetRedirectURL = function () {
        console.log("resetRedirectURL");
        $rootScope.redirectURL = null;
    }
}]);