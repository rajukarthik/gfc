gfcApp.controller('gfcTableController', ['$scope', 'searchService', function ($scope, searchService) {

    var pageNo = 1,
        pageSize = 10,
        totalPages;

    $scope.tableData = [];
    $scope.disablePrev = true;
    $scope.disableNext = false;

    getSearchHistory();

    $scope.next = function () {
        pageNo += 1;
        getSearchHistory();
    };

    $scope.prev = function () {
        pageNo -= 1;
        getSearchHistory();
    };

    function getSearchHistory() {
        searchService.getSearchHistory({pageNo: pageNo, pageSize: pageSize}, successCallback);
    }

    function successCallback(response) {
        $scope.tableData = response.data.data;
        totalPages = response.data.totalPage;
        setButtonsStatus();
    }

    function setButtonsStatus() {
        $scope.disablePrev = false;
        $scope.disableNext = false;

        if (pageNo == 1) {
            $scope.disablePrev = true;
        }

        if (pageNo >= totalPages) {
            $scope.disableNext = true;
        }
    }
}]);