gfcApp.controller('headerCtrl', ['$scope', '$rootScope', '$cookies', 'userService', function ($scope, $rootScope, $cookies, userService) {

    $rootScope.isNavOpen = false;
    $scope.showSearchBox = false;

    if (userService.isAlreadyLoggedIn()) {
        $rootScope.isLoggedIn = true;
        var userData = JSON.parse($cookies.get('user'));
        if (userData) {
            $rootScope.authToken = userData.authToken;
            $rootScope.userDetails = userData.userDetails;
             $rootScope.teamDetails = userData.teamDetails ;
        }
    } else {
        $rootScope.isLoggedIn = false;
    }

    $rootScope.toggleNavigation = function () {
        $rootScope.isNavOpen = !$rootScope.isNavOpen;
        if($rootScope.isNavOpen)
            Utils().toggleBodyScrollbar(false) ; //hide body scrollbar
        else
            Utils().toggleBodyScrollbar(true) ; //show body scrollbar
    };

    $scope.logoutUser = function () {
        $rootScope.isNavOpen = false;
        userService.logout();
    };

    $rootScope.$watch('activeLink', function (value) {
        var pagesWithSearchBox = ['dashboard', 'bookmarks', 'searchResult', 'cbc', 'recentSearches', 'messages'];
        var pagesWithHomeLink = ['login', 'signup', 'about']
        Utils().toggleBodyScrollbar(true) ; //show body scrollbar
        $scope.showSearchBox = (pagesWithSearchBox.indexOf(value) !== -1);
        $scope.showHomeLink = (pagesWithHomeLink.indexOf(value) !== -1);
    });
}]);
