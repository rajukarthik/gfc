var Utils = function () {

    function scrollTo(element) {
        var element = document.getElementById(element);
        $(document).scrollTop(element.offsetTop);

        if($(window).width() > 767)
             $(".tag-box-v3").css({"border": "none", "border-left": "solid 2px #eee"}); //show border around chapter when screen size is large or medium
    }

    function changeToInternalLink() {
        $.each($('.cbc-sidebar a[href^="/cbc"]'), function (index, ele) {
            var ele = $(ele);
            var url = ele.attr('href');
            url = '#' + url.replace('.html', '');
            ele.attr('href', url)
        });

        $('.cbc-sidebar .list-group-item ul.collapse').removeClass('in');
    }
    function canToggle(){
        return $(window).width() < 767 ;
    }

    function toggleChapterActive(listId, operation){
        (operation) ?  $('.mobile-sketcher .result-item').addClass('active') : $('.mobile-sketcher .result-item').removeClass('active') ;
        (operation) ?  $('#' + listId).removeClass('active') :$('#' + listId).addClass('active') ;
        (operation) ?  $(".footer-v1 .copyright").addClass('marginBottom120') :$('.footer-v1 .copyright').removeClass('marginBottom120') ;
        
        showRatePanel() ;
        $(document).scrollTop(0);
    }

    function showRatePanel(){ //for mobile screen
        if ($('.mobile-sketcher .result-item').hasClass('active')){
          $('.rate-panel').addClass('active');
        }
        else{
          $('.rate-panel').removeClass('active');
        }
    }
    function resetControls(){
        $('.footer-v1 .copyright').removeClass('marginBottom120') ;
        $(document).scrollTop(0);
    }
    function toggleBodyScrollbar(show){
        show ? $('html').css("overflow-y","scroll"): $('html').css("overflow-y","hidden") ;
    }

    return {
        scrollTo: scrollTo,
        changeToInternalLink: changeToInternalLink,
        toggleChapterActive : toggleChapterActive,
        showRatePanel : showRatePanel,
        resetControls : resetControls,
        canToggle : canToggle,
        toggleBodyScrollbar : toggleBodyScrollbar
    };
};