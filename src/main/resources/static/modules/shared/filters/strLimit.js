gfcApp.filter('strLimit', ['$filter', function($filter) {
    return function(value, limit) {
        if (! value) return;
        if (value.length <= limit) {
            return value;
        }

        return $filter('limitTo')(value, limit) + '...';
    };
}]);