gfcApp.filter('trustAsHtml', ['$sce', function($sce){
    var div = document.createElement('div');
    return function(text) {
        div.innerHTML = text || '';
        return $sce.trustAsHtml(div.textContent);
    };
}]);