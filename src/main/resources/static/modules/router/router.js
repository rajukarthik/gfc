 gfcApp.run(['$rootScope', '$location', 'userService',function($rootScope, $location, userService) {

	    // register listener to watch route changes
	    $rootScope.$on( "$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
	      	console.log("routeChangeStart..") ;

	      	Utils().resetControls() ;
	       	 if(toState.url == "/signupMessage" && !$rootScope.changePath){
	      	 	event.preventDefault() ;
	      	 }
	      	 else if(toState.url == "/signupMessage" && $rootScope.changePath){
	      	 	$rootScope.changePath = false ;
	      	 }

	      	 if(toState.url == "/activationSuccessful"){
	      	    $rootScope.activationHeader = "Activation Successful" ;
             	$rootScope.activationMessage = "Your account has been activated now. Please login to access resources." ;
             }
             else if(toState.url == "/activationFailed"){
                $rootScope.activationHeader = "Activation Failed" ;
                $rootScope.activationMessage = "Activation failed. Please try again." ;
             }

             if(toState.url == "/ask?"){
                event.preventDefault() ;
             }
             if(toState.url == "/login" && userService.isAlreadyLoggedIn()){
                history.back() ;
             }
	      });
	 }]) ;