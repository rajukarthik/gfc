if (!gfcApp) {
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}

gfcApp.controller('cbcController', ['$scope', '$stateParams', function ($scope, $stateParams) {

    var chaptersRootUrl = '/cbc/';

    $scope.chapterUrl = chaptersRootUrl + $stateParams.chapterId + '.html';
alert( $scope.chapterUrl);
    $scope.convertToInternalLinks = function () {
        Utils().changeToInternalLink();
    };
}]);
