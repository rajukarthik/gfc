
	gfcApp.controller("registerCtrl",['$scope','$log', '$rootScope', '$http', 'userService','$stateParams', function($scope, $log, $rootScope, $http, userService, $stateParams){
	    var user = {firstName:null, lastName:null, email:null, password:null, confirmPassword:null, userType:"individual" , teamOrOrgName:null} ;
       	var belongTo = {} ;
   		belongTo.placeholder = "" ;
      $rootScope.isInternalError = false ;
      $rootScope.errorMessage = "" ;
      $scope.signUpType=$stateParams.signUpType;


   		var register = function() {
          $rootScope.isInternalError = false ;
   		    if($scope.isUserTypeSelected == undefined){
        		$scope.isUserTypeSelected = false;
        		return ;
            }
      		$log.log("register called");
       		$log.log($scope.user) ;
  			userService.createUser($scope.user) ;
        }
   		var checkEmail = function() {
       		$log.log($scope.user.email);
       		if($scope.user.email){
       			//call api to check if email already exists
       			/*if($scope.user.email == "abc@xyz"){
       				$scope.isEmailExists = true ;
       				return ;
       			}*/
       			var req = {
                		     method: 'GET',
                			 url: '/api/people?email=' + $scope.user.email
                 }

                $http(req).then(function(response){
                    $log.log('checkemail Success..') ;
                    $log.log(response) ;
                    $rootScope.dataLoading = false ;
                    if(response.data.message.toUpperCase() == "SUCCESS"){
                        $scope.isEmailExists = true ;
                    }else if(response.data.message.toUpperCase() == "EMAIL_NOT_REGISTERED"){
                        $scope.isEmailExists = false ;
                    }
                }, function(response){
                			     $rootScope.dataLoading = false ;
                			     $scope.isEmailExists = false ;
                			     $log.error("checkemail error..") ;
                				 $log.error(response) ;
                 });
       		 }
       			$scope.isEmailExists = false ;
     		}

     			var userTypeChanged = function(newType) {
                    	$scope.user.userType =  newType ;
                    	$scope.isUserTypeSelected = true ;
                    	//$log.log($scope.user.userType) ;
                        if($scope.user.userType == "team"){
                    		$scope.isIndividual = false ;
                    		belongTo.placeholder = "Team Name" ;
                    		$("#btn_team").addClass("active")
                    		$("#btn_org").removeClass("active") ;
                    		$("#btn_individual").removeClass("active");
                    	}
                    	else if($scope.user.userType == "organization"){
                    		$scope.isIndividual = false ;
                    		belongTo.placeholder = "Organization Name" ;
                    		$("#btn_team").removeClass("active")
                            $("#btn_org").addClass("active") ;
                            $("#btn_individual").removeClass("active") ;
                    	}else{
                    		$scope.isIndividual = true ;
                    		//$scope.user.teamOrOrgName = null ;
                    		belongTo.placeholder = "" ;
                    		$("#btn_team").removeClass("active")
                            $("#btn_org").removeClass("active") ;
                            $("#btn_individual").addClass("active") ;
                    	}
                    }

   	/*	var userTypeChanged = function(newType) {
        	$scope.user.userType =  newType ;
        	$scope.isUserTypeSelected = true ;
        	$log.log($scope.user.userType) ;
            if($scope.user.userType == "team"){
        		$scope.isIndividual = false ;
        		belongTo.placeholder = "Team Name" ;
        		$("#btn_team").addClass("btn-success")
        		$("#btn_team").removeClass("btn-default") ;
        		$("#btn_org").removeClass("btn-success") ;
        		$("#btn_org").addClass("btn-default") ;
        		$("#btn_individual").removeClass("btn-success") ;
        	    $("#btn_individual").addClass("btn-default") ;
        	}
        	else if($scope.user.userType == "organization"){
        		$scope.isIndividual = false ;
        		belongTo.placeholder = "Organization Name" ;
        		$("#btn_org").addClass("btn-success")
        		$("#btn_org").removeClass("btn-default") ;
        		$("#btn_team").removeClass("btn-success") ;
        		$("#btn_team").addClass("btn-default") ;
        		$("#btn_individual").removeClass("btn-success") ;
        		$("#btn_individual").addClass("btn-default") ;
        	}else{
        		$scope.isIndividual = true ;
        		//$scope.user.teamOrOrgName = null ;
        		belongTo.placeholder = "" ;
        		$("#btn_individual").addClass("btn-success")
        		$("#btn_individual").removeClass("btn-default") ;
        		$("#btn_org").removeClass("btn-success") ;
        		$("#btn_org").addClass("btn-default") ;
        		$("#btn_team").removeClass("btn-success") ;
        		$("#btn_team").addClass("btn-default") ;
        	}
        }*/
		$rootScope.dataLoading = false ;
		$rootScope.changePath = false ;
		$scope.user = user ;
       	$scope.register = register ;
       	$scope.checkEmail = checkEmail ;
       	$scope.isEmailExists = false ;
        $scope.userTypeChanged = userTypeChanged ;
        $scope.belongTo = belongTo ;
        $scope.isIndividual = true ;

		$(function () {
		 if($scope.signUpType != null){
               userTypeChanged($scope.signUpType);
              }
			$('[data-toggle="popover"]').popover();
		})
	}]) ;
