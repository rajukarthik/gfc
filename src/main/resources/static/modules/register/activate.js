if(!gfcApp){
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}

gfcApp.controller('ActivationController',['$scope', '$log', '$http', '$rootScope', '$sce', '$location', 'userService', function($scope, $log, $http, $rootScope, $sce, $location, userService){

    var validateActivationToken = function(){
    	userService.validateActivationToken('/api/people/activate?token=', $scope.token, successCallback, errorCallback) ;

        function successCallback(response) {            
        	console.log('<<<<<<<<<<<<<<<< validateActivationToken Success >>>>>>>>>>>>>>>>>>>>>>>..' + response) ;    
            $rootScope.dataLoading = false ;
            if(response != undefined && response.status === 202){
	      	    $rootScope.activationHeader = "Activation Successful" ;
             	$rootScope.activationMessage = "Your account has been activated now. Please login to access resources." ;            	
            	$location.path("/activationSuccessful");
            }
            else{
                $rootScope.activationHeader = "Activation Failed" ;
                $rootScope.activationMessage = "Activation failed. Invalid token.";            	
            	$location.path("/activationFailed");
            }
        }
        function errorCallback(response) {            
            console.log('<<<<<<<<<<<<<<<< validateActivationToken failure >>>>>>>>>>>>>>>>>>>>>>>..' + response) ;
            console.log(response) ;
            $rootScope.dataLoading = false ;
            $location.path("/activationFailed");
        }
    }
        
    if ( $location.search().hasOwnProperty( 'token' ) ) {
    	$scope.token = $location.search()['token'];
    	if($scope.token === "") {
    		$location.path("/activationFailed");
    	}
    } else {
    	$location.path("/activationFailed") ;
    }
    
    validateActivationToken();
}]) ;