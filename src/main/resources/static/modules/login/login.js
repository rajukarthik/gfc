gfcApp.controller('loginCtrl',['$scope','$log','$rootScope','userService', function($scope, $log, $rootScope, userService){

	localStorage.removeItem('searchText');
			$scope.user = {email:null, password:null} ;
			$scope.isPersistentLogin = false ;
			$scope.login = function() {
				$log.log("login called..") ;
				$log.log($scope.user) ;
				userService.loginUser($scope.user, $scope.isPersistentLogin) ;
			}
			$scope.hideError = function(){
				$log.log("hideError..") ;
				$rootScope.invalidCredentials = false ;
				$rootScope.invalidUsername = false ;
				$rootScope.pendingUsername = false ;
			}
		}]) ;