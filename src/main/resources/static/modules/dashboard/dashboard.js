if (!gfcApp) {
    var gfcApp = angular.module('gfcApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'angulartics', 'ngCookies']);
}

gfcApp.controller('dashboardCtrl', ['$scope', function ($scope) {

    $scope.tableFields = ['Email', 'Question', 'Top Answer', 'Search Date Time'];

}]);
