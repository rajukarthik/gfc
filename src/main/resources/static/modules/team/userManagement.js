gfcApp.controller("userManagementCtrl",['$scope','$log', '$rootScope', '$http', 'userService','$stateParams','$location','$window',
function($scope, $log, $rootScope, $http, userService, $stateParams,$location,$window){
        $scope.teamResult=$rootScope.teamDetails[0][1];
           var req = {
                         method: 'GET',
                          url: '/api/getTeamMembers?id='+$rootScope.teamDetails[0][0]
            }
            $http(req).then(function(response){
                $log.log(response) ;
                            if (response && response.statusText == "OK") {
                                showSearchResults(response);
                            } else {
                            }
            });
            function showSearchResults(obj) {
                $scope.searchResults = [];
                     if (obj.data.length> 0) {
                            $scope.searchResults = obj.data;
                     }
                     else {
                           $scope.searchResults = obj.data;
                     }
            }
            $scope.edit=function(id){
                var req = {
                      method: 'GET',
                      url: '/api/getTeamMemberDetails?id='+id
                }
                $http(req).then(function(response){
                    successResponse(response);
                });

            }

            function successResponse(response){
                if(response && response.data){
                    if(response.data.status.toUpperCase() === 'NOT_FOUND')
                        $scope.isPasswordCorrect = false ;
                    else if(response.data.status.toUpperCase() === 'SUCCESS')
                         $rootScope.teamMemberDetails = response.data ;
                         $location.path('/editTeamMember');
                }
            }
            $scope.deleteUser=function(id){
                var req = {
                    method: 'GET',
                    url: '/api/deleteTeamMember?id='+id
                }
                $http(req).then(function(){
                $rootScope.dataLoading = false ;
                   $window.location.reload();

                });
            }
}]) ;

