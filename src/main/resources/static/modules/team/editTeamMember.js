gfcApp.controller("editTeamMemberCtrl",['$scope','$log', '$rootScope', '$http', 'userService','$stateParams','$location',
function($scope, $log, $rootScope, $http, userService, $stateParams,$location){
var user = {firstName:null, lastName:null, email:null, password:null, confirmPassword:null, userType:"individual" , teamOrOrgName:null} ;
            if($rootScope.teamMemberDetails!=null){
                 $scope.user ={firstName: $rootScope.teamMemberDetails.person.firstName,
                                    lastName:$rootScope.teamMemberDetails.person.lastName,
                                    email:$rootScope.teamMemberDetails.person.email} ;
            }else{
                $location.path("/userManagement");
            }
            $scope.createTeamMember = function() {
                   userService.createTeamMember($scope.user) ;
           }
}]) ;