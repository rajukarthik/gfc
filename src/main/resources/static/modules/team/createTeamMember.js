gfcApp.controller("createTeamMemberCtrl",['$scope','$log', '$rootScope', '$http', 'userService','$stateParams','$location',
function($scope, $log, $rootScope, $http, userService, $stateParams,$location){

            $scope.createTeamMember = function() {
                   userService.createTeamMember($scope.user) ;
           }



           $scope.checkEmail = function() {
                   		$log.log($scope.user.email);
                   		if($scope.user.email){
                   			//call api to check if email already exists
                   			/*if($scope.user.email == "abc@xyz"){
                   				$scope.isEmailExists = true ;
                   				return ;
                   			}*/
                   			var req = {
                            		     method: 'GET',
                            			 url: '/api/people?email=' + $scope.user.email
                             }

                            $http(req).then(function(response){
                                $log.log('checkemail Success..') ;
                                $log.log(response) ;
                                $rootScope.dataLoading = false ;
                                if(response.data.message.toUpperCase() == "SUCCESS"){
                                    $scope.isEmailExists = true ;
                                }else if(response.data.message.toUpperCase() == "EMAIL_NOT_REGISTERED"){
                                    $scope.isEmailExists = false ;
                                }
                            }, function(response){
                            			     $rootScope.dataLoading = false ;
                            			     $scope.isEmailExists = false ;
                            			     $log.error("checkemail error..") ;
                            				 $log.error(response) ;
                             });
                   		 }
                   			$scope.isEmailExists = false ;
                 		}
                 		$scope.isEmailExists = false ;
}]) ;