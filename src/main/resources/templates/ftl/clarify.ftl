Dear ${recipient},
<br/><br/>

${sender} needs your help and wants you to clarify below query.
<br/><br/>
<strong>Query:</strong><br/> 
${query} 
<br/><br/>

Kindly click on below link to answer:
<br/><br/>

<a href='${url}'>${url}</a>
<br/><br/><br/>


Regards,
Admin

<br/><br/>
Having problem with the link? Contact out support team: <a href='mailto:info@gofetchcode.com'>info@gofetchcode.com</a>