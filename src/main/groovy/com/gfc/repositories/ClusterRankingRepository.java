package com.gfc.repositories;

import com.gfc.models.domain.ClusterRankingEntity;
import com.gfc.users.accounts.enums.ActivityStatus;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This repository is used to manage IBM watson solr cluster details.
 * Created by gurinder on 28/1/16.
 */
public interface ClusterRankingRepository extends JpaRepository<ClusterRankingEntity, Integer> {
    ClusterRankingEntity findBySolrClusterId(String solrClusterId);

    ClusterRankingEntity findByStatus(ActivityStatus status);
}
