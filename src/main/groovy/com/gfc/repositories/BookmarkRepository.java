/**
 * 
 */
package com.gfc.repositories;

import com.gfc.models.domain.BookmarkEntity;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * It acts as a repository which will interact with database and manage bookmark actions.
 * @author Shantanu Singh
 */
@Repository
public interface BookmarkRepository extends JpaRepository<BookmarkEntity, Integer> {
	
	public BookmarkEntity findById(Long id);

	public BookmarkEntity findBySectionSequenceIdAndPeople(String sectionSequenceId, User people);

	public List<BookmarkEntity> findByStatusAndPeopleOrderByUpdatedDtDesc(ActivityStatus status, User people);
}
