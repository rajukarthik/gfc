/**
 *
 */
package com.gfc.repositories;

import com.gfc.models.domain.SearchHistory;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This repository is used to manage recent searches or search history details in database.
 * @author Shantanu Singh
 */
@Repository
public interface SearchHistoryRepository extends JpaRepository<SearchHistory, Integer> {

    List<SearchHistory> findByPeople(User people);

    SearchHistory findById(Long id);

    List<SearchHistory> findByQuestion(String question);

    SearchHistory findByQuestionAndPeopleAndStatus(String question, User people, ActivityStatus status);

    List<SearchHistory> findByPeopleAndStatus(User people, ActivityStatus status);

}
