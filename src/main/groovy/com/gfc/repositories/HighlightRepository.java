package com.gfc.repositories;

import com.gfc.models.domain.HighlightEntity;
import com.gfc.users.accounts.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This repository is used to manage text highlights details in database.
 * Created by gurinder on 19/1/16.
 */
@Repository
public interface HighlightRepository extends JpaRepository<HighlightEntity, Integer> {
    List<HighlightEntity> findByChapterAndPeople(String chapter, User people);

    HighlightEntity findByStartIndexAndEndIndex(String startIndex, String endIndex);
}
