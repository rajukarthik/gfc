package com.gfc.repositories;

import com.gfc.models.domain.ConfigurationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This repository is used to manage configurations in database for the application.
 * Created by gurinder on 29/1/16.
 */
public interface ConfigurationRepository extends JpaRepository<ConfigurationEntity, Integer> {

    ConfigurationEntity findByCode(int code);
}
