package com.gfc.repositories.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.models.domain.HighlightEntity;
import com.gfc.models.dto.MessageDto;
import com.gfc.models.dto.request.HighLightDetailsDto;
import com.gfc.repositories.HighlightRepository;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * This acts as an abstraction layer between highlight service and highlight repository.
 * Created by gurinder on 19/1/16.
 */
@Service
public class HighlightRepositoryService {

    private HighlightRepository highlightRepository;

    @Autowired
    public HighlightRepositoryService(HighlightRepository highlightRepository) {
        this.highlightRepository = highlightRepository;
    }

    /**
     * This method is used to save highlight details for the selected chapter and logged-in user.
     * @param loggedInUser
     * @param request
     * @return
     */
    public MessageDto addChapter(User loggedInUser, HighLightDetailsDto request) {
        HighlightEntity highlightEntity = highlightRepository.findByStartIndexAndEndIndex(request.getStartIndex(), request.getEndIndex());
        if (highlightEntity != null) {
            if (highlightEntity.getStatus().equals(ActivityStatus.INACTIVE)) {
                highlightEntity.setStatus(ActivityStatus.ACTIVE);
                highlightRepository.save(highlightEntity);
                return new MessageDto("success", ResponseStatus.SUCCESS);
            }
            return new MessageDto("indexes already exist", ResponseStatus.INTERNAL_ERROR);
        }
        if (highlightEntity == null) {
            highlightEntity = new HighlightEntity();
        }
        highlightEntity.setPeople(loggedInUser);
        highlightEntity.setChapter(request.getChapter());
        highlightEntity.setStartIndex(request.getStartIndex());
        highlightEntity.setEndIndex(request.getEndIndex());
        highlightEntity.setStatus(ActivityStatus.ACTIVE);
        HighlightEntity saved = highlightRepository.save(highlightEntity);

        if (saved == null) {
            return new MessageDto("failure", ResponseStatus.INTERNAL_ERROR);
        }

        return new MessageDto("success", ResponseStatus.SUCCESS);

    }

    /**
     * This method is used to get chapter details for selected chapter and logged-in user.
     * @param chapterNo
     * @param people
     * @return
     */
    public List<HighLightDetailsDto> getChapter(String chapterNo, User people) {
        List<HighlightEntity> chapterIndexes = highlightRepository.findByChapterAndPeople(chapterNo, people);
        List<HighLightDetailsDto> highLightIndexes = null;
        if (CollectionUtils.isNotEmpty(chapterIndexes)) {
            highLightIndexes = chapterIndexes.stream()
                    .filter(gfcHighLight -> gfcHighLight.getStatus().equals(ActivityStatus.ACTIVE))
                    .map(gfcHighLight -> new HighLightDetailsDto(gfcHighLight.getStartIndex(), gfcHighLight.getEndIndex()))
                    .collect(toList());
        }
        return highLightIndexes;
    }

    /**
     * This method is used to delete highlight.
     * @param request
     * @return
     */
    public MessageDto deleteChapterIndexes(HighLightDetailsDto request) {
        HighlightEntity highlightEntity = highlightRepository.findByStartIndexAndEndIndex(request.getStartIndex(), request.getEndIndex());
        if (highlightEntity == null) {
            return new MessageDto("no indexes exist", ResponseStatus.INTERNAL_ERROR);
        } else if (highlightEntity.getStatus().equals(ActivityStatus.INACTIVE)) {
            return new MessageDto("indexes already deleted", ResponseStatus.INTERNAL_ERROR);
        }
        highlightEntity.setStatus(ActivityStatus.INACTIVE);
        HighlightEntity saved = highlightRepository.save(highlightEntity);

        if (saved == null) {
            return new MessageDto("failure", ResponseStatus.INTERNAL_ERROR);
        }

        return new MessageDto("success", ResponseStatus.SUCCESS);

    }
}
