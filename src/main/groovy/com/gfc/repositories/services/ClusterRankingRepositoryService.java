package com.gfc.repositories.services;

import com.gfc.models.domain.ClusterRankingEntity;
import com.gfc.repositories.ClusterRankingRepository;
import com.gfc.users.accounts.enums.ActivityStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * This acts as an abstraction layer between watson solr cluster service and watson solr cluster repository.
 * Created by gurinder on 28/1/16.
 */
@Service
public class ClusterRankingRepositoryService {
    
	private ClusterRankingRepository clusterRankingRepository;
	
    @Autowired
    public ClusterRankingRepositoryService(ClusterRankingRepository clusterRankingRepository) {
        this.clusterRankingRepository = clusterRankingRepository;
    }	
	
    /**
     * This method is used to save IBM watson solr cluster details in database.
     * @param clusterRankingEntity
     * @return
     */
    @Transactional
    public ClusterRankingEntity save(ClusterRankingEntity clusterRankingEntity) {
    	return clusterRankingRepository.save(clusterRankingEntity);
    }
    /**
     * This method is used to fetch all IBM watson solr cluster details from database.
     * @return
     */
    @Transactional
    public List<ClusterRankingEntity> findAll() {
    	return clusterRankingRepository.findAll();
    }
    
    /**
     * This method is used to find IBM watson colr cluster details based on status.
     * @param status
     * @return
     */
    public ClusterRankingEntity findByStatus(ActivityStatus status) {
    	return clusterRankingRepository.findByStatus(status);
    }
    
    /**
     * This method is used to delete selected IBM watson solr cluster record.
     * @param clusterRankingEntity
     */
    @Transactional
    public void deleteWatsonSolrCluster(ClusterRankingEntity clusterRankingEntity) {
    	clusterRankingRepository.delete(clusterRankingEntity);
    }
	
}
