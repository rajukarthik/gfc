package com.gfc.repositories.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.exceptions.RegistrationException;
import com.gfc.models.dto.request.ResetPasswordRequestDto;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.UserRepository;
import com.gfc.utils.EncryptUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This acts as an abstraction layer between person service and person repository.
 * Created by abhinav on 20/12/15.
 */
@Service
public class PersonRepositoryService {

    private UserRepository personRepository;
    private static final Logger logger = LoggerFactory.getLogger(PersonRepositoryService.class);


    @Autowired
    public PersonRepositoryService(UserRepository personRepository) {
        this.personRepository = personRepository;
    }

    /**
     * This method is used to manage change password.
     * @param request
     * @param personId
     * @return
     */
    public ResponseDto changePassword(ResetPasswordRequestDto request, Integer personId) {
        User person = personRepository.findById(personId);
        String encryptedPassword = EncryptUtils.encryptMD5(request.getPassword());

        person.setPassword(encryptedPassword);
        User user = personRepository.saveAndFlush(person);

        if (user.getId() != null) {
            return new ResponseDto("Password changed successfully", ResponseStatus.SUCCESS, null, null, user);
        } else {
            return new ResponseDto("Password changed failed", ResponseStatus.INVALID_REQUEST, null, null, user);
        }
    }

    /**
     * This method is used to save user in database.
     * @param user
     * @return
     * @throws RegistrationException
     */
    public User save(User user) throws RegistrationException {
        if (personRepository.findByEmail(user.getEmail()) != null) {
            throw new RegistrationException(ResponseStatus.EMAIL_ALREADY_REGISTERED.getStatus());
        }
        return personRepository.save(user);
    }
    
    /**
     * This method is used to find user details for the selected email
     * @param email
     * @return
     */
    public User findByEmail(String email) {
        logger.info("The email is " + email);
        return personRepository.findByEmail(email);
    }

    /**
     * This method is used to activate user.
     * @param id
     * @return
     * @throws PersonNotFoundException
     */
    public User activate(Integer id) throws PersonNotFoundException {
        User person = personRepository.findOne(id);
        if (person != null) {
            person.setStatus(ActivityStatus.ACTIVE);
            person = personRepository.saveAndFlush(person);
        } else {
            throw new PersonNotFoundException(ResponseStatus.PERSON_NOT_FOUND.getStatus());
        }
        return person;
    }

    /**
     * This method is used to fetch user details for selected email and status.
     * @param email
     * @param status
     * @return
     * @throws PersonNotFoundException
     */
    public User findByEmailAndStatus(String email, ActivityStatus status) throws PersonNotFoundException {
        User person = personRepository.findByEmailAndStatus(email, status);
        if (person == null)
            throw new PersonNotFoundException(ResponseStatus.LOGIN_FAILED.getStatus());
        return person;
    }

    /**
     * This method is used to fetch user details for the selected id.
     * @param id
     * @return
     * @throws PersonNotFoundException
     */
    public User findById(Integer id) throws PersonNotFoundException {
        User person = personRepository.findOne(id);
        if (person == null)
            throw new PersonNotFoundException(ResponseStatus.PERSON_NOT_FOUND.getStatus());
        return person;
    }

    /*
     * This method is used to fetch user details for the selected email.
     */
    public User getNameByEmail(String email) throws PersonNotFoundException {
        User person = personRepository.findByEmail(email);

        if (person == null)
            throw new PersonNotFoundException(ResponseStatus.PERSON_NOT_FOUND.getStatus());
        return person;
    }


}
