package com.gfc.repositories.services;

import com.gfc.enums.SearchStatus;
import com.gfc.exceptions.HistoryNotFoundException;
import com.gfc.models.domain.SearchHistory;
import com.gfc.models.domain.SearchResponse;
import com.gfc.models.dto.response.AnswerResponseDto;
import com.gfc.models.dto.response.QuestionResponseDto;
import com.gfc.repositories.SearchHistoryRepository;
import com.gfc.services.ResponseSummaryRepository;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * This acts as an abstraction layer between search history service and search history repository.
 * Created by gurinder on 12/1/16.
 */

@Service
public class HistoryRepositoryService {

    private SearchHistoryRepository searchHistoryRepository;

    private ResponseSummaryRepository responseSummaryRepository;

    @Autowired
    public HistoryRepositoryService(SearchHistoryRepository searchHistoryRepository, ResponseSummaryRepository responseSummaryRepository) {
        this.searchHistoryRepository = searchHistoryRepository;
        this.responseSummaryRepository = responseSummaryRepository;
    }

    /**
     * This method is used to fetch all questions searched by logged-in user.
     * @param loggedInUser
     * @return
     * @throws HistoryNotFoundException
     */
    public List<QuestionResponseDto> getAllQuestions(User loggedInUser) throws HistoryNotFoundException {
        List<QuestionResponseDto> questionResponseDtoList;
//        List<SearchHistory> userHistory = searchHistoryRepository.findByPeople(loggedInUser);
        List<SearchHistory> userHistory = searchHistoryRepository.findByPeopleAndStatus(loggedInUser, ActivityStatus.ACTIVE);
        if (CollectionUtils.isNotEmpty(userHistory)) {
            questionResponseDtoList = userHistory.stream().map(history -> new QuestionResponseDto(history.getId(), history.getQuestion(), history.getCreatedDt())).collect(toList());
        } else {
            throw new HistoryNotFoundException(SearchStatus.NO_HISTORY_FOUND.getStatus());
        }
        return questionResponseDtoList;
    }

    /**
     * This method is used fetch answer details for the selected question.
     * @param id
     * @return
     * @throws HistoryNotFoundException
     */
    public List<AnswerResponseDto> getAnswer(Long id) throws HistoryNotFoundException {
        List<AnswerResponseDto> answerResponseDtoList = null;
        SearchHistory searchHistoryEntity = searchHistoryRepository.findById(id);
        if (searchHistoryEntity == null) {
            throw new HistoryNotFoundException(SearchStatus.NO_HISTORY_FOUND.getStatus());
        }
        List<SearchResponse> searchResponseSummaryList = responseSummaryRepository.findBySearchHistory(searchHistoryEntity);

        if (CollectionUtils.isNotEmpty(searchResponseSummaryList)) {
            AnswerResponseDto answerResponseDto = AnswerResponseDto.getInstance();
            answerResponseDtoList = searchResponseSummaryList.stream().map(answerResponseDto::createAnswerDto).collect(toList());
        } else {
            throw new HistoryNotFoundException(SearchStatus.NO_ANSWER_FOUND.getStatus());
        }
        return answerResponseDtoList;
    }


}
