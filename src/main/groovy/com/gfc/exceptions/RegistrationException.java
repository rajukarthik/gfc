package com.gfc.exceptions;

/**
 * Created by abhinav on 20/12/15.
 */
public class RegistrationException extends Exception {


    public RegistrationException(String message) {
        super(message);
    }


}
