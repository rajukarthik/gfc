package com.gfc.exceptions;

/**
 * Created by gurinder on 14/1/16.
 * An exception used to manage search history exceptions.
 */
public class HistoryNotFoundException extends Exception {
    public HistoryNotFoundException(String message) {
        super(message);
    }
}
