package com.gfc.exceptions;

/**
 * Created by abhinav on 27/12/15.
 */
public class PersonNotFoundException extends Exception{

    public PersonNotFoundException(String message) {
        super(message);
    }
}
