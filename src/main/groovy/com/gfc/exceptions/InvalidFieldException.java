package com.gfc.exceptions;

/**
 * Created by abhinav on 26/12/15.
 */
public class InvalidFieldException extends Exception {
    public InvalidFieldException(String message) {
        super(message);
    }
}
