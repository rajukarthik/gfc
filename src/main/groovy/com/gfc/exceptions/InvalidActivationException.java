package com.gfc.exceptions;

/**
 * Created by gurinder on 26/12/15.
 * An exception used to manage Invalid Activation exception in activation workflow.
 */
public class InvalidActivationException extends Exception {
    public InvalidActivationException(String message) {
        super(message);
    }
}
