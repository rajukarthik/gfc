package com.gfc.exceptions;

/**
 * An exception used to manage email failure.
 * @author athakran
 *
 */
public class EmailFailureException extends Exception {

    public EmailFailureException(String message) {
        super(message);
    }
}
