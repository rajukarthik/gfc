package com.gfc.services;

import com.gfc.enums.EmailType;
import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.EmailFailureException;
import com.gfc.exceptions.InvalidActivationException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.dto.request.ResetPasswordRequestDto;
import com.gfc.repositories.services.PersonRepositoryService;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.services.UserService;
import com.gfc.utils.TokenGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by Shantanu Singh on 05/03/16.
 * It is a service for Reset Password.
 */
@SuppressWarnings("Duplicates")
@Service
public class ResetPasswordService {

    private PersonRepositoryService personRepositoryService;
    private EmailService emailService;
    private UserService personService;

    private static final Logger logger = LoggerFactory.getLogger(ResetPasswordService.class);

    @Autowired
    public ResetPasswordService(
            PersonRepositoryService personRepositoryService,
            EmailService emailService,
            UserService personService
    ) {
        this.personRepositoryService = personRepositoryService;
        this.emailService = emailService;
        this.personService = personService;
    }

    /**
     * This method is used to send reset password notification mail to user.
     * @param request
     * @return
     * @throws IllegalArgumentException
     * @throws PersonNotFoundException
     * @throws EmailFailureException
     */
    public ResponseDto sendResetPasswordEmail(ResetPasswordRequestDto request)
            throws IllegalArgumentException, PersonNotFoundException, EmailFailureException {
        String emailId = request.getEmail();

        User person = personRepositoryService.findByEmail(emailId);

        if (person != null) {
            String userName = person.getFirstName() + " " + person.getLastName();
            String token = TokenGenerator.getToken();
            emailService.sendEmail(userName, emailId, null, token, EmailType.RESET_PASSWORD);
        } else {
            return new ResponseDto("This email address is not registered.", ResponseStatus.EMAIL_NOT_REGISTERED, null, null, person);
        }

        return new ResponseDto("A password recovery link has been sent to your email address.",
                ResponseStatus.SUCCESS, null, null, person);
    }

    /**
     * This method is used to validation reset pasword token.
     * @param token
     * @return
     * @throws InvalidActivationException
     */
    public Integer validateAuthToken(String token)
            throws InvalidActivationException {
        return personService.validateAuthToken(token);
    }

    /**
     * This method is used to save modified password in database.
     * @param resetPasswordRequestDto
     * @param token
     * @return
     * @throws InvalidActivationException
     */
    public ResponseDto resetPassword(ResetPasswordRequestDto resetPasswordRequestDto, String token)
            throws InvalidActivationException {
        Integer personId = validateAuthToken(token);

        return personRepositoryService.changePassword(resetPasswordRequestDto, personId);
    }
}
