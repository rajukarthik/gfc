package com.gfc.services;

import com.gfc.enums.EmailType;
import com.gfc.exceptions.EmailFailureException;
import com.gfc.utils.EmailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by gurinder on 21/12/15.
 * It is a service for sending emails.
 */

@Service
public class EmailService {
    private EmailSender emailSender;

    @Autowired
    public EmailService(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    /**
     * This method is used to prepare email messages and then send them.
     * @param userName
     * @param recipientEmail
     * @param comment
     * @param token
     * @param emailType
     * @throws EmailFailureException
     */
    public void sendEmail(String userName, String recipientEmail, String comment, String token, EmailType emailType) throws EmailFailureException{
        emailSender.processEmail(userName, recipientEmail, comment, token, emailType);
    }
}
