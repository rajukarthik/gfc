/**
 *
 */
package com.gfc.services;

import com.gfc.models.domain.SearchHistory;
import com.gfc.models.domain.SearchResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Shantanu Singh
 * It acts as a repository for search response from IBM watson.
 */
@Repository
public interface ResponseSummaryRepository extends JpaRepository<SearchResponse, Integer> {

    List<SearchResponse> findBySearchHistory(SearchHistory searchHistoryEntity);
}
