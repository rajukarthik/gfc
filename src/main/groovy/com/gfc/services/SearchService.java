/**
 *
 */
package com.gfc.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gfc.client.WatsonClient;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.domain.ClusterRankingEntity;
import com.gfc.models.domain.SearchHistory;
import com.gfc.models.domain.SearchResponse;
import com.gfc.models.dto.ResponseSummaryDto;
import com.gfc.models.dto.response.SearchDto;
import com.gfc.repositories.ConfigurationRepository;
import com.gfc.repositories.SearchHistoryRepository;
import com.gfc.response.ClusterDetails;
import com.gfc.response.Docs;
import com.gfc.response.RankingDetails;
import com.gfc.response.WatsonResponse;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.LoginActivity;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.UserRepository;
import com.gfc.users.accounts.repositories.services.LoginActivityRepositoryService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Shantanu Singh
 * This service is used to search questions from IBM watson.
 */
@Service
public class SearchService {

    private WatsonClient watsonClient;

    private LoginActivityRepositoryService loginActivityRepositoryService;

    public UserRepository personRepository;

    private SearchHistoryRepository searchHistoryRepository;

    private ResponseSummaryRepository responseSummaryRepository;

    private List<ResponseSummaryDto> responseSummaryDtos;

    private ClusterRankingService clusterRankingService;

    private ConfigurationRepository configurationRepository;

    private boolean flag = false;

    private volatile int attempts = 0;

    private int actualAttepmts;

    int anotherCount = 0;

    @Autowired
    public SearchService(WatsonClient watsonClient,
                         LoginActivityRepositoryService loginActivityRepositoryService, UserRepository personRepository,
                         SearchHistoryRepository searchHistoryRepository,
                         ResponseSummaryRepository responseSummaryRepository, ClusterRankingService clusterRankingService, ConfigurationRepository configurationRepository) {
        this.watsonClient = watsonClient;
        this.loginActivityRepositoryService = loginActivityRepositoryService;
        this.personRepository = personRepository;
        this.searchHistoryRepository = searchHistoryRepository;
        this.responseSummaryRepository = responseSummaryRepository;
        this.clusterRankingService = clusterRankingService;
        this.configurationRepository = configurationRepository;
    }

    /**
     * Method used to find login User details
     *
     * @param authToken
     * @return
     * @throws PersonNotFoundException
     */
    private User getUser(String authToken) throws PersonNotFoundException {
        LoginActivity loginActivity = loginActivityRepositoryService.findByAuthToken(authToken);
        User people = loginActivity.getPeople();

        return people;
    }

    /**
     * Method used to get User from email.
     *
     * @param user
     * @return
     */
    private String getUserFromEmail(User user) {
        String email = user.getEmail();
        String userFromEmail = StringUtils.substringBefore(email, "@");

        return userFromEmail;
    }

    int getActualAttepmts() {
        return Integer.parseInt(configurationRepository.findByCode(1).getInfo());
    }

    /**
     * Method is responsible for checking cluster status, fetch cluster and its ranker details
     * and saving them in database.
     *
     * @param type
     * @param question
     * @return
     * @throws PersonNotFoundException
     */
    public SearchDto process(String type, String authToken, String question) throws Exception {
        String activeClusterId = null;
        String rankId = null;
        SearchDto searchData = new SearchDto();
        
        //Check if IBM watson cluster is ready.
        Optional<ClusterDetails> clusterIsReady = clusterRankingService.isClusterReady();
        if (clusterIsReady.isPresent()) {
            ClusterDetails clusterDetails = clusterIsReady.get();
            activeClusterId = clusterDetails.getSolrClusterId();
        }
        
        //Fetch all rankers from IBM watson
        List<RankingDetails> rankers = clusterRankingService.getRankers();
        
        for(RankingDetails ranker : rankers) {
        	try {
        		//Fetch data from IBM watson, process it and save in database.
        		searchData = process(type, question, activeClusterId, ranker.getRankerId(), authToken);
        		break;
        	} catch(Exception ex) {
        		ex.printStackTrace();
        	}        	
        }
        return searchData;
    }

    /**
     * Method is responsible for parsing Reserve and Rank response and storing
     * them database.
     *
     * @param type
     * @param question
     * @return
     * @throws PersonNotFoundException
     */
    public SearchDto process(String type, String question, String activeClusterId, String rankId, String authToken) throws PersonNotFoundException {

        responseSummaryDtos = new ArrayList<ResponseSummaryDto>();
        List<SearchResponse> responseSummaries = new ArrayList<SearchResponse>();

        User people = getUser(authToken);


        WatsonResponse response = null;
        
        //This method is used to fetch all relevant answers for the entered question from IBM watson
        response = watsonClient.question(activeClusterId, rankId, type, question);
        List<Docs> docList = response.getResponse().getDocs();

        ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter();
        String jsonInString = null;

        try {
            jsonInString = mapper.writeValueAsString(response.getResponse());
        } catch (JsonProcessingException e) {
            System.out.println(e.getStackTrace());
        }

        //Check if exact question is asked twice in order to prevent duplicacy and get latest relevant answers for the entered question.
        SearchHistory searchHistory = searchHistoryRepository.findByQuestionAndPeopleAndStatus(question, people, ActivityStatus.ACTIVE);
        if (null != searchHistory) {
            searchHistory.setUpdatedDt(new Date());
            searchHistory.setUpdatedBy(getUserFromEmail(people));
            searchHistory.setStatus(ActivityStatus.INACTIVE);
            searchHistoryRepository.saveAndFlush(searchHistory);
        }

        searchHistory = new SearchHistory();
        searchHistory.setQuestion(question);
        searchHistory.setResponse(jsonInString);
        searchHistory.setCreatedDt(new Date());
        searchHistory.setUpdatedDt(new Date());
        searchHistory.setUpdatedBy(getUserFromEmail(people));
        searchHistory.setCreatedBy(getUserFromEmail(people));
        searchHistory.setPeople(people);
        searchHistory.setStatus(ActivityStatus.ACTIVE);

        searchHistory = searchHistoryRepository.saveAndFlush(searchHistory);
        populateResponse(docList, responseSummaryDtos, responseSummaries, searchHistory);

        Collections.sort(responseSummaryDtos);

        SearchDto searchDto = new SearchDto();
        searchDto.setId(searchHistory.getId());
        searchDto.setResponses(responseSummaryDtos);

        return searchDto;        
    }

    /**
     * This method is used to process and populate relevant answers in database table.
     * @param docList
     * @param responseSummaryDtos
     * @param responseSummaries
     * @param searchHistory
     */
    private void populateResponse(List<Docs> docList, List<ResponseSummaryDto> responseSummaryDtos,
                                  List<SearchResponse> responseSummaries, SearchHistory searchHistory) {
        int count = 0;
        Iterator<Docs> iterator = docList.iterator();

        while (iterator.hasNext()) {
            Docs docs = iterator.next();
            SearchResponse responseSummary = new SearchResponse();

            responseSummary.setSectionSequenceId(docs.getSSID());
            responseSummary.setSectionSequenceTitle(docs.getSSTitle());
            responseSummary.setChapterTitle(docs.getChapterTitle());
            responseSummary.setSectionTitle(docs.getSectionTitle());
            responseSummary.setSectionSequenceId1(docs.getSS1ID());
            responseSummary.setSectionSequenceTitle1(docs.getSS1Title());
            responseSummary.setSectionSequenceId2(docs.getSS2ID());
            responseSummary.setSectionSequenceTitle2(docs.getSS2Title());
            responseSummary.setBody(docs.getBody());
            responseSummary.setVersion(docs.get_version_());
            responseSummary.setScore(docs.getScore());
            responseSummary.setFeatureVector(docs.getFeatureVector());
            responseSummary.setSearchHistory(searchHistory);
            responseSummaries.add(responseSummary);

            responseSummaryRepository.save(responseSummary);

            ResponseSummaryDto responseSummaryDto = new ResponseSummaryDto();

            responseSummaryDto.setIdWatson(count);
            responseSummaryDto.setSnip(docs.getBody());
            responseSummaryDto.setConfidence(docs.getScore());
            responseSummaryDto.setAnswer(docs.getBody());

            String sectionNumber = docs.getSSID();

            if (StringUtils.isNotBlank(sectionNumber)) {
                responseSummaryDto.setSectionNumber(sectionNumber);
                responseSummaryDto.setHyperlink(createHyperlink(sectionNumber, responseSummaryDto));
                responseSummaryDto.setSectionName(docs.getSSTitle());

                responseSummaryDtos.add(responseSummaryDto);
            }

            count++;
        }
    }

    /**
     * This method is used to create hyperlink for the selected page.
     * @param sectionNumber
     * @param responseSummary
     * @return
     */
    private String createHyperlink(String sectionNumber, ResponseSummaryDto responseSummary) {
        String hyperlink = null;
        int location1 = sectionNumber.indexOf(".");
        String chapter = sectionNumber.substring(0, location1);

        // to get chapter we need to see if it is single digit or double single
        if (chapter.length() == 3) {
            hyperlink = "/chapter/chapter0" + chapter.substring(0, 1) + ".html#" + sectionNumber;
            responseSummary.setChapterNo(chapter.substring(0, 1));
        } else if (chapter.length() == 4) { // double
            hyperlink = "/chapter/chapter" + chapter.substring(0, 2) + ".html#" + sectionNumber;
            responseSummary.setChapterNo(chapter.substring(0, 2));
        }

        return hyperlink;
    }
    
    /**
     * This method is used to ingest data in IBM watson ranker for re-ranking the results.
     */
    public void ingestDataInRanker() {
        String activeClusterId = null;
        String rankerId = null;
        ClusterRankingEntity clusterRankingEntity = null;
        
        clusterRankingEntity = clusterRankingService.findActiveCluster();
        if (null == clusterRankingEntity) {
            Optional<ClusterDetails> clusterIsReady = clusterRankingService.isClusterReady();

            if (clusterIsReady.isPresent()) {
                ClusterDetails clusterDetails = clusterIsReady.get();
                activeClusterId = clusterDetails.getSolrClusterId();
                rankerId = clusterRankingService.getLatestRankId();
            }
            clusterRankingService.saveGfcClusterRanking(rankerId, ActivityStatus.ACTIVE, activeClusterId);       	
        } else {
        	rankerId = clusterRankingEntity.getRankerId();
        }
        try {
			clusterRankingService.ingest(clusterRankingEntity.getSolrClusterId(), rankerId);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}

