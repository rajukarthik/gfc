/**
 *
 */
package com.gfc.services;

import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.domain.BookmarkEntity;
import com.gfc.models.dto.request.BookmarkDetailsDto;
import com.gfc.repositories.BookmarkRepository;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.LoginActivity;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.services.LoginActivityRepositoryService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author Shantanu Singh
 * It is a service for bookmarking.
 */
@Service
public class BookmarkService {

    private LoginActivityRepositoryService loginActivityRepositoryService;

    private BookmarkRepository bookmarkRepository;

    @Autowired
    public BookmarkService(LoginActivityRepositoryService loginActivityRepositoryService, BookmarkRepository bookmarkRepository) {
        this.loginActivityRepositoryService = loginActivityRepositoryService;
        this.bookmarkRepository = bookmarkRepository;
    }

    /**
     * Method used for find login User details
     *
     * @param authToken
     * @return User
     * @throws PersonNotFoundException
     */
    private User getUser(String authToken) throws PersonNotFoundException {
        LoginActivity loginActivity = loginActivityRepositoryService.findByAuthToken(authToken);
        User people = loginActivity.getPeople();

        return people;
    }

    /**
     * Method used to get User from email.
     *
     * @param user
     * @return
     */
    private String getUserFromEmail(String email) {
        String userFromEmail = StringUtils.substringBefore(email, "@");

        return userFromEmail;
    }

    /**
     * This method is used to save bookmarking details in database for the logged-in user.
     * @param authToken
     * @param bookmarkDetailsDto
     * @return
     * @throws PersonNotFoundException
     */
    public BookmarkDetailsDto addBookmark(String authToken, BookmarkDetailsDto bookmarkDetailsDto)
            throws PersonNotFoundException {
        User people = getUser(authToken);
        String userName = getUserFromEmail(people.getEmail());
        String sectionSequenceId = bookmarkDetailsDto.getSectionSequenceId();

        //Used to check if same section is already bookmarked or not.
        BookmarkEntity bookmarkEntity = bookmarkRepository.findBySectionSequenceIdAndPeople(sectionSequenceId, people);

        if (bookmarkEntity == null) {
            bookmarkEntity = new BookmarkEntity();
            bookmarkEntity.setPeople(people);
            bookmarkEntity.setSectionSequenceId(sectionSequenceId);
            bookmarkEntity.setSectionSequenceTitle(bookmarkDetailsDto.getSectionSequenceTitle());
            bookmarkEntity.setStatus(ActivityStatus.ACTIVE);
            bookmarkEntity.setCreatedBy(userName);
            bookmarkEntity.setUpdatedBy(userName);
            bookmarkEntity.setCreatedDt(new Date());
            bookmarkEntity.setUpdatedDt(new Date());

            bookmarkEntity = bookmarkRepository.saveAndFlush(bookmarkEntity);
        } else {

            bookmarkEntity.setStatus(ActivityStatus.ACTIVE);
            bookmarkEntity.setUpdatedDt(new Date());
            bookmarkEntity = bookmarkRepository.saveAndFlush(bookmarkEntity);
        }

        BookmarkDetailsDto bookmarkDto = createBookmarkDto(bookmarkEntity);

        return bookmarkDto;
    }

    /**
     * This method is used to create bookmark DTO from bookmark entity.
     * @param bookmark
     * @return
     */
    private BookmarkDetailsDto createBookmarkDto(BookmarkEntity bookmark) {
        BookmarkDetailsDto bookmarkDetailsDto = new BookmarkDetailsDto();

        bookmarkDetailsDto.setId(bookmark.getId());
        bookmarkDetailsDto.setUpdatedDt(bookmark.getUpdatedDt());
        bookmarkDetailsDto.setSectionSequenceId(bookmark.getSectionSequenceId());
        bookmarkDetailsDto.setSectionSequenceTitle(bookmark.getSectionSequenceTitle());
        bookmarkDetailsDto.setHyperlink(createHyperlink(bookmark.getSectionSequenceId()));

        return bookmarkDetailsDto;
    }

    /**
     * This method is used to update selected bookmark.
     * @param id
     * @return
     */
    public boolean updateBookmark(Long id) {
        BookmarkEntity bookmark = bookmarkRepository.findById(id);

        if (bookmark != null && bookmark.getId() != null) {
            bookmark.setStatus(ActivityStatus.INACTIVE);
            bookmark.setUpdatedDt(new Date());
            bookmark = bookmarkRepository.saveAndFlush(bookmark);
            return true;
        }

        return false;
    }

    /**
     * This method is used to fetch bookmarking details from database for the logged-in user.
     * @param authToken
     * @return
     * @throws PersonNotFoundException
     */
    public List<BookmarkDetailsDto> getBookmarkList(String authToken) throws PersonNotFoundException {
        User people = getUser(authToken);
        List<BookmarkEntity> bookmarkList = bookmarkRepository.findByStatusAndPeopleOrderByUpdatedDtDesc(ActivityStatus.ACTIVE, people);
        List<BookmarkDetailsDto> bookmarkDetailList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(bookmarkList)) {
            Iterator<BookmarkEntity> iterator = bookmarkList.iterator();

            while (iterator.hasNext()) {
                BookmarkEntity bookmark = iterator.next();

                BookmarkDetailsDto bookmarkDto = new BookmarkDetailsDto();
                bookmarkDto.setId(bookmark.getId());
                bookmarkDto.setSectionSequenceId(bookmark.getSectionSequenceId());
                bookmarkDto.setSectionSequenceTitle(bookmark.getSectionSequenceTitle());
                bookmarkDto.setHyperlink(createHyperlink(bookmark.getSectionSequenceId()));
                bookmarkDto.setUpdatedDt(bookmark.getUpdatedDt());

                bookmarkDetailList.add(bookmarkDto);
            }
        }

        return bookmarkDetailList;
    }

    /**
     * This method is used to create hyperlink from section sequence id.
     * @param sectionId
     * @return
     */
    private String createHyperlink(String sectionId) {
        String hyperlink = null;
        int location1 = sectionId.indexOf(".");
        String chapter = sectionId.substring(0, location1);

        // to get chapter we need to see if it is single digit or double single
        if (chapter.length() == 3) {
            hyperlink = "/chapter/chapter0" + chapter.substring(0, 1) + ".html#" + sectionId;
        } else if (chapter.length() == 4) { // double
            hyperlink = "/chapter/chapter" + chapter.substring(0, 2) + ".html#" + sectionId;
        }

        return hyperlink;
    }

    /**
     * This method is used to check if bookmark already exists for selected section or not.
     * @param id
     * @return
     */
    public BookmarkDetailsDto checkBookmark(String authToken, String ssid) throws PersonNotFoundException {
        BookmarkDetailsDto bookmarkDto = new BookmarkDetailsDto();
        bookmarkDto.setId(-1L);

        User people = getUser(authToken);
        String userName = getUserFromEmail(people.getEmail());
        BookmarkEntity bookmark = bookmarkRepository.findBySectionSequenceIdAndPeople(ssid, people);
        if (null != bookmark) {
            bookmarkDto.setId(bookmark.getId());
        }
        return bookmarkDto;
    }
}