package com.gfc.services;

import com.gfc.client.ClusterClient;
import com.gfc.client.RankerClient;
import com.gfc.models.domain.ClusterRankingEntity;
import com.gfc.repositories.services.ClusterRankingRepositoryService;
import com.gfc.response.ClusterDetails;
import com.gfc.response.ClusterResponse;
import com.gfc.response.RankerResponse;
import com.gfc.response.RankingDetails;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.activities.services.RatingService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by gurinder on 28/1/16.
 * It is a service for IBM watson ranker
 */

@Service
public class ClusterRankingService {

    private ClusterClient clusterClient;

    private RankerClient rankerClient;

    private RatingService ratingService;
    
    private ClusterRankingRepositoryService clusterRankingRepositoryService;

    @Autowired
    public ClusterRankingService(ClusterClient clusterClient, RankerClient rankerClient, RatingService ratingService, ClusterRankingRepositoryService clusterRankingRepositoryService) {
        this.clusterClient = clusterClient;
        this.rankerClient = rankerClient;
        this.clusterRankingRepositoryService = clusterRankingRepositoryService;
        this.ratingService = ratingService;
    }

    /**
     * This method is used to check if IBM watson cluster is ready.
     * @return
     */
    public Optional<ClusterDetails> isClusterReady() {
        boolean isClusterReady = false;
        Optional<ClusterDetails> activeCluster = null;
        ClusterResponse clusterResponse = clusterClient.checkClusterStatus();
        List<ClusterDetails> clusters = clusterResponse.getClusters();
        if (CollectionUtils.isNotEmpty(clusters)) {
            isClusterReady = clusters.stream().anyMatch(clusterDetails -> clusterDetails.getSolrClusterStatus().equals("READY"));

            if (isClusterReady)
                activeCluster = clusters.stream().findFirst();

        }

        return activeCluster;
    }

    /**
     * This method is used to get all IBM watson rankers whose status is available..
     * @return
     */
    public List<RankingDetails> getRankers() {
        RankerResponse rankerResponse = rankerClient.getAllRankers();
        List<RankingDetails> rankers = rankerResponse.getRankers();
        Iterator itr = rankers.iterator();
        
        List<RankingDetails> activeRankers = new ArrayList<>();
        
        while(itr.hasNext()) {
        	String rankerId = ((RankingDetails) itr.next()).getRankerId();
        	RankingDetails ranker = rankerClient.getRankerDetail(rankerId);
        	if( !ranker.getStatus().equalsIgnoreCase("Available")) {
        		itr.remove();
        		continue;
        	}
        	activeRankers.add(ranker);
        }
        return activeRankers;
    }
    
    /**
     * This method is used to find latest ranker id.
     * @return
     */
    public String getLatestRankId() {
        RankingDetails rankingByLatestDate = null;
        String rankId = null;
        RankerResponse rankerResponse = rankerClient.getAllRankers();
        List<RankingDetails> rankers = rankerResponse.getRankers();
        Iterator itr = rankers.iterator();
        
        List<RankingDetails> activeRankers = new ArrayList<>();
        
        while(itr.hasNext()) {
        	String rankerId = ((RankingDetails) itr.next()).getRankerId();
        	RankingDetails ranker = rankerClient.getRankerDetail(rankerId);
        	if( !ranker.getStatus().equalsIgnoreCase("Available")) {
        		itr.remove();
        		continue;
        	}
        	activeRankers.add(ranker);
        }
        if (CollectionUtils.isNotEmpty(activeRankers)) {
            rankingByLatestDate = Collections.max(activeRankers, Comparator.comparing(RankingDetails::getCreatedDate));
            rankId = rankingByLatestDate.getRankerId();
        }
        return rankId;
    }

    /**
     * This method is used to save IBM watson solr cluster and ranking details in database.
     * @param rankId
     * @param active
     * @param activeClusterId
     * @return
     */
    public ClusterRankingEntity saveGfcClusterRanking(String rankId, ActivityStatus active, String activeClusterId) {
        List<ClusterRankingEntity> clusterRankingEntityList = clusterRankingRepositoryService.findAll();
        if (CollectionUtils.isNotEmpty(clusterRankingEntityList)) {
            clusterRankingEntityList.stream().forEach(g -> g.setStatus(ActivityStatus.INACTIVE));
        }
        ClusterRankingEntity clusterRankingEntity = new ClusterRankingEntity();
        clusterRankingEntity.setRankerId(rankId);
        clusterRankingEntity.setStatus(active);
        clusterRankingEntity.setSolrClusterId(activeClusterId);
        clusterRankingEntity = clusterRankingRepositoryService.save(clusterRankingEntity);
        return clusterRankingEntity;
    }

    /**
     * This method is used to fetch active cluster from database.
     * @return
     */
    public ClusterRankingEntity findActiveCluster() {
        ClusterRankingEntity activeGfcCluster = clusterRankingRepositoryService.findByStatus(ActivityStatus.ACTIVE);
        return activeGfcCluster;
    }
    
    /**
     * This method is used to ingest data in IBM watson ranker for re-ranking.
     * @param clusterId
     * @param rankerId
     */
    public void ingest(String clusterId, String rankerId) {
    	ratingService.ingestDataInWatson(clusterId, rankerId);
    }
    
    /**
     * This method is used to delete IBM watson solr cluster details from database.
     * @param clusterRankingEntity
     */
    public void deleteWatsonSolrCluster(ClusterRankingEntity clusterRankingEntity) {
    	clusterRankingRepositoryService.deleteWatsonSolrCluster(clusterRankingEntity);
    }
}
