package com.gfc.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.enums.SearchStatus;
import com.gfc.exceptions.HistoryNotFoundException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.dto.response.AnswerResponseDto;
import com.gfc.models.dto.response.QuestionResponseDto;
import com.gfc.repositories.services.HistoryRepositoryService;
import com.gfc.users.accounts.models.entities.LoginActivity;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.LoginActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by gurinder on 12/1/16.
 * It is a service for recent searches or search history.
 */
@Service
public class HistoryService {

    private HistoryRepositoryService repositoryService;

    private LoginActivityRepository loginActivityRepository;

    @Autowired
    public HistoryService(HistoryRepositoryService repositoryService, LoginActivityRepository loginActivityRepository) {
        this.repositoryService = repositoryService;
        this.loginActivityRepository = loginActivityRepository;
    }

    /**
     * This method is used to get list of questions from database searched by logged-in user.
     * @param authToken
     * @return
     * @throws HistoryNotFoundException
     * @throws PersonNotFoundException
     */
    public List<QuestionResponseDto> getQuestions(String authToken) throws HistoryNotFoundException, PersonNotFoundException {

        LoginActivity currentUser = loginActivityRepository.findByAuthToken(authToken);
        if (currentUser == null) {
            throw new HistoryNotFoundException(SearchStatus.INVALID_AUTH_TOKEN.getStatus());
        }
        User loggedInUser = currentUser.getPeople();

        if (loggedInUser == null) {
            throw new PersonNotFoundException(ResponseStatus.PERSON_NOT_FOUND.getStatus());
        }
        List<QuestionResponseDto> allQuestionsInfo = repositoryService.getAllQuestions(loggedInUser);

        return allQuestionsInfo;
    }

    /**
     * This method is used to get all answers from database for the selected question from recent searches.
     * @param id
     * @param authToken
     * @return
     * @throws HistoryNotFoundException
     */
    public List<AnswerResponseDto> getAnswers(Long id, String authToken) throws HistoryNotFoundException {
        LoginActivity currentUser = loginActivityRepository.findByAuthToken(authToken);
        if (currentUser == null) {
            throw new HistoryNotFoundException(SearchStatus.INVALID_AUTH_TOKEN.getStatus());
        }
        List<AnswerResponseDto> answerList = repositoryService.getAnswer(id);
        return answerList;
    }
}
