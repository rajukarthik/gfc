package com.gfc.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.enums.SearchStatus;
import com.gfc.exceptions.HistoryNotFoundException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.dto.MessageDto;
import com.gfc.models.dto.request.HighLightDetailsDto;
import com.gfc.repositories.services.HighlightRepositoryService;
import com.gfc.users.accounts.models.entities.LoginActivity;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.LoginActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by gurinder on 19/1/16.
 * It is a service for highlight text.
 */
@Service
public class HighlightService {

    private LoginActivityRepository loginActivityRepository;

    private HighlightRepositoryService highlightRepositoryService;

    @Autowired
    public HighlightService(LoginActivityRepository loginActivityRepository, HighlightRepositoryService highlightRepositoryService) {
        this.loginActivityRepository = loginActivityRepository;
        this.highlightRepositoryService = highlightRepositoryService;
    }

    /*
     * This method is used to save highlight details for the logged-in user.
     */
    public MessageDto addChapterIndexes(String authToken, HighLightDetailsDto request) throws HistoryNotFoundException, PersonNotFoundException {
        LoginActivity currentUser = loginActivityRepository.findByAuthToken(authToken);
        if (currentUser == null) {
            throw new HistoryNotFoundException(SearchStatus.INVALID_AUTH_TOKEN.getStatus());
        }
        User loggedInUser = currentUser.getPeople();

        if (loggedInUser == null) {
            throw new PersonNotFoundException(ResponseStatus.PERSON_NOT_FOUND.getStatus());
        }

        MessageDto messageDto = highlightRepositoryService.addChapter(loggedInUser, request);

        return messageDto;
    }

    /**
     * This method is used to fetch all highlights for the logged-in user.
     * @param authToken
     * @param chapterNo
     * @return
     * @throws HistoryNotFoundException
     */
    public List<HighLightDetailsDto> getChapterIndexes(String authToken, String chapterNo) throws HistoryNotFoundException{
        LoginActivity currentUser = loginActivityRepository.findByAuthToken(authToken);
        if (currentUser == null) {
            throw new HistoryNotFoundException(SearchStatus.INVALID_AUTH_TOKEN.getStatus());
        }

        final List<HighLightDetailsDto> highLightIndexes = highlightRepositoryService.getChapter(chapterNo, currentUser.getPeople());
        return highLightIndexes;
    }

    /**
     * This method is used to delete selected highlight.
     * @param authToken
     * @param request
     * @return
     * @throws HistoryNotFoundException
     */
    public MessageDto deleteChapterIndexes(String authToken, HighLightDetailsDto request) throws HistoryNotFoundException {
        LoginActivity currentUser = loginActivityRepository.findByAuthToken(authToken);
        if (currentUser == null) {
            throw new HistoryNotFoundException(SearchStatus.INVALID_AUTH_TOKEN.getStatus());
        }
        MessageDto messageDto = highlightRepositoryService.deleteChapterIndexes(request);

        return messageDto;
    }
}
