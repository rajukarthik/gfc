package com.gfc.controllers;

import com.gfc.constants.Constant;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.dto.response.SearchDto;
import com.gfc.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by gurinder on 2/1/16.
 * This controller is used to search question from IBM watson.
 */
@RequestMapping("/api/secure")
@RestController
public class SearchController {

    private SearchService searchService;

    @Value(value = "${watson.search.wt-type}")
    String type;

    @Autowired
    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    /**
     * It is used to fetch all relevant answers from IBM watson for the selected question.
     * @param question
     * @param authToken
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/ask", method = RequestMethod.GET)
    public ResponseEntity<SearchDto> ask(@RequestParam("question") String question, @RequestHeader(Constant.AUTH_TOKEN) String authToken) throws Exception {
        SearchDto searchDto = null;

        try {
            searchDto = searchService.process(type, authToken, question);
        } catch (PersonNotFoundException e) {
            new ResponseEntity<SearchDto>(HttpStatus.UNAUTHORIZED);
        }

        return new ResponseEntity<SearchDto>(searchDto, HttpStatus.OK);
    }
}