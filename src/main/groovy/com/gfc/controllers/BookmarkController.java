/**
 * 
 */
package com.gfc.controllers;

import com.gfc.constants.Constant;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.dto.request.BookmarkDetailsDto;
import com.gfc.services.BookmarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Shantanu Singh
 * This controller is used to maintain bookmarks of logged-in user.
 */
@RequestMapping("/api/secure")
@RestController
public class BookmarkController {

	private BookmarkService bookmarkService;
	
	@Autowired
	public BookmarkController(BookmarkService bookmarkService) {
		this.bookmarkService = bookmarkService;
	}

	/**
	 * It is used to add bookmark of the logged-in user.
	 * @param authToken
	 * @param bookmarkDetailsDto
	 * @return
	 * @throws IOException
     */
	@RequestMapping(value = "/bookmark/add", method = RequestMethod.POST)
	public ResponseEntity<BookmarkDetailsDto> addBookmark(@RequestHeader(Constant.AUTH_TOKEN) String authToken,
			@RequestBody BookmarkDetailsDto bookmarkDetailsDto)
					throws IOException {
		BookmarkDetailsDto bookmarkDto = null;

		try {
			bookmarkDto = bookmarkService.addBookmark(authToken, bookmarkDetailsDto);
		} catch (PersonNotFoundException e) {
            return new ResponseEntity<BookmarkDetailsDto>(HttpStatus.UNAUTHORIZED);
		}

		return new ResponseEntity<BookmarkDetailsDto>(bookmarkDto, HttpStatus.OK);
	}

	/**
	 * It is used to delete selected bookmark.
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/bookmark/remove/{id}", method = RequestMethod.PUT)
	public boolean removeBookmarks(@PathVariable("id") Long id) {
		return bookmarkService.updateBookmark(id);
	}

	/**
	 * It is used to check whether selected section is bookmarked or not.
	 * @param authToken
	 * @param ssid
	 * @return
	 */
	@RequestMapping(value = "/bookmark/check", method = RequestMethod.GET)
	public ResponseEntity<BookmarkDetailsDto> checkBookmark(@RequestHeader(Constant.AUTH_TOKEN) String authToken, @RequestParam("sectionSequenceId") String ssid) {
		BookmarkDetailsDto bookmarkDto = null;
		try {
			bookmarkDto = bookmarkService.checkBookmark(authToken, ssid);
		} catch (PersonNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<>(bookmarkDto, HttpStatus.OK);
	}
	
	/**
	 * It is used to fetch all bookmarks of the logged-in user.
	 * @param authToken
	 * @return
	 */
	@RequestMapping(value = "/bookmark", method = RequestMethod.GET)
	public ResponseEntity<List<BookmarkDetailsDto>> getBookmarks(@RequestHeader(Constant.AUTH_TOKEN) String authToken) {
		List<BookmarkDetailsDto> bookmarkList = new ArrayList<>();
		
		try {
			bookmarkList = bookmarkService.getBookmarkList(authToken);
		} catch (PersonNotFoundException e) {
            return new ResponseEntity<List<BookmarkDetailsDto>>(HttpStatus.UNAUTHORIZED);
		}

		return new ResponseEntity<List<BookmarkDetailsDto>>(bookmarkList, HttpStatus.OK);
	}
}
