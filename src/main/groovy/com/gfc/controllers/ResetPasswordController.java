package com.gfc.controllers;

import com.gfc.exceptions.EmailFailureException;
import com.gfc.exceptions.InvalidActivationException;
import com.gfc.models.dto.request.ResetPasswordRequestDto;
import com.gfc.services.ResetPasswordService;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by Shantanu Singh on 05-Mar-16.
 * This controller is used to reset password either from forgot password or my account.
 */
@RestController
@RequestMapping("/api/people")
public class ResetPasswordController {

    private ResetPasswordService resetPasswordService;
    private static final Logger logger = LoggerFactory.getLogger(ResetPasswordController.class);

    @Autowired
    public ResetPasswordController(ResetPasswordService resetPasswordService) {
        this.resetPasswordService = resetPasswordService;
    }

    /**
     * It is used to send forgot password mail to registered email address of the logged-in user.
     * @param resetPasswordRequestDto
     * @return
     */
    @RequestMapping(value = "/resetPassword/sendResetPasswordEmail", method = RequestMethod.PUT)
    public ResponseEntity sendResetPasswordEmail(@RequestBody ResetPasswordRequestDto resetPasswordRequestDto) {
        ResponseDto response = null;

        try {
            response = resetPasswordService.sendResetPasswordEmail(resetPasswordRequestDto);
        } catch (EmailFailureException e) {
            logger.info("Reset password process failed");
            logger.error(e.getMessage());
            response = new ResponseDto(e.getMessage(), com.gfc.enums.ResponseStatus.INTERNAL_ERROR, null, null, null);

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
            response = new ResponseDto("Reset password process failed", com.gfc.enums.ResponseStatus.INVALID_REQUEST,
                    null, null,null);

            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            response = new ResponseDto("Reset password process failed", com.gfc.enums.ResponseStatus.INTERNAL_ERROR, null, null,null);

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    /**
     * It is used to reset password from reset user's password.
     * @param resetPasswordRequestDto
     * @param token
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/resetPassword", method = RequestMethod.PUT)
    public ResponseEntity resetPassword(@RequestBody ResetPasswordRequestDto resetPasswordRequestDto,
                                        @RequestParam(value = "token", required = true) String token) throws IOException {
        ResponseDto response = null;

        //Server-side password match validation. Throws an error if password mismatch.
        if (!(resetPasswordRequestDto.getPassword().equals(resetPasswordRequestDto.getRepeatPassword()))) {
            response = new ResponseDto("Password Mismatch", com.gfc.enums.ResponseStatus.PASSWORD_MISMATCH, null, null, null);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            response = resetPasswordService.resetPassword(resetPasswordRequestDto, token);
        } catch (InvalidActivationException e) {
            response = new ResponseDto("Invalid request", com.gfc.enums.ResponseStatus.INVALID_REQUEST, null, null, null);

            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            response = new ResponseDto("Reset password process failed", com.gfc.enums.ResponseStatus.INTERNAL_ERROR, null, null, null);

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);
    }
}
