package com.gfc.controllers;

import com.gfc.constants.Constant;
import com.gfc.enums.ResponseStatus;
import com.gfc.enums.SearchStatus;
import com.gfc.exceptions.HistoryNotFoundException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.dto.response.AnswerResponseDto;
import com.gfc.models.dto.response.QuestionResponseDto;
import com.gfc.services.HistoryService;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by gurinder on 11/1/16.
 * This controller is used for Search history.
 */
@RestController
@RequestMapping("/api/secure/history")
public class SearchHistoryController {

    private HistoryService historyService;
    private static final Logger logger = LoggerFactory.getLogger(SearchHistoryController.class);


    @Autowired
    public SearchHistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    /**
     * It is used to fetch all answers searched by logged-in user.
     * @param authToken
     * @return
     */
    @RequestMapping(value = "/question", method = RequestMethod.GET)
    public ResponseEntity getUserQuestionsHistory(@RequestHeader(Constant.AUTH_TOKEN) String authToken) {
        logger.info("Request recieved for questions history");
        List<QuestionResponseDto> responseDto = null;
        ResponseDto response = null;
        try {
            responseDto = historyService.getQuestions(authToken);
            return new ResponseEntity(responseDto, HttpStatus.OK);
        } catch (HistoryNotFoundException e) {
            logger.error(e.getMessage());
            response = new ResponseDto(
                    e.getMessage(),
                    null,
                    null, SearchStatus.NO_HISTORY_FOUND,null
            );
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (PersonNotFoundException e) {
            logger.error(e.getMessage());
            response = new ResponseDto(
                    e.getMessage(),
                    ResponseStatus.PERSON_NOT_FOUND,
                    null, null,null
            );
            return new ResponseEntity(response, HttpStatus.OK);
        }
    }

    /**
     * It is used to fetch all answers found for the selected question.
     * @param authToken
     * @param id
     * @return
     * @throws HistoryNotFoundException
     */
    @RequestMapping(value = "/answer", method = RequestMethod.GET)
    public ResponseEntity getUserAnswerHistory(@RequestHeader(Constant.AUTH_TOKEN) String authToken, @RequestParam("id") Long id) throws HistoryNotFoundException {
        ResponseDto response = null;
        List<AnswerResponseDto> responseDto = null;
        try {
            responseDto = historyService.getAnswers(id, authToken);
            return new ResponseEntity(responseDto, HttpStatus.OK);
        } catch (HistoryNotFoundException e) {
            logger.error(e.getMessage());
            response = new ResponseDto(
                    e.getMessage(),
                    null,
                    null, SearchStatus.NO_HISTORY_FOUND,null
            );
            return new ResponseEntity(response, HttpStatus.OK);
        }

    }


}
