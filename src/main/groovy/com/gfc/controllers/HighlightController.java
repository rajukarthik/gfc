package com.gfc.controllers;

import com.gfc.constants.Constant;
import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.HistoryNotFoundException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.dto.MessageDto;
import com.gfc.models.dto.request.HighLightDetailsDto;
import com.gfc.services.HighlightService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by gurinder on 19/1/16.
 * This controller is used to maintain highlights for the logged-in user.
 */
@RestController
@RequestMapping("/api/secure/highlight")
public class HighlightController {


    private HighlightService highlightService;

    @Autowired
    public HighlightController(HighlightService highlightService) {
        this.highlightService = highlightService;
    }

    /**
     * It is used to add text for logged-in user which user wants to highlight.
     * @param request
     * @param authToken
     * @return
     * @throws PersonNotFoundException
     * @throws HistoryNotFoundException
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity addChapter(
            @RequestBody HighLightDetailsDto request, @RequestHeader(Constant.AUTH_TOKEN) String authToken) throws PersonNotFoundException, HistoryNotFoundException {
        MessageDto messageDto = null;
        try {
            messageDto = highlightService.addChapterIndexes(authToken, request);
            return new ResponseEntity(messageDto, HttpStatus.ACCEPTED);
        } catch (PersonNotFoundException e) {
            messageDto.setMessage(e.getMessage());
            messageDto.setStatus(ResponseStatus.INTERNAL_ERROR);
            return new ResponseEntity(messageDto, HttpStatus.BAD_REQUEST);
        } catch (HistoryNotFoundException e) {
            messageDto.setMessage(e.getMessage());
            messageDto.setStatus(ResponseStatus.INTERNAL_ERROR);
            return new ResponseEntity(messageDto, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * It is used to fetch all highlights for the logged-in user.
     * @param authToken
     * @param chapterNo
     * @return
     * @throws HistoryNotFoundException
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity getChapterDetails(
            @RequestHeader(Constant.AUTH_TOKEN) String authToken, @RequestParam("chapter") String chapterNo) throws HistoryNotFoundException {
        List<HighLightDetailsDto> response = null;
        MessageDto messageDto = null;
        try {
            response = highlightService.getChapterIndexes(authToken, chapterNo);
            if (CollectionUtils.isEmpty(response)) {
                messageDto = new MessageDto("no indexes available", ResponseStatus.INTERNAL_ERROR);
                return new ResponseEntity(messageDto, HttpStatus.ACCEPTED);
            } else {
                return new ResponseEntity(response, HttpStatus.ACCEPTED);
            }
        } catch (HistoryNotFoundException e) {
            messageDto = new MessageDto(e.getMessage(), ResponseStatus.INTERNAL_ERROR);
            return new ResponseEntity(messageDto, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * It is used to delete selected highlight.
     * @param request
     * @param authToken
     * @return
     * @throws HistoryNotFoundException
     */
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity deleteChapterDetails(
            @RequestBody HighLightDetailsDto request, @RequestHeader(Constant.AUTH_TOKEN) String authToken) throws HistoryNotFoundException {
        MessageDto messageDto = null;
        try {
            messageDto = highlightService.deleteChapterIndexes(authToken, request);
            return new ResponseEntity(messageDto, HttpStatus.ACCEPTED);
        } catch (HistoryNotFoundException e) {
            messageDto.setMessage(e.getMessage());
            messageDto.setStatus(ResponseStatus.INTERNAL_ERROR);
            return new ResponseEntity(messageDto, HttpStatus.BAD_REQUEST);
        }
    }
}
