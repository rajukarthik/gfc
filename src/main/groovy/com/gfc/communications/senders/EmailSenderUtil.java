package com.gfc.communications.senders;

import com.gfc.communications.models.dto.EmailDto;
import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by abhinav on 2/4/16.
 * Mail sender  utility for sending mails.
 */
@Component
public class EmailSenderUtil {

    @Value("${mail.smtp.from}")
    private String senderEmail;

    private Session session;
    private MimeMessage message;

    @Autowired
    public EmailSenderUtil(Session session) {
        this.session = session;
    }

    public ResponseDto send(EmailDto emailDto){
        this.message = new MimeMessage(session);
        ResponseDto response = new ResponseDto();
        try {
            message.setFrom(new InternetAddress(senderEmail));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailDto.getEmail()));
            message.setSubject(emailDto.getSubject());
            message.setContent(emailDto.getBody(),emailDto.getContentType());
            Transport.send(message);
            System.out.println("*******************");
            System.out.println(emailDto.getType().getType() + " Mail sent");
            System.out.println("*******************");
            response.setMessage(ResponseStatus.SUCCESS.getStatus());
            response.setStatus(ResponseStatus.SUCCESS);
            response.setHttpStatus(HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            response.setMessage(emailDto.getType().getType()+"_"+ResponseStatus.EMAIL_SENDING_FAILED.getStatus());
            response.setStatus(ResponseStatus.INTERNAL_ERROR);
            response.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }
}
