package com.gfc.communications.contract.impl;

import com.gfc.communications.models.dto.CommunicationDto;
import com.gfc.communications.services.RefactoredEmailService;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by abhinav on 2/4/16.
 * It is used to
 */
@Component
public class CommunicationProcessor {

    private RefactoredEmailService emailService;

    @Autowired
    public CommunicationProcessor(RefactoredEmailService emailService) {
        this.emailService = emailService;
    }

    @FunctionalInterface
    private interface CommunicationContract {
        public ResponseDto send(CommunicationDto communication);
    }

    /**
     * A contract between mail sender process and mail communication for sending mails.
     */
    private CommunicationContract communicationContract
            = (CommunicationDto communication) -> {
        switch (communication.getType()){
            case EMAIL:
                switch (communication.getEmail().getType()) {
                    case ACTIVATION:
                        return emailService.sendActivation(communication.getEmail());
                    case CLARIFICATION:
                        return emailService.sendClarification(communication.getEmail());
                    case RESET_PASSWORD:
                        return emailService.sendResetPassword(communication.getEmail());
                    case CLARIFICATION_FEEDBACK:
                        return emailService.sendClarificationFeedback(communication.getEmail());
                    case INVITATION:
                        return emailService.sendInvitation(communication.getEmail());
                }
        }
        return null;
    };

    /**
     * This method works like a contract for sending mails depend on type of process.
     * @param communication
     * @return
     */
    public ResponseDto communicate(CommunicationDto communication){
        return communicationContract.send(communication);
    }





}
