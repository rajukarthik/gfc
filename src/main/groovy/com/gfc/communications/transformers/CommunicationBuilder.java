package com.gfc.communications.transformers;

import com.gfc.communications.enums.EmailType;
import com.gfc.communications.enums.MessageType;
import com.gfc.communications.models.dto.CommunicationDto;
import com.gfc.communications.models.dto.EmailDto;
import com.gfc.users.accounts.models.entities.User;

/**
 * Created by abhinav on 3/4/16.
 * A builder utility used to build email messages.
 */
public class CommunicationBuilder {


    @FunctionalInterface
    private static interface I_CommunicationBuilder{
        public CommunicationDto apply(User user, String token, EmailType type);
    }
    /**
     * This method is used to build mail message.
     */
    private static I_CommunicationBuilder EMAIL_BUILDER = (user, token, type) -> {
        CommunicationDto communication = new CommunicationDto();
        communication.setType(MessageType.EMAIL);
        EmailDto email = new EmailDto();
        email.setEmail(user.getEmail());
        email.setToken(token);
        email.setType(EmailType.INVITATION);
        email.setUserName(user.getFirstName());
        email.setPassword(user.getPassword());
        communication.setEmail(email);
        return communication;
    };

    public static CommunicationDto buildEmail(User user, String token, EmailType emailType){
      return EMAIL_BUILDER.apply(user,token,emailType);
    }
}
