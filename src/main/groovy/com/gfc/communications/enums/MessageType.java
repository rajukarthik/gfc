package com.gfc.communications.enums;

/**
 * Created by abhinav on 2/4/16.
 * Enum for different types of messages.
 */
public enum MessageType {
    EMAIL("EMAIL");

    private final String type;

    MessageType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
