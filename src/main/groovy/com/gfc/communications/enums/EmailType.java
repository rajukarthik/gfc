package com.gfc.communications.enums;

/**
 * Created by abhinav on 2/4/16.
 * Enum for different types of mails.
 */
public enum EmailType {
    ACTIVATION("ACTIVATION"),
    CLARIFICATION("CLARIFICATION"),
    CONFIRMATION("CONFIRMATION"),
    RESET_PASSWORD("RESET_PASSWORD"),
    CLARIFICATION_FEEDBACK("CLARIFICATION_FEEDBACK"),
    INVITATION("INVITATION");

    private final String type;

    EmailType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
