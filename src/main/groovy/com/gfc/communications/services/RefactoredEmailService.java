package com.gfc.communications.services;

import com.gfc.communications.models.dto.EmailDto;
import com.gfc.communications.senders.EmailSenderUtil;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.utils.EncryptUtils;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by abhinav on 1/4/16.
 * Mail service for creating mail messages and then forward to mail sender utility for sending mails.
 */
@Service
public class RefactoredEmailService {

    @Value("${mail.smtp.from}")
    private String senderEmail;

    @Value("${gfc.server.url}")
    private String baseUrI;

    @Value("${gfc.activation.api}")
    private String activation;

    @Value("${gfc.clarification.api}")
    private String clarification;

    @Value("${gfc.resetPassword.api}")
    private String resetPassword;

    private EmailSenderUtil sender;

    @Autowired
    public RefactoredEmailService(EmailSenderUtil sender) {
        this.sender = sender;
    }

    public ResponseDto sendActivation(EmailDto emailDetails){
        String bodyText = "Dear " + emailDetails.getUserName() + "," +
                "<br/><br/>" +
                "Thank you for signing up with us.<br/><br/>" +
                "To complete the registration process, please click on the link below to " +
                "confirm your email-address and activate your account.<br/><br/>" +
                "<a href='" + "http://" + url(activation,emailDetails.getToken()) + "'>" +
                "http://" + url(activation,emailDetails.getToken()) + "</a>" +
                "<br/><br/><br/>" +
                "Do you have problems with account activation? Contact out support team: " +
                "<a href='mailto:"+senderEmail+"'>"+senderEmail+"</a>" +
                "";
        emailDetails.setSubject("Activate Your Account");
        emailDetails.setBody(bodyText);
        emailDetails.setContentType("text/html");
        return sender.send(emailDetails);
    }

    public ResponseDto sendClarification(EmailDto emailDetails){
        String bodyText = "Dear " +
                "<br/><br/>" +
                emailDetails.getUserName() + " needs your help.<br/><br/>" +
                "Comment: " + emailDetails.getClarificationComment() + "<br/>" +
                "To answer, please click on the link below to answer." +
                "<br/><br/>" +
                "<a href='" + "http://" + url(clarification,emailDetails.getToken()) + "'>" +
                "http://" + url(clarification,emailDetails.getToken()) + "</a>" +
                "<br/><br/><br/>" +
                "Do you have problems with this url? Contact out support team: " +
                "<a href='mailto:"+senderEmail+"'>"+senderEmail+"</a>" +
                "";
        emailDetails.setSubject("Clarification Required");
        emailDetails.setBody(bodyText);
        emailDetails.setContentType(emailDetails.getContentType());
        return sender.send(emailDetails);
    }

    public ResponseDto sendResetPassword(EmailDto emailDetails){
        String bodyText = "Dear " + emailDetails.getUserName() + "," +
                "<br/><br/>" +
                "To complete the reset password process, please click on the link below to " +
                "complete your password reset process.<br/><br/>" +
                "<a href='" + "http://" + url(resetPassword,emailDetails.getToken()) + "'>" +
                "http://" + url(resetPassword,emailDetails.getToken()) + "</a>" +
                "<br/><br/><br/>" +
                "Do you have problems with password reset? Contact out support team: " +
                "<a href='mailto:"+senderEmail+"'>"+senderEmail+"</a>" +
                "";
        emailDetails.setSubject("Reset password");
        emailDetails.setBody(bodyText);
        emailDetails.setContentType(emailDetails.getContentType());
        return sender.send(emailDetails);
    }

    public ResponseDto sendClarificationFeedback(EmailDto emailDetails){
        String bodyText =  "Dear " + emailDetails.getUserName() + "," +
                "<br/><br/>" +
                "You got response for your clarification. Below is the feedback that you got:<br/><br/>" +
                "<h3>Feedback:</h3> <br/>" + (emailDetails.getClarificationComment().length() > 100 ? emailDetails.getClarificationComment().substring(0, 100) + "...<a href='http://do.gofetchcode.com/#/login'><strong>Read More>>></strong></a>" : emailDetails.getClarificationComment()) + "<br/><br/>" +
                "<br/><br/><br/>" +
                "Do you have problem? Contact out support team: " +
                "<a href='mailto:"+senderEmail+"'>"+senderEmail+"</a>" +
                "";
        emailDetails.setSubject("Clarification Feedback");
        emailDetails.setBody(bodyText);
        emailDetails.setContentType(emailDetails.getContentType());
        return sender.send(emailDetails);
    }

    public ResponseDto sendInvitation(EmailDto emailDetails){
        String bodyText = "Dear " + emailDetails.getUserName() + "," +
                "<br/><br/>" +
                "Gofetchcode invited you to join.<br/><br/>" +
                "Please click on the link below to " +
                "accept the request.<br/><br/>" +
                "<a href='" + "http://" + url(resetPassword,emailDetails.getToken()) + "'>" +
                "http://" + url(resetPassword,emailDetails.getToken()) + "</a>" +
                "<br/><br/><br/>" +
                "<br/><br/><br/>" +
                "Do you have problems with account activation? Contact out support team: " +
                "<a href='mailto:"+senderEmail+"'>"+senderEmail+"</a>" +
                "";
        emailDetails.setSubject("Activate Your Account");
        emailDetails.setBody(bodyText);
        emailDetails.setContentType("text/html");
        return sender.send(emailDetails);
    }


    private String url(String type, String token){
        return Strings.isNullOrEmpty(token)?baseUrI.concat(type):baseUrI.concat(type).concat(token);
    }
}
