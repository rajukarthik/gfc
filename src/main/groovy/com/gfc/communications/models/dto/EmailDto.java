package com.gfc.communications.models.dto;

import com.gfc.communications.enums.EmailType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by abhinav on 2/4/16.
 * DTO for mail message.
 */
@Getter
@Setter
@NoArgsConstructor
public class EmailDto {
    private String userName;
    private String password;
    private String email;
    private String body;
    private String token;
    private String subject;
    private String contentType;
    private String clarificationComment;
    private EmailType type;
}
