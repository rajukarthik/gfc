package com.gfc.communications.models.dto;

import com.gfc.communications.enums.MessageType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by abhinav on 2/4/16.
 * Communication DTO which contains email and message type.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommunicationDto {
    private MessageType type;
    private EmailDto email;
}
