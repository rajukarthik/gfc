package com.gfc.enums;

/**
 * Created by gurinder on 14/1/16.
 * Enum for Search Question status.
 */
public enum SearchStatus {
    INVALID_AUTH_TOKEN("PERSON NOT FOUND FOR THIS AUTH TOKEN"),
    NO_HISTORY_FOUND("NO HISTORY FOUND FOR CURRENT USER"),
    NO_ANSWER_FOUND("NO ANSWER FOUND FOR CURRENT QUESTION");

    private final String status;

    SearchStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }
}
