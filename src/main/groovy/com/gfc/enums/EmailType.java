package com.gfc.enums;

/**
 * Created by Shantanu Singh on 17-Mar-16.
 * Enum for email types.
 */
public enum EmailType {
    CLARIFICATION("Clarification"),
    CONFIRMATION("Confirmation"),
    RESET_PASSWORD("Reset password"),
    CLARIFICATION_FEEDBACK("Clarification Feedback");

    private final String type;

    EmailType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
