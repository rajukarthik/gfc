package com.gfc.search.scheduler.watson;

import com.gfc.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
 * This component is a scheduler which would be used as a scheduler in order to ingest data in IBM watson ranker.
 */
@Component
public class DataIngestionScheduler {

    private SearchService searchService;

    @Autowired
    public DataIngestionScheduler(SearchService searchService) {
        this.searchService = searchService;
    }
    
    /**
     * This is a scheduler for ingesting data in IBM watson ranker for re-ranking.
     */
//	@Scheduled(fixedDelay = 10000)
	public void rankerDataIngestion() {
		searchService.ingestDataInRanker();
	}
}
