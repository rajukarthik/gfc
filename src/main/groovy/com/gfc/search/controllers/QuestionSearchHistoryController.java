package com.gfc.search.controllers;

import com.gfc.dashboard.response.dto.QuestionSearchHistoryResponseDto;
import com.gfc.dashboard.services.QuestionSearchHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * It acts as a controller for search history or recent searches.
 * @author athakran
 *
 */
@RestController
@RequestMapping("/api/secure")
public class QuestionSearchHistoryController {

	private QuestionSearchHistoryService userQuestionSearchHistoryService;
    @Autowired
    public QuestionSearchHistoryController(QuestionSearchHistoryService userQuestionSearchHistoryService) {
        this.userQuestionSearchHistoryService = userQuestionSearchHistoryService;
    }

    /**
     * This API is used to fetch all questions searched by logged-in user.
     * @param pageNo
     * @param pageSize
     * @param sortBy
     * @param direction
     * @return
     */
    @RequestMapping(value = "/question/search/history", method = RequestMethod.GET)
    public ResponseEntity<QuestionSearchHistoryResponseDto> findUserQuestionSearchHistory(
    		@RequestParam(value = "pageno", required = true) int pageNo, 
    		@RequestParam(value = "pagesize", required = true) int pageSize,
    		@RequestParam(value = "sortby", required = false) String sortBy,
    		@RequestParam(value = "direction", required = false) String direction) {
    	QuestionSearchHistoryResponseDto response = new QuestionSearchHistoryResponseDto();
        try {
        	direction = "desc";
        	sortBy = "updatedDt";
        	response = userQuestionSearchHistoryService.findAllQuestionSearchHistory(pageNo, pageSize, sortBy, direction);
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {            
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }
    }
}
