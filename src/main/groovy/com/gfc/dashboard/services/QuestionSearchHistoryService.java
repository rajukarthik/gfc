package com.gfc.dashboard.services;

import com.gfc.dashboard.repositories.services.QuestionSearchHistoryRepositoryService;
import com.gfc.dashboard.response.dto.QuestionSearchHistoryResponseDto;
import com.gfc.dashboard.transformer.QuestionSearchHistoryTransformer;
import com.gfc.models.domain.SearchHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

/**
 * It acts as a service which is used to maintain recent searches or search history of logged-in user.
 * @author athakran
 *
 */
@Service
public class QuestionSearchHistoryService {

	private QuestionSearchHistoryRepositoryService userQuestionSearchHistoryRepositoryService;	
	
    @Autowired
    public QuestionSearchHistoryService(QuestionSearchHistoryRepositoryService userQuestionSearchHistoryRepositoryService) {
    	this.userQuestionSearchHistoryRepositoryService = userQuestionSearchHistoryRepositoryService;    	
    }

    /**
     * This method is used to find recent searches or search history for the logged-in user.
     * @param pageNumber
     * @param pageSize
     * @param sortBy
     * @param direction
     * @return
     */
	public QuestionSearchHistoryResponseDto findAllQuestionSearchHistory(int pageNumber, int pageSize, String sortBy, String direction) {	
		Page<SearchHistory> page = userQuestionSearchHistoryRepositoryService.findAllQuestionSearchHistory(pageNumber, pageSize, sortBy, direction);
		long count = userQuestionSearchHistoryRepositoryService.count();
		
		QuestionSearchHistoryResponseDto questions = new QuestionSearchHistoryResponseDto();
		if(null != page && page.hasContent()) {
			questions = QuestionSearchHistoryTransformer.transform(page.getContent());
			questions.setTotalPage( (long) Math.ceil( (double) count/pageSize));			
		}
						
		return questions;
	}	

}
