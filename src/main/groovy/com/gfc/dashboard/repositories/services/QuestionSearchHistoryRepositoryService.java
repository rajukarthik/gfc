package com.gfc.dashboard.repositories.services;

import com.gfc.dashboard.repositories.QuestionSearchHistoryRepository;
import com.gfc.models.domain.SearchHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * It acts as a repository service which is a communication point to interact with repository for 
 * recent searches or search history of logged-in user.
 * @author athakran
 *
 */
@Service
public class QuestionSearchHistoryRepositoryService {

	private QuestionSearchHistoryRepository userQuestionSearchHistoryRepository;

    @Autowired
    public QuestionSearchHistoryRepositoryService(QuestionSearchHistoryRepository userQuestionSearchHistoryRepository) {
    	this.userQuestionSearchHistoryRepository = userQuestionSearchHistoryRepository;
    }
    
    /**
     * This method is used to fetch recent searches/search question history for the logged in user.
     * @param pageNumber
     * @param pageSize
     * @param sortBy
     * @param direction
     * @return
     */
	public Page<SearchHistory> findAllQuestionSearchHistory(int pageNumber, int pageSize, String sortBy, String direction) {
        PageRequest pageRequest = new PageRequest((pageNumber > 0 ? pageNumber - 1 : 0), pageSize, (direction.equalsIgnoreCase(Sort.Direction.DESC.name()) ? Sort.Direction.DESC : Sort.Direction.ASC), sortBy);		
		return userQuestionSearchHistoryRepository.findAll(pageRequest);
	}
	
	/**
	 * It is used to count total number of receords.
	 * @return
	 */
	public long count() {        	
		return userQuestionSearchHistoryRepository.count();
	}	
}
