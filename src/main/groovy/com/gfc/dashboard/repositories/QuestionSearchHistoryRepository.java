package com.gfc.dashboard.repositories;

import com.gfc.models.domain.SearchHistory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *  @author athakran
 *	This repository is used to maintain recent search or search history.
 */
@Repository
public interface QuestionSearchHistoryRepository extends JpaRepository<SearchHistory, Long> {

	public List<SearchHistory> findAllByOrderByUpdatedDtDesc(Pageable pageable); 
}
