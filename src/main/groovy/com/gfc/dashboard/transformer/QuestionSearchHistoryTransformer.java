package com.gfc.dashboard.transformer;

import com.gfc.dashboard.response.dto.QuestionSearchHistoryDto;
import com.gfc.dashboard.response.dto.QuestionSearchHistoryResponseDto;
import com.gfc.models.domain.SearchHistory;
import com.gfc.models.domain.SearchResponse;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * It is a utility and used to transform Search History entity to Question Search Response DTO.
 * @author athakran
 *
 */
public abstract class QuestionSearchHistoryTransformer {

    public static QuestionSearchHistoryResponseDto transform(List<SearchHistory> searchHistoryList) {
    	QuestionSearchHistoryResponseDto transformedData = new QuestionSearchHistoryResponseDto();
    	searchHistoryList.forEach(item->{
    		SearchResponse response = null;
    		if(null != item.getSearchResponses() && !item.getSearchResponses().isEmpty()) {
        		response = Collections.max(item.getSearchResponses(), Comparator.comparing(c -> c.getScore()));	
    		}
    		QuestionSearchHistoryDto data = new QuestionSearchHistoryDto(item.getId(), item.getPeople().getEmail(), 
    				item.getQuestion(), (null != response ? response.getSectionSequenceId() : ""), (null != response ? response.getSectionSequenceTitle() : ""), 
    				(null != response ? response.getBody() : ""), (null != response ? response.getScore() : 0), item.getCreatedDt());    		    		
    		transformedData.getData().add(data);    		
    	});
    	return transformedData;
    }
}
