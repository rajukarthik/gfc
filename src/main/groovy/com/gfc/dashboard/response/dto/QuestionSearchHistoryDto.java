package com.gfc.dashboard.response.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionSearchHistoryDto {

    private Long id;

    private String email;
    
    private String question;
    
    private String sectionId;
    
    private String sectionTitle;
    
    private String answer;
    
    private double score;

    private Date createdDate;
}
