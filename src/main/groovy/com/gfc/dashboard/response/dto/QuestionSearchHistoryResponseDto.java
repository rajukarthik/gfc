package com.gfc.dashboard.response.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gurinder on 11/1/16.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionSearchHistoryResponseDto {

	private long totalPage;
	
	private List<QuestionSearchHistoryDto> data = new ArrayList<QuestionSearchHistoryDto>();
    
}
