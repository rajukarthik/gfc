package com.gfc.constants;

/**
 * Created by gurinder on 16/1/16.
 * A common class used to maintain common constants used in application.
 */
public interface Constant {
    String AUTH_TOKEN = "Auth-Token";
}