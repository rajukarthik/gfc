package com.gfc.client;

import com.gfc.response.RankerResponse;
import com.gfc.response.RankingDetails;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * Created by gurinder on 28/1/16.
 */
public interface RankerClient {
    @RequestLine("GET")
    @Headers("Content-Type: application/json")
    RankerResponse getAllRankers();
    
    @RequestLine("GET {ranker_id}")
    @Headers("Content-Type: application/json")
    RankingDetails getRankerDetail(@Param("ranker_id") String rankerId);
    
	@RequestLine("POST /rank")
//	@Headers("Content-Type: multipart/form-data")
	@Headers("Content-Type: multipart/form-data")
	@Body("{\"training_data\" : \"{training_data}\", \"training_metadata\" : {\"name\" : \"{ranker_name}\"}}")
	void ingest(@Param("training_data") String trainingData, @Param("ranker_name") String rankerName);    
}
