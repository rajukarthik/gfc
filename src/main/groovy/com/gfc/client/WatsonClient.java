package com.gfc.client;

import com.gfc.response.WatsonRankerResponse;
import com.gfc.response.WatsonResponse;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * Created by gurinder on 2/1/16.
 */
public interface WatsonClient {

	@RequestLine("GET {active_cluster_id}/solr/gfc-syn/fcselect?ranker_id={ranker_id}&wt={type}&q={question}")
	@Headers("Content-Type: application/json")
	WatsonResponse question(@Param("active_cluster_id") String activeClusterId, @Param("ranker_id") String rankerId, @Param("type") String type,
							@Param("question") String question);
	
	@RequestLine("GET {active_cluster_id}/solr/{solr_collection_name}/fcselect?q={question}&ampgt={gt}&generateHeader={header}&rows={rows}&returnRSInput={rsInput}&wt={type}")
	@Headers("Content-Type: application/json")
	WatsonRankerResponse findRelevanceCollection(@Param("active_cluster_id") String activeClusterId, 
										@Param("solr_collection_name") String collectionName,
										@Param("question") String question,
										@Param("gt") String groundTruth,
										@Param("header") boolean generateHeader,
										@Param("rows") String rows,
										@Param("rsInput") boolean returnRSInput,
										@Param("type") String type);	
}
