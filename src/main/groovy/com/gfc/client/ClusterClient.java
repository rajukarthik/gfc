package com.gfc.client;

import com.gfc.response.ClusterResponse;
import feign.Headers;
import feign.RequestLine;


/**
 * Created by gurinder on 27/1/16.
 */
public interface ClusterClient {

    @RequestLine("GET")
    @Headers("Content-Type: application/json")
    ClusterResponse checkClusterStatus();
}
