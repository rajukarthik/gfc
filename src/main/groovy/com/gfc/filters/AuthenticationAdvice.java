package com.gfc.filters;

import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.users.accounts.models.entities.LoginActivity;
import com.gfc.users.accounts.repositories.services.LoginActivityRepositoryService;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by abhinav on 4/1/16.
 * An authentication filter used to check auth-token and session time-out period.
 */
@Component
@NoArgsConstructor
public class AuthenticationAdvice implements Filter {

    @Autowired
    private LoginActivityRepositoryService loginActivityRepositoryService;

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationAdvice.class);

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        logger.info(request.getRequestURL().toString());
        if(request.getRequestURL().toString().contains("/secure/")){
            boolean isValid = validateAuthToken(request);
            if(!isValid) {
                response.sendError(HttpStatus.FORBIDDEN.value(), ResponseStatus.INVALID_REQUEST.getStatus());
                return;
            }
        }
        chain.doFilter(req, res);
    }

    @Override
    public void destroy() {}

    @Override
    public void init(FilterConfig arg0) throws ServletException {}

    private boolean validateAuthToken(HttpServletRequest request){
        String token=request.getHeader("Auth-Token");
        try {
        	LoginActivity loginActivity = loginActivityRepositoryService.findByAuthToken(token);   
        	if(null == loginActivity) {
        		return false;
        	}

        	long minutes = ((new Date().getTime()/60000) - (loginActivity.getUpdatedDt().getTime()/60000));
        	if(minutes > 30) {
        		return false;
        	}
        	loginActivityRepositoryService.updateLoginActivity(loginActivity);
        	return true;
        }catch (PersonNotFoundException e){
            return false;
        }
    }
}