package com.gfc.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.lang.annotation.*;

/**
 * Created by abhinav on 26/12/15.
 *
 * This class is used for validating name format.
 */
@Documented
@Constraint(validatedBy = {})
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@NotNull
@Size(min = 2, max = 100)
@Pattern(regexp = "^[a-zA-Z\\\\s]*$")
@ReportAsSingleViolation
public @interface Name {
    public abstract String message() default "{error.name}";

    public abstract Class<?>[] groups() default {};

    public abstract Class<? extends Payload>[] payload() default {};
}
