/**
 * 
 */
package com.gfc.response;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Generated;

/**
 * @author Shantanu Singh
 */
@Generated("org.jsonschema2pojo")
@Getter
@Setter
public class WatsonResponse {

	@Expose
	private ResponseHeader responseHeader;
	
	@Expose
	private Response response;
}
