/**
 * 
 */
package com.gfc.response;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Generated;

/**
 *
 * @author Shantanu Singh
 */
@Getter
@Setter
@Generated("org.jsonschema2pojo")
public class Docs {
	
	@Expose
	private String SSID;
	
	@Expose
	private String SSTitle;
	
	@Expose
	private String chapterTitle;
	
	@Expose
	private String sectionTitle;
	
	@Expose
	private String SS1ID;
	
	@Expose
	private String SS1Title;
	
	@Expose
	private String SS2ID;
	
	@Expose
	private String SS2Title;
	
	@Expose
	private String body;
	
	@Expose
	private Double _version_;
	
	@Expose
	private Double score;
	
	@Expose
	private String featureVector;
}
