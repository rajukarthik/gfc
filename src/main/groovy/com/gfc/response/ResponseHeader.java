/**
 * 
 */
package com.gfc.response;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Generated;

/**
 *
 * @author Shantanu Singh
 */
@Getter
@Setter
@Generated("org.jsonschema2pojo")
public class ResponseHeader {
	
	@Expose
	private Double status;

	@Expose
	private Double QTime;
}
