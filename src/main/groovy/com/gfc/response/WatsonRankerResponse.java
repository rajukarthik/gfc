package com.gfc.response;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
@Getter
@Setter
public class WatsonRankerResponse {

	@Expose
	private ResponseHeader responseHeader;
	
	@Expose
	private String RSInput;
}
