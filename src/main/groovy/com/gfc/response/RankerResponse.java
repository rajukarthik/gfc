package com.gfc.response;

import com.google.gson.annotations.Expose;
import lombok.*;

import java.util.List;

/**
 * Created by gurinder on 28/1/16.
 */

@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
@ToString
public class RankerResponse {

    @Expose
    List<RankingDetails> rankers;

}
