package com.gfc.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.*;

import java.time.Instant;

/**
 * Created by gurinder on 28/1/16.
 */

@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
@ToString
public class RankingDetails {

    @Expose
    @SerializedName("ranker_id")
    private String rankerId;

    @Expose
    @SerializedName("name")
    private String name;
    
    @Expose
    @SerializedName("created")
    private String createdDate;
    
    @Expose
    @SerializedName("url")
    private String url;
    
    @Expose
    @SerializedName("status")
    private String status;
    
    @Expose
    @SerializedName("status_description")
    private String description;    
    

    public Instant getCreatedDate() {
        return Instant.parse(createdDate);
    }
}
