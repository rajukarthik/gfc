package com.gfc.response;

import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by gurinder on 28/1/16.
 */
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
public class ClusterResponse {
    @Expose
    List<ClusterDetails> clusters;
}
