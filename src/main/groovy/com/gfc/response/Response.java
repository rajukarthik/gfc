/**
 * 
 */
package com.gfc.response;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Shantanu Singh
 */
@Getter
@Setter
@Generated("org.jsonschema2pojo")
public class Response {

	@Expose
	private Double numFound;

	@Expose
	private Double start;

	@Expose
	private Double maxScore;

	@Expose
	private List<Docs> docs = new ArrayList<Docs>();
}
