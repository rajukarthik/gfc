package com.gfc.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by gurinder on 28/1/16.
 */
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
public class ClusterDetails {
    @Expose
    @SerializedName("solr_cluster_id")
    private String solrClusterId;

    @Expose
    @SerializedName("cluster_name")
    private String clusterName;

    @Expose
    @SerializedName("cluster_size")
    private String clusterSize;

    @Expose
    @SerializedName("solr_cluster_status")
    private String solrClusterStatus;
}
