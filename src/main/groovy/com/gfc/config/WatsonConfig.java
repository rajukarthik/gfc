package com.gfc.config;

import com.gfc.client.ClusterClient;
import com.gfc.client.RankerClient;
import com.gfc.client.WatsonClient;
import feign.Feign;
import feign.auth.BasicAuthRequestInterceptor;
import feign.gson.GsonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by gurinder on 27/1/16.
 * This is configuration class for IBM Watson.
 */

@Configuration
public class WatsonConfig {

    @Value("${watson.search.username}")
    private String watsonSearchUserName;

    @Value("${watson.search.password}")
    private String watsonSearchPassword;

    @Value("${watson.search.gateway.url}")
    private String watsonSearchGatewayUrl;


    /**
     * This method is used to create bean which fetches list of clusters from IBM watson.
     * @return
     */
    @Bean(name = "gfcClient")
    public WatsonClient artifactoryClient() {
        WatsonClient gfcClient =
                Feign.builder()
                        .encoder(new JacksonEncoder())
                        .requestInterceptor(new BasicAuthRequestInterceptor(watsonSearchUserName, watsonSearchPassword))
                        .decoder(new GsonDecoder())
                        .target(WatsonClient.class, "https://" + watsonSearchGatewayUrl + "solr_clusters/");
        return gfcClient;
    }

    /**
     * This method is used to create bean which fetches list of clusters from IBM watson.
     * @return
     */
    @Bean(name = "gfcCluster")
    public ClusterClient artifactoryClusterClient() {
        ClusterClient clusterClient =
                Feign.builder()
                        .encoder(new JacksonEncoder())
                        .requestInterceptor(new BasicAuthRequestInterceptor(watsonSearchUserName, watsonSearchPassword))
                        .decoder(new GsonDecoder())
                        .target(ClusterClient.class, "https://" + watsonSearchGatewayUrl + "solr_clusters/");
        return clusterClient;
    }

    /**
     * This method is used to create a bean which fetches rankers from IBM watson.
     * @return
     */
    @Bean(name = "gfcRanker")
    public RankerClient artifactoryRankerClient() {
        RankerClient rankerClient =
                Feign.builder()
                        .encoder(new JacksonEncoder())
                        .requestInterceptor(new BasicAuthRequestInterceptor(watsonSearchUserName, watsonSearchPassword))
                        .decoder(new GsonDecoder())
                        .target(RankerClient.class, "https://" + watsonSearchGatewayUrl + "rankers/");
        return rankerClient;
    }
}
