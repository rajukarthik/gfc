package com.gfc.utils;

import com.gfc.enums.EmailType;
import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.EmailFailureException;
import com.gfc.models.MimeMessageHelper;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.Activation;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.UserActivationRepository;
import com.gfc.users.accounts.repositories.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 * Created by gurinder on 21/12/15.
 * A mail sender utility used to create mail messages and sending.
 */

@Component
public class EmailSender {

    private static final Logger logger = LoggerFactory.getLogger(EmailSender.class);

    @Value("${mail.smtp.from}")
    private String emailId;

    @Value("${gfc.server.port}")
    private String port;

    @Value("${gfc.server.url}")
    private String url;

    @Value("${gfc.activation.api}")
    private String api;

    @Value("${gfc.clarification.api}")
    private String clarificationApi;

    @Value("${gfc.resetPassword.api}")
    private String resetPasswordApi;

    private UserActivationRepository personActivationRepository;

    private UserRepository personRepository;

    private Session session;


    @Autowired
    public EmailSender(UserActivationRepository personActivationRepository, UserRepository personRepository, Session session) {
        this.personActivationRepository = personActivationRepository;
        this.personRepository = personRepository;
        this.session = session;
    }

    /**
     * this method handles mailing process for activation
     * @param userName
     * @param userEmail
     * @param token
     */
    public void processEmail(String userName, String userEmail, String token) throws EmailFailureException{
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper();
        mimeMessageHelper.setFrom(emailId);
        mimeMessageHelper.setTo(userEmail);
        mimeMessageHelper.setText(EmailType.CONFIRMATION.getType());
        mimeMessageHelper.setToken(token);
        sendEmail(mimeMessageHelper);
    }

    /**
     * This method is used to prepare mail message.
     * @param userName
     * @param recipientEmail
     * @param comment
     * @param token
     * @param emailType
     * @throws EmailFailureException
     */
    public void processEmail(String userName, String recipientEmail, String comment, String token, EmailType emailType) throws EmailFailureException {
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper();
        mimeMessageHelper.setFrom(emailId);
        mimeMessageHelper.setSenderName(userName);
        mimeMessageHelper.setTo(recipientEmail);
        mimeMessageHelper.setText(emailType.getType());
        mimeMessageHelper.setToken(token);

        if (StringUtils.isNotBlank(comment)) {
            mimeMessageHelper.setComment(comment);
        }

        sendEmail(mimeMessageHelper);
    }

    /**
     * This method is used to create mail message and then send mail message.
     * @param mimeMessageHelper
     * @return
     * @throws EmailFailureException
     */
    private boolean sendEmail(MimeMessageHelper mimeMessageHelper) throws EmailFailureException{
        Activation personActivation = null;
        MimeMessage message = new MimeMessage(session);
        String emailContent = null;

        try {
            message.setFrom(new InternetAddress(mimeMessageHelper.getFrom()));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(mimeMessageHelper.getTo()));

            if (mimeMessageHelper.getText().contains(EmailType.CONFIRMATION.getType())) {
                message.setSubject("Activate Your Account");
                User user = personRepository.findByEmail(mimeMessageHelper.getTo());
                Activation gfcUserActivation = new Activation(user, mimeMessageHelper.getToken(), ActivityStatus.PENDING);
                personActivationRepository.save(gfcUserActivation);
                logger.info("****************************************");
                logger.info("http://" + url + ":" + port + api + mimeMessageHelper.getToken());
                logger.info("****************************************");

                emailContent = "Dear " + user.getFirstName() + "," +
                        "<br/><br/>" +
                        "Thank you for signing up with us.<br/><br/>" +
                        "To complete the registration process, please click on the link below to " +
                        "confirm your email-address and activate your account.<br/><br/>" +
/*                    "<a href='" + "http://" + url + ":" + port + api + mimeMessageHelper.getToken() + "'>" +
                    "http://" + url + ":" + port + api + mimeMessageHelper.getToken() + "</a>" +*/
                        "<a href='" + "http://" + url + api + mimeMessageHelper.getToken() + "'>" +
                        "http://" + url + api + mimeMessageHelper.getToken() + "</a>" +
                        "<br/><br/><br/>" +
                        "Do you have problems with account activation? Contact out support team: " +
                        "<a href='mailto:info@gofetchcode.com'>info@gofetchcode.com</a>" +
                        "";
            } else if (mimeMessageHelper.getText().equals(EmailType.CLARIFICATION.getType())) {
                message.setSubject("Clarification Required");
                logger.info("****************************************");
                logger.info("http://" + url + ":" + port + clarificationApi + mimeMessageHelper.getToken());
                logger.info("****************************************");

                emailContent = "Dear " +
                        "<br/><br/>" +
                        mimeMessageHelper.getSenderName() + " needs your help.<br/><br/>" +
                        "Comment: " + mimeMessageHelper.getComment() + "<br/>" +
                        "To answer, please click on the link below to answer." +
                        "<br/><br/>" +
                        "<a href='" + "http://" + url + clarificationApi + mimeMessageHelper.getToken() + "'>" +
                        "http://" + url + clarificationApi + mimeMessageHelper.getToken() + "</a>" +
                        "<br/><br/><br/>" +
                        "Do you have problems with this url? Contact out support team: " +
                        "<a href='mailto:info@gofetchcode.com'>info@gofetchcode.com</a>" +
                        "";
            } else if (mimeMessageHelper.getText().contains(EmailType.RESET_PASSWORD.getType())) {
                User user = personRepository.findByEmail(mimeMessageHelper.getTo());
                personActivation = personActivationRepository.findByPeople(user);
                personActivation.setAuthToken(mimeMessageHelper.getToken());
                personActivation.setStatus(ActivityStatus.ACTIVE);   
                personActivationRepository.save(personActivation);
                
                message.setSubject("Reset password");
                logger.info("****************************************");
                logger.info("http://" + url + ":" + port + resetPasswordApi + mimeMessageHelper.getToken());
                logger.info("****************************************");

                emailContent = "Dear " + user.getFirstName() + "," +
                        "<br/><br/>" +
                        "To complete the reset password process, please click on the link below to " +
                        "complete your password reset process.<br/><br/>" +
                        "<a href='" + "http://" + url + resetPasswordApi + mimeMessageHelper.getToken() + "'>" +
                        "http://" + url + resetPasswordApi + mimeMessageHelper.getToken() + "</a>" +
                        "<br/><br/><br/>" +
                        "Do you have problems with password reset? Contact out support team: " +
                        "<a href='mailto:info@gofetchcode.com'>info@gofetchcode.com</a>" +
                        "";
            } else if (mimeMessageHelper.getText().equals(EmailType.CLARIFICATION_FEEDBACK.getType())) {
                message.setSubject("Clarification Feedback");
                logger.info("****************************************");
                logger.info("http://" + url + ":" + port + clarificationApi + mimeMessageHelper.getToken());
                logger.info("****************************************");
                
                emailContent = "Dear " + mimeMessageHelper.getSenderName() + "," +
                        "<br/><br/>" +
                        "You got response for your clarification. Below is the feedback that you got:<br/><br/>" +
                        "<h3>Feedback:</h3> <br/>" + (mimeMessageHelper.getComment().length() > 100 ? mimeMessageHelper.getComment().substring(0, 100) + "...<a href='http://do.gofetchcode.com/#/login'><strong>Read More>>></strong></a>" : mimeMessageHelper.getComment()) + "<br/><br/>" +
                        "<br/><br/><br/>" +
                        "Do you have problem? Contact out support team: " +
                        "<a href='mailto:info@gofetchcode.com'>info@gofetchcode.com</a>" +
                        "";
            }

            message.setContent(emailContent, "text/html");
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new EmailFailureException(ResponseStatus.INTERNAL_ERROR.getStatus());
        }

        return true;
    }
}