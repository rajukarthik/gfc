package com.gfc.utils;

import org.apache.commons.lang.StringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by gurinder on 13/2/16.
 * An encrytpion utility for encrypting data.
 */
public class EncryptUtils {

    public static String encryptMD5(String password) {
        return encrypt(password, null);
    }

    /**
     * This method is used to encrypt data using MD5.
     * @param msg
     * @param type
     * @return
     */
    private static String encrypt(String msg, String type) {
        MessageDigest md = null;
        StringBuilder password = new StringBuilder();

        try {
                md = MessageDigest.getInstance("MD5");

            if (StringUtils.isNotEmpty(type)) {
                md.update(type.getBytes());
            } else {
                md.update(msg.getBytes());
            }

            byte[] bytes = md.digest();
            for (int i = 0; i < bytes.length; i++) {
                String param = Integer.toString((bytes[i] & 0xff) + 0x100, 16);
                password.append(param.substring(1));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return password.toString();
    }

}
