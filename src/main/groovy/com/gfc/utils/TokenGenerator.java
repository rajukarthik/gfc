package com.gfc.utils;

import java.util.UUID;

/**
 * Created by gurinder on 22/12/15.
 *
 * A utility to generate rankom tokens.
 */
public class TokenGenerator {

    /**
     * This method is used to generate random token.
     *
     * @return String random token.
     */
    public static String getToken() {
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }
}
