package com.gfc.utils;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * A throwing aspect used to log error message.
 * @author athakran
 *
 */
@Aspect
@Component
public class ApplicationLogger {

	/**
	 * This method is used to log exceptions
	 *
	 * @param jp JoinPoints where log is injected
	 * @param ex Exception thrown
	 */
	@AfterThrowing(pointcut = "within(com.gfc..*)", throwing = "ex")
	public void logError(JoinPoint jp, Exception ex) {
		System.out.println("Exception: " + ex.getMessage());
	}
}
