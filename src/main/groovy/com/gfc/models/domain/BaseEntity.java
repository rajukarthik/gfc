package com.gfc.models.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by abhinav on 20/12/15.
 */

@Setter
@Getter
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    @Column(name = "created_dt")
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date createdDt;

    @Column(name = "updated_dt")
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date updatedDt;

    @Column(name = "created_by")
    @JsonIgnore
    private String createdBy;

    @Column(name = "updated_by")
    @JsonIgnore
    private String updatedBy;

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @PrePersist
    public void preCreate(){
        Date date = new Date();
        this.setCreatedDt(date);
        this.setUpdatedDt(date);
        this.setUpdatedBy("master");
        this.setCreatedBy("master");
    }

    @PreUpdate
    public void preUpdate(){
        Date date = new Date();
        this.setUpdatedDt(date);
        this.setUpdatedBy("master");
    }
}
