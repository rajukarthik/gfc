/**
 * 
 */
package com.gfc.models.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 *
 * @author Shantanu Singh
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "gfc_search_response_summary")
public class SearchResponse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="gfc_search_history_id")
	private SearchHistory searchHistory;

	@Column(name = "section_sequence_id")
	private String sectionSequenceId;

	@Column(name = "section_sequence_title")
	private String sectionSequenceTitle;
	
	@Column(name = "chapter_title")
	private String chapterTitle;
	
	@Column(name = "section_title")
	private String sectionTitle;
	
	@Column(name = "section_sequence_id_1")
	private String sectionSequenceId1;
	
	@Column(name = "section_sequence_title_1")
	private String sectionSequenceTitle1;
	
	@Column(name = "section_sequence_id_2")
	private String sectionSequenceId2;
	
	@Column(name = "section_sequence_title_2")
	private String sectionSequenceTitle2;
	
	@Lob
	private String body;

	private Double version;

	private Double score;

	@Column(name = "feature_vector")
	@Lob
	private String featureVector;
}
