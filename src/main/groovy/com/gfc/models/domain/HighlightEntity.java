package com.gfc.models.domain;

import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by gurinder on 19/1/16.
 */
@Entity
@Table(name = "gfc_highlight")
@NoArgsConstructor
@Setter
@Getter
public class HighlightEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "chapter")
    private String chapter;

    @Column(name = "startIndex")
    private String startIndex;

    @Column(name = "endIndex")
    private String endIndex;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;

    @ManyToOne
    @JoinColumn(name = "people_id", referencedColumnName = "id")
    private User people;


}
