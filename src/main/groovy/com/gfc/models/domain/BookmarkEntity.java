/**
 * 
 */
package com.gfc.models.domain;

import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 *
 * @author Shantanu Singh
 */
@Entity
@Table(name = "gfc_bookmark")
@NoArgsConstructor
@Setter
@Getter
public class BookmarkEntity extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "section_sequence_id")
	private String sectionSequenceId;

	@Column(name = "section_sequence_title")
	private String sectionSequenceTitle;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;

    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "people_id", referencedColumnName = "id")
    private User people;
}
