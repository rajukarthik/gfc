package com.gfc.models.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by gurinder on 29/1/16.
 */

@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
public class ClusterRankingPk implements Serializable {

    private String solrClusterId;

    private String rankerId;

}
