package com.gfc.models.domain;

import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * @author Shantanu Singh
 */
@Entity
@Table(name = "gfc_search_history")
@NoArgsConstructor
@Setter
@Getter
public class SearchHistory extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 4096)
    private String question;

    @Lob
    private String response;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;
    
    @ManyToOne
    @JoinColumn(name = "people_id"  ,referencedColumnName = "id")
    private User people;

    @OneToMany(mappedBy = "searchHistory")
    private List<SearchResponse> searchResponses;

}
