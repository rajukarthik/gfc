package com.gfc.models.domain;

import com.gfc.users.accounts.enums.ActivityStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by gurinder on 28/1/16.
 */

@Entity
@Table(name = "gfc_cluster_ranking")
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
@IdClass(ClusterRankingPk.class)
public class ClusterRankingEntity extends BaseEntity implements Serializable {

    @Id
    @Column(name = "solr_cluster_id")
    private String solrClusterId;

    @Id
    @Column(name = "ranker_id")
    private String rankerId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;



}
