package com.gfc.models.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by gurinder on 29/1/16.
 */
@Entity
@Table(name = "gfc_configuration")
@NoArgsConstructor
@Setter
@Getter
@AllArgsConstructor
public class ConfigurationEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "code")
    private int code;

    @Column(name = "description")
    private String description;

    @Column(name = "info")
    private String info;
}
