package com.gfc.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by gurinder on 23/12/15.
 */
public class MimeMessageHelper {

    private @Getter @Setter String to;

    private @Getter @Setter String from;

    private @Getter @Setter String senderName;

    private @Getter @Setter String text;

    private @Getter @Setter String token;

    private @Getter @Setter String comment;


}
