/**
 * 
 */
package com.gfc.models.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.models.dto.ResponseSummaryDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 *
 * @author Shantanu Singh
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SearchDto {

	private Long id;
	private List<ResponseSummaryDto> responses;
}
