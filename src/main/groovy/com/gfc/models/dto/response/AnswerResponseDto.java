package com.gfc.models.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.models.domain.SearchResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by gurinder on 14/1/16.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AnswerResponseDto {

    private String sectionSequenceId;

    private String sectionSequenceTitle;

    private String chapterTitle;

    private String sectionTitle;

    private String sectionSequenceId1;

    private String sectionSequenceTitle1;

    private String sectionSequenceId2;

    private String sectionSequenceTitle2;

    private String body;

    private Double version;

    private Double score;

    private String featureVector;

    public static AnswerResponseDto getInstance() {
        return new AnswerResponseDto();
    }

    public AnswerResponseDto createAnswerDto(SearchResponse searchResponseSummaryEntity) {
        AnswerResponseDto answerResponseDto = new AnswerResponseDto();
        answerResponseDto.setBody(searchResponseSummaryEntity.getBody());
        answerResponseDto.setChapterTitle(searchResponseSummaryEntity.getChapterTitle());
        answerResponseDto.setFeatureVector(searchResponseSummaryEntity.getFeatureVector());
        answerResponseDto.setSectionSequenceId(searchResponseSummaryEntity.getSectionSequenceId());
        answerResponseDto.setSectionSequenceId1(searchResponseSummaryEntity.getSectionSequenceId1());
        answerResponseDto.setSectionSequenceId2(searchResponseSummaryEntity.getSectionSequenceId2());
        answerResponseDto.setScore(searchResponseSummaryEntity.getScore());
        answerResponseDto.setSectionSequenceTitle(searchResponseSummaryEntity.getSectionSequenceTitle());
        answerResponseDto.setSectionSequenceTitle1(searchResponseSummaryEntity.getSectionSequenceTitle1());
        answerResponseDto.setSectionSequenceTitle2(searchResponseSummaryEntity.getSectionSequenceTitle2());
        answerResponseDto.setSectionTitle(searchResponseSummaryEntity.getSectionTitle());
        answerResponseDto.setVersion(searchResponseSummaryEntity.getVersion());
        return answerResponseDto;
    }
}
