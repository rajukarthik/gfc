package com.gfc.models.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by gurinder on 19/1/16.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class HighLightDto {

    @JsonProperty("chapter")
    private String chapter;

    @JsonProperty("startIndex")
    private String startIndex;

    @JsonProperty("endIndex")
    private String endIndex;

    public HighLightDto(String startIndex, String endIndex) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

}
