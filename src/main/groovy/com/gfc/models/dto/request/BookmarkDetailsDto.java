package com.gfc.models.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 *
 * @author Shantanu Singh
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BookmarkDetailsDto {
	
	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("sectionSequenceId")
	private String sectionSequenceId;

	@JsonProperty("sectionSequenceTitle")
	private String sectionSequenceTitle;
	
	@JsonProperty("hyperlink")
	private String hyperlink;
	
	@JsonProperty("updatedDt")
	private Date updatedDt;
}
