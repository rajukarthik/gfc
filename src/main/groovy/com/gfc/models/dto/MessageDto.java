package com.gfc.models.dto;

import com.gfc.enums.ResponseStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by abhinav on 25/12/15.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MessageDto {
    private String message;
    private ResponseStatus status;
}
