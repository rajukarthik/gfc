/**
 * 
 */
package com.gfc.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Shantanu Singh
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseSummaryDto implements Comparable<ResponseSummaryDto> {

	String answer;
	Integer idWatson;
	String sectionName;
	String snip;
	Double confidence;
	String hyperlink;
	String sectionNumber;
	String chapterNo;

	@Override
	public int compareTo(ResponseSummaryDto dto) {
		if (this.getConfidence() > dto.getConfidence())
			return -1;
		else if (this.getConfidence() == dto.getConfidence())
			return 0;

		return 1;
	}
}
