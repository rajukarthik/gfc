package com.gfc.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Shantanu Singh on 18-Mar-16.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClarificationGuestValidationDto {
    private long clarificationId;
    private String userName;
}
