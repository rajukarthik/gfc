package com.gfc.users.accounts.exceptions;

/**
 * Created by abhinav on 9/3/16.
 *
 * This is class is used as custom exception.
 */
public class NotFoundException extends Exception{
    public NotFoundException(String message) {
        super(message);
    }
}
