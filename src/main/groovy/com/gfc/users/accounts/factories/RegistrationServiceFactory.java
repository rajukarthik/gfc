//package com.gfc.users.accounts.factories;
//
//import com.gfc.users.accounts.services.OrganisationService;
//import com.gfc.users.accounts.services.TeamService;
//import com.gfc.users.accounts.services.UserService;
//import com.gfc.users.accounts.services.contract.RegistrationContract;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * Created by abhinav on 1/4/16.
// */
//@Component
//public class RegistrationServiceFactory {
//
//    @Autowired
//    private TeamService teamService;
//    @Autowired
//    private OrganisationService organisationService;
//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    public RegistrationServiceFactory(TeamService teamService, OrganisationService organisationService, UserService userService) {
//        this.userService = userService;
//        this.teamService = teamService;
//        this.organisationService = organisationService;
//    }
//
////    @FunctionalInterface
////    private interface I_RegistrationContractFactory{
////        RegistrationContract getService(String factoryName);
////    }
////
////    private I_RegistrationContractFactory factory = (String factoryName) -> {
////       switch (factoryName.toLowerCase()){
////           case "team":
////               return teamService;
////           case "organisation":
////               return organisationService;
////           case "individual":
////               return userService;
////       }
////        return null;
////    };
//
//    public RegistrationContract getService(String factoryName) {
//        System.out.println("*******************");
//        System.out.println(factoryName);
//        System.out.println("*******************");
//        switch (factoryName.toLowerCase()){
//            case "team":
//                return teamService;
//            case "organisation":
//                return organisationService;
//            case "individual":
//                return userService;
//        }
//        return null;
//    }
//}
