package com.gfc.users.accounts.controllers;

import com.gfc.users.accounts.models.dto.request.AccountDetailsDto;
import com.gfc.users.accounts.models.dto.request.PasswordRequestDto;
import com.gfc.users.accounts.models.dto.response.ProfileResponseDto;
import com.gfc.users.accounts.models.dto.response.StatusDto;
import com.gfc.users.accounts.services.AccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by abhinav on 5/3/16.
 *
 * This class is used to handle accounts related apis.
 */
@RestController
@RequestMapping(value = "/api/secure/users")
public class AccountsController {

    private AccountsService accountsService;

    @Autowired
    public AccountsController(AccountsService accountsService) {
        this.accountsService = accountsService;
    }

    /**
     * This method is used to update accounts details.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/accounts/update", method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody AccountDetailsDto request){
        ProfileResponseDto response = accountsService.update(request);
        return new ResponseEntity(response , response.getStatus().getHttpStatus());
    }

    /**
     * This method is used to changes account password.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/accounts/password/update", method = RequestMethod.PUT)
    public ResponseEntity changePassword(@RequestBody PasswordRequestDto request){
        StatusDto response = accountsService.changePassword(request);
        return new ResponseEntity(response, response.getHttpStatus());
    }

    /**
     * This method is used to validate account password.
     *
     * @param id
     * @param password
     * @return
     */
    @RequestMapping(value = "/{id}/accounts/password/{password}/validate", method = RequestMethod.GET)
    public ResponseEntity validatePassword(@PathVariable("id") Integer id, @PathVariable("password") String password){
        PasswordRequestDto request = new PasswordRequestDto();
        request.setUserId(id);
        request.setCurrentPassword(password);
        StatusDto response = accountsService.validatePassword(request);
        return new ResponseEntity(response,response.getHttpStatus());
    }
}
