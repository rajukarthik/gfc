package com.gfc.users.accounts.controllers;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.models.dto.request.RegistrationDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.services.RegistrationService;
import com.gfc.users.accounts.services.TeamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by abhinav on 19/12/15.
 *
 * This class is used to handle registration.
 */

@RestController
@RequestMapping("/api")
public class RegistrationController {

    private RegistrationService registrationService;
    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    /**
     * This method is used to register new users.
     *
     * @param request
     * @param device
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody @Validated RegistrationDto request,
            @RequestHeader(value = "X-Device-Info" , defaultValue = "web-app" ,required = false) String device
    ){
        ResponseDto response = null;
        try {
            response = registrationService.register(request, device);
            return new ResponseEntity(response, response.getHttpStatus());
        }catch (Exception e){
            e.printStackTrace();
            response = new ResponseDto(request.getUser().getType() + " Registration Failed",
                    ResponseStatus.INTERNAL_ERROR, null, null,HttpStatus.INTERNAL_SERVER_ERROR ,null
            );
            return new ResponseEntity(response,response.getHttpStatus());
        }
    }
}
