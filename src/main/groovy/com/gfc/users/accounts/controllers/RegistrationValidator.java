package com.gfc.users.accounts.controllers;

import com.gfc.exceptions.RegistrationException;
import com.gfc.models.dto.MessageDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Locale;

/**
 * Created by abhinav on 25/12/15.
 *
 * This class is used as validator for registration
 */
@ControllerAdvice
public class RegistrationValidator {

    private MessageSource messageSource;

    @Autowired
    public RegistrationValidator(MessageSource messageSource) {
        this.messageSource = messageSource;
    }


    /**
     * This method handle the registration exception.
     *
     * @param exception
     * @return
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseDto registrationExceptionHandler(RegistrationException exception) {
        ResponseDto response = new ResponseDto(
                " Registration Failed",
                com.gfc.enums.ResponseStatus.valueOf(exception.getMessage()), null, null,null);
        return response;
    }

    /**
     * This method is used to handle illegalArgumentException
     * @param exception
     * @return
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseDto illegalArgumentExceptionHandler(IllegalArgumentException exception) {
        return new ResponseDto(
                exception.getMessage() + " Registration Failed",
                com.gfc.enums.ResponseStatus.INVALID_REQUEST,
                null, null,null
        );
    }

    /**
     * This method is used to handle internal error, if any.
     * @param exception
     * @return
     */
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseDto internalErrors(MethodArgumentNotValidException exception) {
        BindingResult result = exception.getBindingResult();
        List<FieldError> errors = result.getFieldErrors();
        return processFieldErrors(errors);
    }

    /**
     * This method is used to handle process field errors
     *
     * @param errors
     * @return
     */
    private ResponseDto processFieldErrors(List<FieldError> errors) {
        List<MessageDto> messages = Lists.newArrayList();
        if (errors != null && errors.size() > 0) {
            for (FieldError error : errors) {
                Locale local = LocaleContextHolder.getLocale();
                String msg = messageSource.getMessage(error.getDefaultMessage(), null, local);
                messages.add(new MessageDto(msg, com.gfc.enums.ResponseStatus.INVALID_FIELD));
            }
        }
        ResponseDto response = new ResponseDto(
                "registration failed",
                com.gfc.enums.ResponseStatus.INVALID_FIELD,
                messages, null,null
        );
        response.setErrors(messages);
        return response;
    }
}