package com.gfc.users.accounts.controllers;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.builders.UserResponseBuilder;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by abhinav on 5/3/16.
 *
 * This class is used to handle user informations.
 */
@RestController
@RequestMapping(value = "/api/people")
public class UsersController {

    private UserService userService;
    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    /**
     * This method is used to find user details based on email
     *
     * @param email
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<ResponseDto> findByEmail(@RequestParam("email") String email) {
        ResponseDto response = null;
        try {
            response = userService.findByEmail(email);
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            response = UserResponseBuilder.transform(null,"INTERNAL_ERROR", ResponseStatus.INTERNAL_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity(response, HttpStatus.OK);
        }
    }

}
