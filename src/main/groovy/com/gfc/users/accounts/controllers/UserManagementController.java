package com.gfc.users.accounts.controllers;

import com.gfc.enums.ResponseStatus;
import com.gfc.models.dto.response.SearchDto;
import com.gfc.users.accounts.builders.UserResponseBuilder;
import com.gfc.users.accounts.models.dto.request.TeamMemberDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.services.UserRepositoryService;
import com.gfc.users.accounts.services.RegistrationService;
import com.gfc.users.accounts.services.TeamService;
import com.gfc.users.accounts.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Arun Kumar on 28-07-2016.
 */
@RestController
@RequestMapping(value = "/api")
public class UserManagementController {


    private UserService userService;
    private TeamService teamService;
    private RegistrationService registrationService;
    private static final Logger logger = LoggerFactory.getLogger(UserRepositoryService.class);
    @Autowired
    public UserManagementController(UserService userService, TeamService teamService,RegistrationService registrationService) {
        this.userService = userService;
        this.teamService=teamService;
        this.registrationService=registrationService;
    }
    @RequestMapping(value="/getTeamMembers", method = RequestMethod.GET)
    public List<User> findTeamMembers(@RequestParam("id") Integer id) {
        ResponseDto response = null;
        List<User> user=null;
        try {
            user = userService.findTeamMembers(id);
            return user;
           // return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
        } catch (Exception e) {
            response = UserResponseBuilder.transform(null,"INTERNAL_ERROR", ResponseStatus.INTERNAL_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
           // return new ResponseEntity(response, HttpStatus.OK);
            return null;
        }
    }
    @RequestMapping(value="/deleteTeamMember", method = RequestMethod.GET)
    public void deleteTeamMember(@RequestParam("id") Integer id) {
        ResponseDto response = null;
        try {
            userService.deleteTeamMember(id);
        } catch (Exception e) {
            logger.info("User Deleted........"+e);
        }
    }
    @RequestMapping(value = "getTeamMemberDetails", method = RequestMethod.GET )
    public ResponseEntity<ResponseDto> findTeamMemberDetailsById(@RequestParam("id") Integer id) {
        ResponseDto response = null;
        try {
            response = userService.findTeamMemberDetailsById(id);
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            response = UserResponseBuilder.transform(null,"INTERNAL_ERROR", ResponseStatus.INTERNAL_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity(response, HttpStatus.OK);
        }
    }

 /*   @RequestMapping(value="/getTeamName", method = RequestMethod.GET)
    public List<Team> findTeamName(@RequestParam("id") Integer id) {
        ResponseDto response = null;
        List<Team> team=null;
        try {
            team = teamService.findTeamName(id);
            return team;
            // return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
        } catch (Exception e) {
            response = UserResponseBuilder.transform(null,"INTERNAL_ERROR", ResponseStatus.INTERNAL_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
            // return new ResponseEntity(response, HttpStatus.OK);
            return null;
        }
    }*/
    @RequestMapping(value="/getAllTeams", method = RequestMethod.GET)
    public List<Team> findAllTeams() {
        ResponseDto response = null;
        List<Team> team=null;
        try {
            team = teamService.findAllTeams();
            return team;
            // return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
        } catch (Exception e) {
            response = UserResponseBuilder.transform(null,"INTERNAL_ERROR", ResponseStatus.INTERNAL_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
            // return new ResponseEntity(response, HttpStatus.OK);
            return null;
        }
    }

    /**
     * This method is used to register new users.
     *
     * @param request
     * @param device
     * @return
     */
    @RequestMapping(value = "/createTeamMember", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody @Validated TeamMemberDto request,
                                   @RequestHeader(value = "X-Device-Info" , defaultValue = "web-app" ,required = false) String device
    ){

        ResponseDto response = null;
        try {
            response = registrationService.createTeamMember(request, device);
            return new ResponseEntity(response, response.getHttpStatus());
        }catch (Exception e){
            e.printStackTrace();
         /*   response = new ResponseDto(request.getUser().getType() + " Registration Failed",
                    com.gfc.enums.ResponseStatus.INTERNAL_ERROR, null, null, HttpStatus.INTERNAL_SERVER_ERROR ,null
            );*/
            return new ResponseEntity(response,response.getHttpStatus());
        }

    }
}
