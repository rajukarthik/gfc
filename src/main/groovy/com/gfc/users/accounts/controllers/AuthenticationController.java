package com.gfc.users.accounts.controllers;

import com.gfc.constants.Constant;
import com.gfc.exceptions.InvalidActivationException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.users.accounts.models.dto.request.LoginRequestDto;
import com.gfc.users.accounts.models.dto.response.LoginResponseDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by abhinav on 5/3/16.
 *
 * This class is used to authentication user.
 */
@RestController
@RequestMapping("/api/people")
public class AuthenticationController {

    private AuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    /**
     * This method is used to authenticate user.
     *
     * @param loginDto
     * @param request
     * @return
     */
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<LoginResponseDto> login(@RequestBody @Validated LoginRequestDto loginDto, HttpServletRequest request) {
        LoginResponseDto response = null;
        try {
            response = authenticationService.login(loginDto);
            HttpHeaders authToken = new HttpHeaders();
            authToken.add("Auth-Token", response.getAuthToken());
            return new ResponseEntity(response, authToken, HttpStatus.OK);
        } catch (PersonNotFoundException e) {
            response = new LoginResponseDto(com.gfc.enums.ResponseStatus.valueOf(e.getMessage()), null, null,null,null,null);
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * This method is used to kill the active session.
     *
     * @param authToken
     * @return
     */
    @RequestMapping(value = "/secure/logout", method = RequestMethod.POST)
    public ResponseEntity<LoginResponseDto> logout(@RequestHeader(Constant.AUTH_TOKEN) String authToken) {
        LoginResponseDto response = null;
        try {
            response = authenticationService.logout(authToken);
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (PersonNotFoundException e) {
            response = new LoginResponseDto(com.gfc.enums.ResponseStatus.valueOf(e.getMessage()), null, null,null,null,null);
            return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
        }
    }


    /**
     * This method is used to activate registered account.
     *
     * @param token
     * @param httpServletResponse
     * @throws IOException
     */
    @RequestMapping(value = "/activate", method = RequestMethod.GET)
    public void activate(
            @RequestParam(value = "token", required = true) String token,
            HttpServletResponse httpServletResponse) throws IOException {
        ResponseDto response = null;
        try {
            authenticationService.activate(token);
            httpServletResponse.setStatus(HttpServletResponse.SC_ACCEPTED);

        } catch (InvalidActivationException e) {
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } catch (PersonNotFoundException e) {
            httpServletResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } catch (Exception e) {
            httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
