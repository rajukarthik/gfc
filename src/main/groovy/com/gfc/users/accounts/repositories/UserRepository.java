package com.gfc.users.accounts.repositories;

import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.models.entities.UserTeamRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by abhinav on 20/12/15.
 *
 * This interface is used for user ralated info.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	public User findById(Integer id);

	public User findByEmail(String email);

	@Transactional
	@Query(value = "from User u where u.id=?1 and u.is_deleted=false and u.status='ACTIVE' and u.type='TEAM_MEMBER'")
	public User findTeamMembers(Integer id);


	/*@Transactional
	@Query(value = "from UserTeamRole u where u.userId=?1")
	public UserTeamRole findTeam(Integer id);
*/
	@Transactional
	@Query(value = "from UserTeamRole u where u.teamId=?1")
	public List<UserTeamRole> findAllPeopleId(Integer id);

	public User findByEmailAndStatus(String email, ActivityStatus status);

	public User findByEmailAndPasswordAndStatus(String email, String password, ActivityStatus status);

	public User findByIdAndStatus(Integer id, ActivityStatus status);

	@Modifying
	@Transactional
	@Query(value = "update User u set u.is_deleted=true,u.status='PENDING' where u.id=?1")
	public void delete(Integer id);

}
