package com.gfc.users.accounts.repositories.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.InvalidActivationException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.models.entities.Activation;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.UserActivationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by abhinav on 27/12/15.
 *
 * This class is used as service class to handle user activation
 */
@Service
public class UserActivationRepositoryService {

    private UserActivationRepository activationRepository;
    private UserRepositoryService userRepositoryService;

    @Autowired
    public UserActivationRepositoryService(
            UserActivationRepository activationRepository,
            UserRepositoryService userRepositoryService
    ) {
        this.activationRepository = activationRepository;
        this.userRepositoryService = userRepositoryService;
    }

    /**
     * This method is used to activate user.
     *
     * @param token
     * @return
     * @throws InvalidActivationException
     * @throws PersonNotFoundException
     */
    public ResponseDto activate(String token) throws InvalidActivationException, PersonNotFoundException {
        Activation personActivation = this.findByAuthToken(token);
        if(personActivation==null || personActivation.getStatus().equals(ActivityStatus.INACTIVE)) {
        	throw new InvalidActivationException(ResponseStatus.INVALID_ACTIVATION_TOKEN.getStatus());
        }
        userRepositoryService.activate(personActivation.getPeople().getId());
        personActivation.setStatus(ActivityStatus.INACTIVE);
        update(personActivation);
        return new ResponseDto("User Activation Successful", ResponseStatus.SUCCESS, null, null,null);
    }

    /**
     * This method is used to find activation details based on authtoken
     *
     * @param token
     * @return
     * @throws InvalidActivationException
     */
    public Activation findByAuthToken(String token) throws InvalidActivationException {
        Activation personActivation = activationRepository.findByAuthToken(token);

        if (personActivation == null) {
            throw new InvalidActivationException(ResponseStatus.INVALID_REQUEST.getStatus());
        }

        return personActivation;
    }

    /**
     * This method is used to update activation detials.
     *
     * @param personActivation
     * @return
     */
    public Activation update(Activation personActivation) {
        return activationRepository.saveAndFlush(personActivation);
    }

    /**
     * This method is used to create activation.
     * @param activation
     * @return
     */
    public Activation create(Activation activation){
        if(activationRepository.findByAuthToken(activation.getAuthToken())==null) {
            return activationRepository.saveAndFlush(activation);
        }else{
            return null;
        }
    }

    /**
     * This method used to validate based on authtoken.
     *
     * @param token
     * @return
     * @throws InvalidActivationException
     */
    public Integer validateAuthToken(String token) throws InvalidActivationException {
        Activation personActivation = this.findByAuthToken(token);

        if(null == personActivation) {
            throw new InvalidActivationException(ResponseStatus.INVALID_ACTIVATION_TOKEN.getStatus());
        }

        if(null != personActivation && personActivation.getStatus().equals(ActivityStatus.INACTIVE)) {
            throw new InvalidActivationException(ResponseStatus.INVALID_ACTIVATION_TOKEN.getStatus());
        }

        personActivation.setStatus(ActivityStatus.INACTIVE);
        personActivation = activationRepository.saveAndFlush(personActivation);
        User people = personActivation.getPeople();

        return people.getId();
    }
}
