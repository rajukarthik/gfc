package com.gfc.users.accounts.repositories.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.RegistrationException;
import com.gfc.users.accounts.enums.PersonRole;
import com.gfc.users.accounts.enums.TeamType;
import com.gfc.users.accounts.exceptions.NotFoundException;
import com.gfc.users.accounts.models.entities.*;
import com.gfc.users.accounts.repositories.RoleRepository;
import com.gfc.users.accounts.repositories.TeamRepository;
import com.gfc.users.accounts.repositories.UserRepository;
import com.gfc.users.accounts.repositories.UserTeamRoleRepository;
import com.gfc.users.accounts.transformers.UserTeamRoleTransformer;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

/**
 * Created by abhinav on 23/12/15.
 *
 * This class is used as service class to handle team.
 */
@Service
public class TeamRepositoryService {

    private static final Logger logger = LoggerFactory.getLogger(TeamRepositoryService.class);

    private TeamRepository teamRepository;
    private UserRepositoryService userRepositoryService;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private UserTeamRoleRepository userTeamRoleRepository;

    @Autowired
    public TeamRepositoryService(
            TeamRepository teamRepository,
            UserRepositoryService userRepositoryService,
            RoleRepository roleRepository,
            UserRepository userRepository,
            UserTeamRoleRepository personTeamRoleRepository
    ) {
        this.teamRepository = teamRepository;
        this.userRepositoryService = userRepositoryService;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.userTeamRoleRepository = personTeamRoleRepository;
    }

    /**
     * This method is used to create team.
     *
     * @param team
     * @param user
     * @return
     * @throws RegistrationException
     * @throws NotFoundException
     */
    @Transactional
    public Team create(Team team, User user) throws RegistrationException,NotFoundException{
        user = userRepositoryService.create(user);
        team.setType(TeamType.INDEPENDENT);
        Team persistedTeam = teamRepository.save(team);
        Role role = roleRepository.findByCode(PersonRole.TEAM_ADMIN);
        UserTeamRole mapper = UserTeamRoleTransformer.transform(user,persistedTeam,role);
        userTeamRoleRepository.save(mapper);
        return persistedTeam;
    }

    /**
     * This method is used to update team details.
     *
     * @param team
     * @return
     * @throws NotFoundException
     */
    @Transactional
    public Team update(Team team) throws NotFoundException {
        Team teamToBeUpdated = findById(team.getId());
        if(Objects.isNull(team)){
            throw new NotFoundException("TEAM_"+ResponseStatus.NOT_FOUND);
        }
        teamToBeUpdated.setName(team.getName());
        teamToBeUpdated = teamRepository.saveAndFlush(teamToBeUpdated);
        return teamToBeUpdated;
    }

    /**
     * This method is used to delete team based on id.
     *
     * @param id
     * @return
     */
    public boolean delete(int id){
      teamRepository.delete(id);
      return true;
    }

    /**
     * This method is used to delete team based on team details.
     *
     * @param team
     * @return
     */
    public boolean delete(Team team){
        teamRepository.delete(team);
        return true;
    }

    /**
     * This method is used to find team based on id.
     *
     * @param id
     * @return
     */
    public Team findById(Integer id){
        Team team = teamRepository.findOne(id);
        return team;
    }

    /**
     * This method is used to delete all the teams.
     *
     * @param userId
     * @return
     */
    public boolean deleteAll(int userId){
        try {
            List<UserTeamRole> userTeamRoles = userTeamRoleRepository.findByUserId(userId);
            List<Team> teams = Lists.newArrayList();
            for (UserTeamRole userTeamRole : userTeamRoles) {
                teams.add(teamRepository.findOne(userTeamRole.getTeamId()));
            }
            teamRepository.delete(teams);
            userTeamRoleRepository.delete(userTeamRoles);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    public List<Team> findAllTeams() {
        List<Team> team = teamRepository.findAllTeams();
        return team;
    }
    public List<Team> findTeamName(Integer id) {
       // UserTeamRole team = personTeamRoleRepository.findByUserId(id);
        List<Team> teamName=teamRepository.findTeamName(userTeamRoleRepository.findByUserId(id).get(0).getTeamId());
        return teamName;
    }
    public List<Team> findTeam(Integer id) {
        // UserTeamRole team = personTeamRoleRepository.findByUserId(id);
        List<Team> teamName=teamRepository.findTeamName(id);
        return teamName;
    }

}
