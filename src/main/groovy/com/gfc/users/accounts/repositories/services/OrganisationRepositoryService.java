package com.gfc.users.accounts.repositories.services;

import com.gfc.users.accounts.exceptions.NotFoundException;
import com.gfc.users.accounts.models.entities.Organisation;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.OrganisationRepository;
import com.gfc.users.accounts.transformers.OrganisationTransformer;
import com.gfc.users.accounts.transformers.UserTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by abhinav on 23/12/15.
 *
 * This class is used as service class to handle organisation.
 */
@Service
public class OrganisationRepositoryService {

    private OrganisationRepository organisationRepository;
    private UserRepositoryService userRepositoryService;

    @Autowired
    public OrganisationRepositoryService(
            OrganisationRepository organisationRepository,
            UserRepositoryService userRepositoryService
    ) {
        this.organisationRepository = organisationRepository;
        this.userRepositoryService = userRepositoryService;
    }

    /**
     * This method is used to create organisation registration.
     *
     * @param organisation
     * @return
     */
    public Organisation create(Organisation organisation){
        Organisation persistedOrganisation = organisationRepository.save(organisation);
        persistedOrganisation.getOwner();
        return persistedOrganisation;
    }

    /**
     * This method is used to update organisation details.
     *
     * @param organisation
     * @return
     * @throws NotFoundException
     */
    public Organisation update(Organisation organisation) throws NotFoundException {
        User userToBeUpdated = userRepositoryService.findById(organisation.getOwner().getId());
        Organisation organisationToBeUpdated = organisationRepository.findOne(organisation.getId());
        User updatedUser = UserTransformer.transformDtoToEntity(userToBeUpdated,organisation.getOwner());
        Organisation updatedOrganisation = OrganisationTransformer.modify(organisationToBeUpdated, organisation,userToBeUpdated);
        return organisationRepository.saveAndFlush(updatedOrganisation);
    }

    /**
     * This method is used to delete Organisation details.
     *
     * @param organisation
     * @return
     */
    public boolean delete(Organisation organisation){
        try {
            organisationRepository.delete(organisation.getId());
            return true;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * This method is used to delete based on id.
     *
     * @param id
     * @return
     */
    public boolean delete(Integer id){
        try {
            organisationRepository.delete(id);
            return true;
        }catch (Exception e){
            return false;
        }
    }


    /**
     * This method is used to find organisation details based on owner info.
     *
     * @param owner
     * @return
     */
    public Organisation findByOwner(User owner){
        return organisationRepository.findByOwner(owner);
    }

    /**
     * This method is used to find organisation details based on name.
     *
     * @param name
     * @return
     */
    public Organisation findByName(String name){
        return organisationRepository.findByName(name);
    }
}
