package com.gfc.users.accounts.repositories;

import com.gfc.users.accounts.models.entities.LoginActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by abhinav on 3/1/16.
 *
 * This interface is used for login related activity.
 */
@Repository
public interface LoginActivityRepository
        extends JpaRepository<LoginActivity, Integer> {
   public LoginActivity findByPeople(Integer userId);
   public LoginActivity findByAuthToken(String token);
}
