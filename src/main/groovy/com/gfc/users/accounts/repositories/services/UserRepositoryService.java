package com.gfc.users.accounts.repositories.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.models.entities.UserTeamRole;
import com.gfc.users.accounts.repositories.UserRepository;
import com.gfc.utils.EncryptUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhinav on 20/12/15.
 * <p>
 * This class is used as service class to handle user detials.
 */
@Service
public class UserRepositoryService {

    private UserRepository userRepository;
    private static final Logger logger = LoggerFactory.getLogger(UserRepositoryService.class);


    @Autowired
    public UserRepositoryService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * This method is used to create user.
     *
     * @param user
     * @return
     */
    public User create(User user) {
        return userRepository.save(user);
    }

    /**
     * This method is used to update user details.
     *
     * @param user
     * @return
     */
    public User update(User user) {
        return userRepository.saveAndFlush(user);
    }

    /**
     * This method is used to find used by email.
     *
     * @param email
     * @return
     */
    public User findByEmail(String email) {
        logger.info("The email is " + email);
        return userRepository.findByEmail(email);
    }

    public List<User> findTeamMembers(Integer id) {
        List<User> user=new ArrayList<>();
        List<UserTeamRole> teams=null;
        teams=userRepository.findAllPeopleId(id);
        for (UserTeamRole people: teams) {
            Integer peopleId=people.getUserId();
            User person = userRepository.findTeamMembers(peopleId);
            if(person!=null)
                user.add(person);
        }
        return user;
    }

    public void deleteTeamMember(Integer id) {
        userRepository.delete(id);
    }

    /**
     * This method is used to activate user.
     *
     * @param id
     * @return
     * @throws PersonNotFoundException
     */
    public User activate(Integer id) throws PersonNotFoundException {
        User person = userRepository.findOne(id);
        if (person != null) {
            person.setStatus(ActivityStatus.ACTIVE);
            person = userRepository.saveAndFlush(person);
        } else {
            throw new PersonNotFoundException(ResponseStatus.PERSON_NOT_FOUND.getStatus());
        }
        return person;
    }

    /**
     * This method is used to find user based on email and status.
     *
     * @param email
     * @param status
     * @return
     * @throws PersonNotFoundException
     */
    public User findByEmailAndStatus(String email, ActivityStatus status) throws PersonNotFoundException {
        User person = userRepository.findByEmail(email);
        if (person == null)
            throw new PersonNotFoundException(ResponseStatus.USER_NOT_FOUND.getStatus());
        if (person.getStatus().equals(ActivityStatus.PENDING))
            throw new PersonNotFoundException(ResponseStatus.ACTIVATION_PENDING.getStatus());
        return person;
    }

    /**
     * This method is used to find user based on id.
     *
     * @param id
     * @return
     */
    public User findById(Integer id) {
        User person = userRepository.findOne(id);
        return person;
    }

    /**
     * This method is used to find user based on id and password and status.
     *
     * @param id
     * @param password
     * @param status
     * @return
     */
    public User findByIdAndPasswordAndStatus(Integer id, String password, ActivityStatus status) {
        String encryptedPassword = EncryptUtils.encryptMD5(password);
        User person = userRepository.findByIdAndStatus(id, status);
        if (person.getPassword().equals(encryptedPassword)) {
            return person;
        }
        return null;
    }

    public User findTeamMemberDetailsById(Integer id) {
        User person = userRepository.findById(id);
        //  logger.info("The user---------------------------- " + person);
        return person;
    }
}
