package com.gfc.users.accounts.repositories;

import com.gfc.users.accounts.enums.PersonRole;
import com.gfc.users.accounts.models.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by gurinder on 27/12/15.
 *
 * This interface is used for role
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    public Role findByCode(PersonRole personRole);
}
