package com.gfc.users.accounts.repositories.services;

import com.gfc.users.accounts.enums.PersonRole;
import com.gfc.users.accounts.models.entities.Role;
import com.gfc.users.accounts.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by gurinder on 27/12/15.
 *
 * This class is used as service class to handle roles
 */
@Service
public class RoleRepositoryService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleRepositoryService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    /**
     * This method to find roles based on person detials.
     *
     * @param personRole
     * @return
     */
    public Role findByCode(PersonRole personRole) {
        return roleRepository.findByCode(personRole);
    }


}
