package com.gfc.users.accounts.repositories.services;

import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.models.entities.UserTeamRole;
import com.gfc.users.accounts.repositories.UserTeamRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by abhinav on 13/3/16.
 *
 * This class is used as service class to handle user team role
 */
@Service
public class UserTeamRoleRepositoryService {

    private UserTeamRoleRepository userTeamRoleRepository;

    @Autowired
    public UserTeamRoleRepositoryService(UserTeamRoleRepository userTeamRoleRepository) {
        this.userTeamRoleRepository = userTeamRoleRepository;
    }
    public UserTeamRole findByUserId(Integer id){
        return userTeamRoleRepository.findByUserId(id).get(0);
    }
}
