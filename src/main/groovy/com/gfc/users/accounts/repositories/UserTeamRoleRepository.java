package com.gfc.users.accounts.repositories;

import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.models.entities.UserTeamRole;
import com.gfc.users.accounts.models.entities.UserTeamRoleId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by abhinav on 23/12/15.
 *
 * This interface is used for user team role.
 */
public interface UserTeamRoleRepository
        extends JpaRepository<UserTeamRole, UserTeamRoleId> {

    public List<UserTeamRole> findByUserId(Integer id);

    //public UserTeamRole findByOwner(User owner);
}