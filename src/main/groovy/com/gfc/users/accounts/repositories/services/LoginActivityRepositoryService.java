package com.gfc.users.accounts.repositories.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.LoginActivity;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.LoginActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * Created by abhinav on 3/1/16.
 *
 * This class is used as service class to handle login activities.
 */
@Service
public class LoginActivityRepositoryService {

	private LoginActivityRepository loginActivityRepository;
	private UserRepositoryService personRepositoryService;

	@Autowired
	public LoginActivityRepositoryService(LoginActivityRepository loginActivityRepository,
										  UserRepositoryService personRepositoryService) {
		this.loginActivityRepository = loginActivityRepository;
		this.personRepositoryService = personRepositoryService;
	}

	/**
	 * This method is used for creating new registration entry.
	 *
	 * @param people
	 * @param status
	 * @return
	 */
	public LoginActivity createLoginActivity(User people, ActivityStatus status) {
		LoginActivity loginActivity = new LoginActivity();
		loginActivity.setAuthToken(UUID.randomUUID().toString());
		loginActivity.setStatus(status);
		loginActivity.setPeople(people);

		return loginActivityRepository.save(loginActivity);
	}

	/**
	 * This method is used to delete the login activity.
	 *
	 * @param loginActivity
	 * @return
	 * @throws PersonNotFoundException
	 */
	public Boolean delete(LoginActivity loginActivity) throws PersonNotFoundException {
		loginActivity = loginActivityRepository.findOne(loginActivity.getId());

		if (loginActivity == null || !loginActivity.getStatus().equals(ActivityStatus.ACTIVE))
			throw new PersonNotFoundException(ResponseStatus.INVALID_AUTH_TOKEN.getStatus());

		loginActivityRepository.delete(loginActivity);

		return true;
	}

	/**
	 * This method is used to delete login activity based on authtoken
	 * @param authToken
	 * @return
	 * @throws PersonNotFoundException
	 */
	public Boolean delete(String authToken) throws PersonNotFoundException {
		System.out.println("*******************************");
		System.out.println(authToken + " ");
		System.out.println("*******************************");
		LoginActivity loginActivity = loginActivityRepository.findByAuthToken(authToken);
		if (loginActivity == null || !loginActivity.getStatus().equals(ActivityStatus.ACTIVE)) {
			System.out.println("*******************************");
			System.out.println(loginActivity.getAuthToken() + " ");
			System.out.println("*******************************");
			throw new PersonNotFoundException(ResponseStatus.INVALID_AUTH_TOKEN.getStatus());
		}
		loginActivityRepository.delete(loginActivity);
		return true;
	}

	/**
	 * This method is used to find login related info based on authtoken
	 * @param token
	 * @return
	 * @throws PersonNotFoundException
	 */
	public LoginActivity findByAuthToken(String token) throws PersonNotFoundException {
		LoginActivity loginActivity = loginActivityRepository.findByAuthToken(token);

		if (loginActivity == null || !loginActivity.getStatus().equals(ActivityStatus.ACTIVE)) {
			throw new PersonNotFoundException(ResponseStatus.INVALID_AUTH_TOKEN.getStatus());
		}

		return loginActivity;
	}

	/**
	 * This method is used to update login activity.
	 * @param activity
	 * @return
	 */
	public LoginActivity updateLoginActivity(LoginActivity activity) {
		activity.setUpdatedDt(new Date());
		return loginActivityRepository.saveAndFlush(activity);
	}	
}
