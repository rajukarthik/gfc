package com.gfc.users.accounts.repositories;

import com.gfc.users.accounts.models.entities.Activation;
import com.gfc.users.accounts.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by gurinder on 22/12/15.
 *
 * This interface is used for user activation
 */
@Repository
public interface UserActivationRepository extends JpaRepository<Activation, Integer> {
    public Activation findByAuthToken(String authToken);

    public Activation findByPeople(User person);
}
