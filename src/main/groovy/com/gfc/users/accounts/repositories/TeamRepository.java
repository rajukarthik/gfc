package com.gfc.users.accounts.repositories;

import com.gfc.users.accounts.models.entities.Organisation;
import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.models.entities.UserTeamRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by abhinav on 21/12/15.
 *
 * This interface is used for team
 */
@Repository
@Transactional
public interface TeamRepository extends JpaRepository<Team, Integer> {

    @Query(value = "select t.id,t.name from Team t")
    public List<Team> findAllTeams();

    @Query(value = "select t.id,t.name from Team t where t.id=?1")
    public List<Team> findTeamName(Integer id);

}
