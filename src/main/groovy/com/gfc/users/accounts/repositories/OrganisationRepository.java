package com.gfc.users.accounts.repositories;

import com.gfc.users.accounts.models.entities.Organisation;
import com.gfc.users.accounts.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by abhinav on 21/12/15.
 *
 * This interface is used for organisation details.
 */
@Repository
public interface OrganisationRepository extends JpaRepository<Organisation, Integer> {

    public Organisation findByOwner(User owner);

    public Organisation findByName(String name);

}
