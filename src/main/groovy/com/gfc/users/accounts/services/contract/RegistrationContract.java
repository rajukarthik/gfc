package com.gfc.users.accounts.services.contract;

import com.gfc.users.accounts.models.dto.request.RegistrationDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import org.springframework.stereotype.Component;

/**
 * Created by abhinav on 1/4/16.
 *
 * This class is a component for registration.
 */
@Component
public interface RegistrationContract {
    public ResponseDto register(RegistrationDto request);
}
