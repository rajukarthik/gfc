package com.gfc.users.accounts.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.builders.RegistrationResponseBuilder;
import com.gfc.users.accounts.enums.SubscriptionType;
import com.gfc.users.accounts.models.dto.request.RegistrationDto;
import com.gfc.users.accounts.models.dto.request.TeamMemberDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.google.common.base.CaseFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.gfc.users.accounts.factories.RegistrationServiceFactory;


/**
 * Created by abhinav on 19/12/15.
 *
 * This class is used as service class to handle registration
 */
@Service
public class RegistrationService {

    private UserService userService;
    private TeamService teamService;
    private OrganisationService organisationService;
    //private RegistrationServiceFactory factory;

    private static final Logger logger = LoggerFactory.getLogger(RegistrationService.class);

    @Autowired
    public RegistrationService(TeamService teamService, OrganisationService organisationService, UserService userService) {
        this.organisationService = organisationService;
        this.teamService = teamService;
        this.userService = userService;
    }

    /**
     * This method is used to handle business logic for registrations.
     *
     * @param request
     * @param device
     * @return
     */
    public ResponseDto register(RegistrationDto request, String device) {
        ResponseDto response = null;
        if (request.getTeam() != null && request.getUser().getType().equals(SubscriptionType.TEAM)) {
            response = teamService.register(request);
        } else if (request.getOrganisation() != null && request.getUser().getType().equals(SubscriptionType.ORGANISATION)) {
            response = organisationService.register(request);
        } else if (request.getUser().getType().equals(SubscriptionType.INDIVIDUAL)) {
            response = userService.register(request);
        } else {
            return RegistrationResponseBuilder.build(request,ResponseStatus.INVALID_REQUEST);
        }

//        response = factory.getService(request.getUser().getType().getType()).register(request);
        return userService.activate(request,device);
    }

    /**
     * This method is used to handle business logic for teamMemberCreation.
     *
     * @param request
     * @param device
     * @return
     */
    public ResponseDto createTeamMember(TeamMemberDto request, String device) {
        ResponseDto response = userService.createTeamMember(request);

//        response = factory.getService(request.getUser().getType().getType()).register(request);
        return response;
    }




    /**
     * This method is used to generate formatted names.
     *
     * @param request
     * @return
     */
    private String getFormattedName(RegistrationDto request) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, request.getUser().getType().getType().toLowerCase());
    }
}
