package com.gfc.users.accounts.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.enums.SubscriptionType;
import com.gfc.users.accounts.models.dto.request.AccountDetailsDto;
import com.gfc.users.accounts.models.dto.request.PasswordRequestDto;
import com.gfc.users.accounts.models.dto.response.ProfileResponseDto;
import com.gfc.users.accounts.models.dto.response.StatusDto;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.services.UserRepositoryService;
import com.gfc.utils.EncryptUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Created by abhinav on 5/3/16.
 *
 * This class is used as service class to handle accounts
 */
@Service
public class AccountsService {

    private UserService userService;
    private TeamService teamService;
    private OrganisationService organisationService;
    private UserRepositoryService userRepositoryService;

    @Autowired
    public AccountsService(UserService userService, TeamService teamService, OrganisationService organisationService, UserRepositoryService userRepositoryService) {
        this.userService = userService;
        this.teamService = teamService;
        this.organisationService = organisationService;
        this.userRepositoryService = userRepositoryService;

    }

    /**
     * This method is used to update aacount detials.
     *
     * @param request
     * @return
     */
    public ProfileResponseDto update(AccountDetailsDto request){
        ProfileResponseDto response = null;
        if(request.getChangedDetails().getType().equals(SubscriptionType.TEAM)) {
            response = teamService.update(request);
        } else if(request.getChangedDetails().getType().equals(SubscriptionType.ORGANISATION)) {
            response = organisationService.update(request);
        } else if(request.getChangedDetails().getType().equals(SubscriptionType.INDIVIDUAL)) {
            response  = userService.update(request);
        } else {
           response = new ProfileResponseDto();
           response.setStatus(new StatusDto("PROFILE UPDATE FAILED", ResponseStatus.INTERNAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR));;
        }
        return response;
    }

    /**
     * This method is used to changes password.
     *
     * @param request
     * @return
     */
    public StatusDto changePassword(PasswordRequestDto request){

      User user = userRepositoryService.findByIdAndPasswordAndStatus(request.getUserId(),request.getCurrentPassword(), ActivityStatus.ACTIVE);
      if(user==null){
            return new StatusDto("Old password is incorrect",ResponseStatus.NOT_FOUND, HttpStatus.NOT_FOUND);
      }
      if(!request.getNewPassword().equals(request.getRepeatPassword())){
           return new StatusDto("Current password mismatch with repeat password",ResponseStatus.PASSWORD_MISMATCH, HttpStatus.PRECONDITION_FAILED);
      }
      try {
          user.setPassword(EncryptUtils.encryptMD5(request.getNewPassword()));
          userRepositoryService.update(user);
          return new StatusDto("Password is changed successfully.",ResponseStatus.SUCCESS, HttpStatus.OK);
      }catch (Exception e){
          e.printStackTrace();
          return new StatusDto("Change password failed",ResponseStatus.INTERNAL_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }

    /**
     * This method is used to validate password.
     *
     * @param request
     * @return
     */
    public StatusDto validatePassword(PasswordRequestDto request){
        User user = userRepositoryService.findByIdAndPasswordAndStatus(request.getUserId(),request.getCurrentPassword(), ActivityStatus.ACTIVE);
        if(user==null){
            return new StatusDto("Old password is incorrect",ResponseStatus.NOT_FOUND, HttpStatus.OK);
        }else{
            return new StatusDto("Password found",ResponseStatus.SUCCESS, HttpStatus.OK);
        }
    }
}
