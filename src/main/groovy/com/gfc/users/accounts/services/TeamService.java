package com.gfc.users.accounts.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.builders.RegistrationResponseBuilder;
import com.gfc.users.accounts.enums.PersonType;
import com.gfc.users.accounts.enums.SubscriptionType;
import com.gfc.users.accounts.models.dto.request.AccountDetailsDto;
import com.gfc.users.accounts.models.dto.request.RegistrationDto;
import com.gfc.users.accounts.models.dto.request.UserDto;
import com.gfc.users.accounts.models.dto.response.ProfileResponseDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.models.dto.response.StatusDto;
import com.gfc.users.accounts.models.entities.*;
import com.gfc.users.accounts.repositories.UserTeamRoleRepository;
import com.gfc.users.accounts.repositories.services.OrganisationRepositoryService;
import com.gfc.users.accounts.repositories.services.TeamRepositoryService;
import com.gfc.users.accounts.repositories.services.UserRepositoryService;
import com.gfc.users.accounts.transformers.TeamTransformer;
import com.gfc.users.accounts.transformers.UserTransformer;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

/**
 * Created by abhinav on 5/3/16.
 *
 * This class is used as service class to handle team
 */
@Service
public class TeamService{

    private UserRepositoryService userRepositoryService;
    private TeamRepositoryService teamRepositoryService;
    private OrganisationRepositoryService organisationRepositoryService;
    private UserTeamRoleRepository userTeamRoleRepository;

    @Autowired
    public TeamService(
            UserRepositoryService userRepositoryService,
            TeamRepositoryService teamRepositoryService,
            OrganisationRepositoryService organisationRepositoryService,
            UserTeamRoleRepository userTeamRoleRepository
    ) {
        this.userRepositoryService = userRepositoryService;
        this.teamRepositoryService = teamRepositoryService;
        this.organisationRepositoryService = organisationRepositoryService;
        this.userTeamRoleRepository = userTeamRoleRepository;
    }

    /**
     * This method is used to handle team registration
     *
     * @param request
     * @return
     */
    @Transactional
    public ResponseDto register(RegistrationDto request){
        User user = UserTransformer.transformDtoToEntity(request.getUser());
        user.setType(PersonType.TEAM_ADMIN);
        Team team = TeamTransformer.transformDtoToEntity(request.getTeam());
        ResponseDto response = null;

        try {
            if (teamRepositoryService.create(team, user) != null) {
                request.setUser(UserTransformer.transformEntityToDto(user));
                return RegistrationResponseBuilder.build(request,ResponseStatus.SUCCESS);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return RegistrationResponseBuilder.build(request,ResponseStatus.INTERNAL_ERROR);
    }

    /**
     * This method is used to update team info.
     *
     * @param request
     * @return
     */
    public ProfileResponseDto update(AccountDetailsDto request){
        ProfileResponseDto response = new ProfileResponseDto();
        try {
            User existingUser = userRepositoryService.findById(request.getUserId());
            Team existingTeam = null;

            if(request.getType().equals(SubscriptionType.TEAM)) {
                 existingTeam = teamRepositoryService.findById(request.getTeam().getId());
                if(Objects.isNull(existingTeam)){
                    response.setStatus(new StatusDto("CURRENT TEAM NOT FOUND", ResponseStatus.NOT_FOUND, HttpStatus.NOT_FOUND));
                    return response;
                }
            }

            if(
                existingUser.getFirstName().equals(request.getChangedDetails().getFirstName()) &&
                existingUser.getLastName().equals(request.getChangedDetails().getLastName()) &&
                request.getChangedDetails().getType().equals(existingUser.getSubscriptionType())
              ){
                response.setStatus(new StatusDto("PROFILE DATA NOT MODIFIED", ResponseStatus.ALREADY_PRESENT, HttpStatus.NOT_MODIFIED));
                return response;
            }

            if(!Strings.isNullOrEmpty(request.getChangedDetails().getFirstName()) &&!existingUser.getFirstName().equals(request.getChangedDetails().getFirstName())){
                existingUser.setFirstName(request.getChangedDetails().getFirstName());
            }

            if(!Strings.isNullOrEmpty(request.getChangedDetails().getLastName()) &&!existingUser.getLastName().equals(request.getChangedDetails().getLastName())){
                existingUser.setLastName(request.getChangedDetails().getLastName());
            }

            if(existingUser.getType().equals(PersonType.ORGANISATION_OWNER)){
                Organisation currentOrganisation = organisationRepositoryService.findByOwner(existingUser);
                organisationRepositoryService.delete(currentOrganisation);
            }

            existingUser.setType(PersonType.TEAM_MEMBER);
            existingUser.setSubscriptionType(SubscriptionType.TEAM);
            User updatedUser = userRepositoryService.update(existingUser);
            response.setUser(updatedUser);
            response.setStatus(new StatusDto("Profile updated successfully.", ResponseStatus.SUCCESS, HttpStatus.CREATED));
            return response;
        }catch (Exception e){
           e.printStackTrace();
           response.setStatus(new StatusDto("PROFILE UPDATE FAILED", ResponseStatus.INTERNAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR));
           return response;
        }
    }

   /* public Team createTeamMember(UserDto userDto, int teamAdminId){

        User user = UserTransformer.transformDtoToEntity(userDto);
        user.setType(PersonType.TEAM_MEMBER);
        user.setSubscriptionType(SubscriptionType.TEAM);
        User user1  = userRepositoryService.create(user);
        List<UserTeamRole> userTeam= userTeamRoleRepository.findByUserId(teamAdminId);
        Team team = userTeam.get(0).getTeam();

        UserTeamRole userTeamRole = new UserTeamRole();
        userTeamRole.setRoleId(4);
        userTeamRole.setTeam(team);
        userTeamRole.setTeamId(team.getId());
        userTeamRole.setUser(user);
        userTeamRole.setUserId(user.getId());

        //userTeamRoleRepository.create(userTeamRole);



        *//*try {
            if((userRepositoryService.create(user))!=null){
                return RegistrationResponseBuilder.build(request,ResponseStatus.SUCCESS);
            }
        }catch (Exception e){
            e.printStackTrace();
        }*//*
        //return RegistrationResponseBuilder.build(request,ResponseStatus.INTERNAL_ERROR);

        return null;
    }*/
    public List<Team> findAllTeams(){
        List<Team> team = teamRepositoryService.findAllTeams();
       /* if (user == null){
            return UserResponseBuilder.transform(
                    null, ResponseStatus.EMAIL_NOT_REGISTERED.getStatus(), ResponseStatus.EMAIL_NOT_REGISTERED, HttpStatus.OK
            );
        }
        return UserResponseBuilder.transform(user,"success",ResponseStatus.SUCCESS,HttpStatus.OK);*/
        return team;

    }
    public List<Team> findTeamName(Integer id){
        List<Team> team = teamRepositoryService.findTeamName(id);
       /* if (user == null){
            return UserResponseBuilder.transform(
                    null, ResponseStatus.EMAIL_NOT_REGISTERED.getStatus(), ResponseStatus.EMAIL_NOT_REGISTERED, HttpStatus.OK
            );
        }
        return UserResponseBuilder.transform(user,"success",ResponseStatus.SUCCESS,HttpStatus.OK);*/
        return team;
    }
}
