package com.gfc.users.accounts.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.InvalidActivationException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.enums.PersonType;
import com.gfc.users.accounts.models.dto.request.LoginRequestDto;
import com.gfc.users.accounts.models.dto.response.LoginResponseDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.models.entities.*;
import com.gfc.users.accounts.repositories.services.*;
import com.gfc.utils.EncryptUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by abhinav on 5/3/16.
 * <p>
 * This class is used as service class to handle authentication
 */
@Service
public class AuthenticationService {

    private UserRepositoryService userRepositoryService;
    private UserActivationRepositoryService activationRepositoryService;
    private LoginActivityRepositoryService loginActivityRepositoryService;
    private OrganisationRepositoryService organisationRepositoryService;
    private TeamRepositoryService teamRepositoryService;
    private UserTeamRoleRepositoryService userTeamRoleRepositoryService;

    @Autowired
    public AuthenticationService(
            UserRepositoryService userRepositoryService,
            UserActivationRepositoryService activationRepositoryService,
            LoginActivityRepositoryService loginActivityRepositoryService,
            TeamRepositoryService teamRepositoryService,
            OrganisationRepositoryService organisationRepositoryService,
            UserTeamRoleRepositoryService userTeamRoleRepositoryService
    ) {
        this.userRepositoryService = userRepositoryService;
        this.activationRepositoryService = activationRepositoryService;
        this.loginActivityRepositoryService = loginActivityRepositoryService;
        this.teamRepositoryService = teamRepositoryService;
        this.organisationRepositoryService = organisationRepositoryService;
        this.userTeamRoleRepositoryService = userTeamRoleRepositoryService;
    }

    /**
     * This method is used to activate.
     *
     * @param token
     * @return
     * @throws InvalidActivationException
     * @throws PersonNotFoundException
     */
    public ResponseDto activate(String token)
            throws InvalidActivationException, PersonNotFoundException {
        return activationRepositoryService.activate(token);
    }

    /**
     * This method is used to authenticate based on login details by user.
     *
     * @param request
     * @return
     * @throws PersonNotFoundException
     */
    public LoginResponseDto login(LoginRequestDto request) throws PersonNotFoundException {
        User person = userRepositoryService.findByEmailAndStatus(request.getEmail(), ActivityStatus.ACTIVE);
        List<Team> team=null;
        String encryptedPassWord = EncryptUtils.encryptMD5(request.getPassword());
        if (!person.getPassword().equals(encryptedPassWord)) {
            throw new PersonNotFoundException(ResponseStatus.LOGIN_FAILED.getStatus());
        }

        if (person.getType().toString().equals("TEAM_ADMIN")) {
            UserTeamRole userTeamRole = userTeamRoleRepositoryService.findByUserId(person.getId());
            team = teamRepositoryService.findTeam(userTeamRole.getTeamId());
        }

        Organisation organisation = organisationRepositoryService.findByOwner(person);
        LoginActivity loginActivity = loginActivityRepositoryService.createLoginActivity(person, ActivityStatus.ACTIVE);
        return new LoginResponseDto(ResponseStatus.LOGIN_SUCCESS, person, person.getSubscriptionType(), organisation, team, loginActivity.getAuthToken());
    }

    /**
     * This method is used to kill the active session for the user.
     *
     * @param authToken
     * @return
     * @throws PersonNotFoundException
     */
    public LoginResponseDto logout(String authToken) throws PersonNotFoundException {
        loginActivityRepositoryService.delete(authToken);
        return new LoginResponseDto(ResponseStatus.LOGOUT_SUCCESS, null, null, null, null, null);
    }
}
