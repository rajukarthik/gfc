package com.gfc.users.accounts.services;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.builders.RegistrationResponseBuilder;
import com.gfc.users.accounts.enums.SubscriptionType;
import com.gfc.users.accounts.models.dto.request.AccountDetailsDto;
import com.gfc.users.accounts.models.dto.request.RegistrationDto;
import com.gfc.users.accounts.models.dto.response.ProfileResponseDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.models.dto.response.StatusDto;
import com.gfc.users.accounts.models.entities.Organisation;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.services.OrganisationRepositoryService;
import com.gfc.users.accounts.repositories.services.TeamRepositoryService;
import com.gfc.users.accounts.repositories.services.UserRepositoryService;
import com.gfc.users.accounts.transformers.OrganisationTransformer;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Created by abhinav on 5/3/16.
 *
 * This class is used as service class to handle organisation
 */
@Service
public class OrganisationService{

    private UserService userService;
    private UserRepositoryService userRepositoryService;
    private OrganisationRepositoryService organisationRepositoryService;
    private TeamRepositoryService teamRepositoryService;

    @Autowired
    public OrganisationService(
            OrganisationRepositoryService organisationRepositoryService,
            UserService userService,
            UserRepositoryService userRepositoryService,
            TeamRepositoryService teamRepositoryService) {
        this.organisationRepositoryService = organisationRepositoryService;
        this.userService = userService;
        this.userRepositoryService = userRepositoryService;
        this.teamRepositoryService = teamRepositoryService;
    }

    /**
     * This method is used to handle organisation registration.
     *
     * @param request
     * @return
     */
    public ResponseDto register(RegistrationDto request){

        Organisation organisation = OrganisationTransformer.transform(request.getOrganisation(),request.getUser());
        ResponseDto response = null;
        try {
            organisationRepositoryService.create(organisation);
            return RegistrationResponseBuilder.build(request,ResponseStatus.SUCCESS);
        }catch (Exception e){
            return RegistrationResponseBuilder.build(request,ResponseStatus.INTERNAL_ERROR);
        }
    }

    /**
     * This method is used to update organisation details.
     *
     * @param request
     * @return
     */
    public ProfileResponseDto update(AccountDetailsDto request){
        ProfileResponseDto response = new ProfileResponseDto();
        try {
            Organisation organisation = null;
            User user = userRepositoryService.findById(request.getUserId());
            if(user.getFirstName().equals(request.getChangedDetails().getFirstName()) &&
                    user.getLastName().equals(request.getChangedDetails().getLastName()) &&
                    request.getChangedDetails().getType().equals(user.getSubscriptionType())
              ){
                response.setStatus(new StatusDto("Profile date not modified.", ResponseStatus.ALREADY_PRESENT, HttpStatus.OK));
                return response;
            }

            if(request.getType().equals(SubscriptionType.TEAM)){
                teamRepositoryService.deleteAll(user.getId());
            }

            user.setSubscriptionType(SubscriptionType.ORGANISATION);

            if(!Strings.isNullOrEmpty(request.getChangedDetails().getFirstName()) && !user.getFirstName().equals(request.getChangedDetails().getFirstName())){
                user.setFirstName(request.getChangedDetails().getFirstName());
            }

            if(!Strings.isNullOrEmpty(request.getChangedDetails().getLastName()) &&!user.getLastName().equals(request.getChangedDetails().getLastName())){
                user.setLastName(request.getChangedDetails().getLastName());
            }
            user = userRepositoryService.update(user);

            response.setOrganisation(organisation);
            response.setUser(user);
            response.setStatus(new StatusDto("Profile updated successfully.", ResponseStatus.SUCCESS, HttpStatus.CREATED));
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus(new StatusDto("Profile update failed.", ResponseStatus.INTERNAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR));
        }
        return response;
    }
}