package com.gfc.users.accounts.services;

import com.gfc.communications.contract.impl.CommunicationProcessor;
import com.gfc.communications.enums.EmailType;
import com.gfc.communications.models.dto.CommunicationDto;
import com.gfc.communications.transformers.CommunicationBuilder;
import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.InvalidActivationException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.users.accounts.builders.RegistrationResponseBuilder;
import com.gfc.users.accounts.builders.UserResponseBuilder;
import com.gfc.users.accounts.enums.*;
import com.gfc.users.accounts.models.dto.request.*;
import com.gfc.users.accounts.models.dto.response.ProfileResponseDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.models.dto.response.StatusDto;
import com.gfc.users.accounts.models.dto.response.UserResponseDto;
import com.gfc.users.accounts.models.entities.*;
import com.gfc.users.accounts.repositories.RoleRepository;
import com.gfc.users.accounts.repositories.UserTeamRoleRepository;
import com.gfc.users.accounts.repositories.services.*;
import com.gfc.users.accounts.transformers.UserTeamRoleTransformer;
import com.gfc.users.accounts.transformers.UserTransformer;
import com.google.common.base.CaseFormat;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by abhinav on 5/3/16.
 *
 * This class is used as service class to handle user info.
 */
@Service
public class UserService{

    private UserRepositoryService userRepositoryService;
    private UserActivationRepositoryService activationRepositoryService;
    private OrganisationRepositoryService organisationRepositoryService;
    private TeamRepositoryService teamRepositoryService;
    private CommunicationProcessor communicationProcessor;
    private UserTeamRoleRepository userTeamRoleRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserService(
            UserRepositoryService personRepositoryService,
            UserActivationRepositoryService activationRepositoryService,
            OrganisationRepositoryService organisationRepositoryService,
            TeamRepositoryService teamRepositoryService,
            CommunicationProcessor communicationProcessor,UserTeamRoleRepository userTeamRoleRepository,RoleRepository roleRepository) {
        this.userRepositoryService = personRepositoryService;
        this.activationRepositoryService = activationRepositoryService;
        this.organisationRepositoryService = organisationRepositoryService;
        this.teamRepositoryService = teamRepositoryService;
        this.communicationProcessor = communicationProcessor;
        this.userTeamRoleRepository=userTeamRoleRepository;
        this.roleRepository=roleRepository;
    }

    /**
     * This method is used to find user by email.
     *
     * @param email
     * @return
     */
    public ResponseDto findByEmail(String email){
        User user = userRepositoryService.findByEmail(email);
        if (user == null){
            return UserResponseBuilder.transform(
                    null, ResponseStatus.EMAIL_NOT_REGISTERED.getStatus(), ResponseStatus.EMAIL_NOT_REGISTERED, HttpStatus.OK
            );
        }
        return UserResponseBuilder.transform(user,"success",ResponseStatus.SUCCESS,HttpStatus.OK);
    }
    public List<User> findTeamMembers(Integer id){
        List<User> user = userRepositoryService.findTeamMembers(id);
       /* if (user == null){
            return UserResponseBuilder.transform(
                    null, ResponseStatus.EMAIL_NOT_REGISTERED.getStatus(), ResponseStatus.EMAIL_NOT_REGISTERED, HttpStatus.OK
            );
        }
        return UserResponseBuilder.transform(user,"success",ResponseStatus.SUCCESS,HttpStatus.OK);*/
        return user;
    }

    public void deleteTeamMember(Integer id){
        userRepositoryService.deleteTeamMember(id);
        /*if (user == null){
            return UserResponseBuilder.transform(
                    null, ResponseStatus.EMAIL_NOT_REGISTERED.getStatus(), ResponseStatus.EMAIL_NOT_REGISTERED, HttpStatus.OK
            );
        }*/
        //return UserResponseBuilder.transform(user,"success",ResponseStatus.SUCCESS,HttpStatus.OK);
    }
    /**
     * This method is used to register user.
     *
     * @param request
     * @return
     */
    public ResponseDto register(RegistrationDto request){
        User user = UserTransformer.transformDtoToEntity(request.getUser());
        user.setType(PersonType.INDIVIDUAL);
        try {
            if((userRepositoryService.create(user))!=null){
                return RegistrationResponseBuilder.build(request,ResponseStatus.SUCCESS);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return RegistrationResponseBuilder.build(request,ResponseStatus.INTERNAL_ERROR);
    }



    /**
     * This method is used to create Team Member.
     *
     * @param request
     * @return
     */
    public ResponseDto createTeamMember(TeamMemberDto request){
        ResponseDto response = null;
        UserDto userDto=new UserDto();
        String pwd = "";
        String possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(int i = 0; i <= 8; i++) {

            pwd += possible. charAt((int) Math. floor(Math. random() * possible.length()));
        }
        userDto.setEmail(request.getEmail());
        userDto.setFirstName(request.getFirstName());
        userDto.setLastName(request.getLastName());
        userDto.setPassword(pwd);
        userDto.setType(SubscriptionType.TEAM);
        User user = UserTransformer.transformDtoToEntity(userDto);
        user.setType(PersonType.TEAM_MEMBER);
        user.setStatus(ActivityStatus.ACTIVE);
        try {
            if((userRepositoryService.create(user))!=null){
               Team team=new Team();
                team.setId(Integer.parseInt(request.getTeamId()));
                team.setName(request.getTeamName());
                team.setType(TeamType.INDEPENDENT);
                Role role = roleRepository.findByCode(PersonRole.TEAM_MEMBER);
                UserTeamRole mapper = UserTeamRoleTransformer.transform(user,team,role);
                userTeamRoleRepository.save(mapper);


                Activation activation = new Activation(user, ActivityStatus.ACTIVE);
                activation = activationRepositoryService.create(activation);
                CommunicationDto communication = CommunicationBuilder.buildEmail(user, activation.getAuthToken(), EmailType.INVITATION);
                response = communicationProcessor.communicate(communication);
                //return RegistrationResponseBuilder.build(null,ResponseStatus.SUCCESS);
                return response;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return RegistrationResponseBuilder.build(null,ResponseStatus.INTERNAL_ERROR);
    }
    /**
     * This method is used to validate user based on auth token
     * @param token
     * @return
     * @throws InvalidActivationException
     */
    public Integer validateAuthToken(String token)
            throws InvalidActivationException {
        return activationRepositoryService.validateAuthToken(token);
    }

    /**
     * This method is used to update user info.
     *
     * @param request
     * @return
     */
    public ProfileResponseDto update(AccountDetailsDto request){
        ProfileResponseDto response = new ProfileResponseDto();
        try {
            User oldUserData = userRepositoryService.findById(request.getUserId());
            if(oldUserData==null){
                response.setStatus(new StatusDto("PROFILE UPDATE FAILED", ResponseStatus.NOT_FOUND, HttpStatus.NOT_FOUND));
                return response;
            }
            if(
                oldUserData.getFirstName().equals(request.getChangedDetails().getFirstName()) &&
                oldUserData.getLastName().equals(request.getChangedDetails().getLastName()) &&
                request.getChangedDetails().getType().equals(oldUserData.getSubscriptionType())
              ){
                response.setStatus(new StatusDto("PROFILE DATA NOT MODIFIED", ResponseStatus.ALREADY_PRESENT, HttpStatus.OK));
                return response;
            }

            if(oldUserData.getSubscriptionType().equals(PersonType.ORGANISATION_OWNER)){
                if(oldUserData.getId().equals(request.getUserId())) {
                    organisationRepositoryService.delete(request.getOrganisation().getId());
                }else{
                    response.setStatus(new StatusDto("INVALID REQUEST", ResponseStatus.INVALID_REQUEST, HttpStatus.BAD_REQUEST));
                    return response;
                }
            }


            if(request.getType().equals(SubscriptionType.TEAM)){
                teamRepositoryService.deleteAll(oldUserData.getId());
            }

            oldUserData.setType(PersonType.INDIVIDUAL);
            oldUserData.setSubscriptionType(SubscriptionType.INDIVIDUAL);

            if(!Strings.isNullOrEmpty(request.getChangedDetails().getFirstName()) &&!oldUserData.getFirstName().equals(request.getChangedDetails().getFirstName())){
                oldUserData.setFirstName(request.getChangedDetails().getFirstName());
            }

            if(!Strings.isNullOrEmpty(request.getChangedDetails().getLastName()) &&!oldUserData.getLastName().equals(request.getChangedDetails().getLastName())){
                oldUserData.setLastName(request.getChangedDetails().getLastName());
            }

            User user = userRepositoryService.update(oldUserData);
            response.setUser(user);
            response.setStatus(new StatusDto("Profile updated successfully.", ResponseStatus.SUCCESS, HttpStatus.CREATED));
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus(new StatusDto("PROFILE UPDATE FAILED", ResponseStatus.INTERNAL_ERROR, HttpStatus.INTERNAL_SERVER_ERROR));
        }
        return response;
    }

    /**
     * This method is used to activate individual user.
     *
     * @param request
     * @param device
     * @return
     */
    public ResponseDto activate(RegistrationDto  request,String device){
        ResponseDto response = null;
        Activation activation = null;
        User user = this.findByEmail(request.getUser().getEmail()).getPerson();
        try {
            if (!device.equalsIgnoreCase("mobile")) {
                activation = new Activation(user, ActivityStatus.PENDING);
                activation = activationRepositoryService.create(activation);
                CommunicationDto communication = CommunicationBuilder.buildEmail(user, activation.getAuthToken(), EmailType.ACTIVATION);
                response = communicationProcessor.communicate(communication);
            } else {
                userRepositoryService.activate(user.getId());
            }
            if(activation!=null) {
                response = RegistrationResponseBuilder.build(request,ResponseStatus.SUCCESS);
            }
        }catch (PersonNotFoundException e){
            e.printStackTrace();
            response =  RegistrationResponseBuilder.build(request,ResponseStatus.PERSON_NOT_FOUND);
        }catch (Exception e){
            e.printStackTrace();
            response =  RegistrationResponseBuilder.build(request,ResponseStatus.INTERNAL_ERROR);
        }
        return response;
    }

    /**
     * This method is used to get formatted name.
     *
     * @param word
     * @return
     */
    private String getFormattedName(String word) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, word.toLowerCase());
    }

    /**
     * This method is used to find user by id.
     *
     * @param id
     * @return
     */
    public UserResponseDto findById(Integer id){
      User user = userRepositoryService.findById(id);
      if(user==null){
          return new UserResponseDto("No User Found",null);
      }
      return new UserResponseDto("User found", user);
    }

    public ResponseDto findTeamMemberDetailsById(Integer id){
        User user = userRepositoryService.findTeamMemberDetailsById(id);
        if (user == null){
            return UserResponseBuilder.transform(
                    null, ResponseStatus.EMAIL_NOT_REGISTERED.getStatus(), ResponseStatus.EMAIL_NOT_REGISTERED, HttpStatus.OK
            );
        }
        return UserResponseBuilder.transform(user,"success",ResponseStatus.SUCCESS,HttpStatus.OK);
    }
}
