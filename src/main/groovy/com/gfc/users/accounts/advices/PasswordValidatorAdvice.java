package com.gfc.users.accounts.advices;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.models.dto.request.RegistrationDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.google.common.base.CaseFormat;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Created by abhinav on 30/3/16.
 *
 * This class is used to validate password.
 */
@Component
@Aspect
@Order(value = 1)
public class PasswordValidatorAdvice{

    /**
     * This method is used to validate password. It used Around advice on register method.
     *
     * @param proceedingJoinPoint
     * @return
     * @throws Throwable
     */
    @Around("execution(* com.gfc.users.accounts.services.RegistrationService.register(..))")
    public ResponseDto validate(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{

        RegistrationDto request = (RegistrationDto) proceedingJoinPoint.getArgs()[0];
        ResponseDto response = null;
        if (!(request.getUser().getPassword().equals(request.getUser().getRepeatPassword()))) {
            String name = getFormattedName(request);
            response = new ResponseDto(name + " Registration Failed",
                    ResponseStatus.PASSWORD_MISMATCH, null,null, HttpStatus.BAD_REQUEST,null);
            return response;
        }
        return (ResponseDto)proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());
    }


    /**
     * This method is used to get formatted name.
     *
     * @param request
     * @return
     */
    private String getFormattedName(RegistrationDto request) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, request.getUser().getType().getType().toLowerCase());
    }
}
