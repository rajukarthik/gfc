package com.gfc.users.accounts.advices;

import com.gfc.users.accounts.models.dto.response.ResponseDto;

/**
 * Created by abhinav on 30/3/16.
 *
 * This interface is used for validation.
 */
public interface Validator {
    public ResponseDto validate();
}
