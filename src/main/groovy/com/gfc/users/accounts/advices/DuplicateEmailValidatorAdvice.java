package com.gfc.users.accounts.advices;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.models.dto.request.RegistrationDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.repositories.services.UserRepositoryService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Created by abhinav on 1/4/16.
 *
 * This class is used as advice to find the duplicate email.
 */
@Component
@Aspect
@Order(value = 2)
public class DuplicateEmailValidatorAdvice {

    @Autowired
    private UserRepositoryService userRepositoryService;

    /**
     * This method is used to validate duplicate email.
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("execution(* com.gfc.users.accounts.services.RegistrationService.register(..))")
    public ResponseDto validateDuplicateEmail(ProceedingJoinPoint joinPoint) throws Throwable{
        RegistrationDto request = (RegistrationDto) joinPoint.getArgs()[0];
        if(userRepositoryService.findByEmail(request.getUser().getEmail())!=null){
            return new ResponseDto(request.getUser().getType().getType()+" Registration Failed", ResponseStatus.EMAIL_ALREADY_REGISTERED, null, null, HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
        return (ResponseDto) joinPoint.proceed(joinPoint.getArgs());
    }
}
