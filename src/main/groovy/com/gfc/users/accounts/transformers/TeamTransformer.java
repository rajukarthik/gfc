package com.gfc.users.accounts.transformers;

import com.gfc.users.accounts.models.dto.request.TeamDto;
import com.gfc.users.accounts.models.entities.Team;

import java.util.function.Function;

/**
 * Created by abhinav on 31/3/16.
 *
 * This class is used as transformer for team
 */
public class TeamTransformer {

    private static final Function<TeamDto, Team> TRANSFORM_DTO_TO_ENTITY
            = new Function<TeamDto, Team>() {
        @Override
        public Team apply(TeamDto teamDto) {
            Team team = new Team();
            team.setId(teamDto.getId());
            team.setName(teamDto.getName());
            team.setType(teamDto.getType());
            return team;
        }
    };

    /**
     * This method is used to transform from dto to entity
     *
     * @param teamDto
     * @return
     */
    public static Team transformDtoToEntity(TeamDto teamDto)
    {
        return TRANSFORM_DTO_TO_ENTITY.apply(teamDto);
    }

}
