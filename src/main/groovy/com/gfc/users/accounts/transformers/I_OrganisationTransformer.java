package com.gfc.users.accounts.transformers;

import com.gfc.users.accounts.models.dto.request.OrganisationDto;
import com.gfc.users.accounts.models.entities.Organisation;
import com.gfc.users.accounts.models.entities.User;

/**
 * Created by abhinav on 31/3/16.
 *
 * This is a functional interface defined for organisation.
 */
@FunctionalInterface
public interface I_OrganisationTransformer {
    public Organisation transform(OrganisationDto organisationDto, User user);
}
