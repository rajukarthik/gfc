package com.gfc.users.accounts.transformers;

import com.gfc.users.accounts.enums.PersonType;
import com.gfc.users.accounts.models.dto.request.OrganisationDto;
import com.gfc.users.accounts.models.dto.request.UserDto;
import com.gfc.users.accounts.models.entities.Organisation;
import com.gfc.users.accounts.models.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by abhinav on 31/3/16.
 *
 * This class is used as transformer for organisation.
 */
@Getter
@Setter
@AllArgsConstructor
public class OrganisationTransformer {


    @FunctionalInterface
    private interface I_OrganisationModifier{
        Organisation deepCopy(Organisation toBeModified, Organisation changed, User owner);
    }

    private static I_OrganisationTransformer TRANSFORM_DTO_TO_ENTITY =
            (OrganisationDto organisationDto, User user) -> {
                Organisation organisation = new Organisation();
                organisation.setId(organisationDto.getId());
                organisation.setName(organisationDto.getName());
                organisation.setOwner(user);
                return organisation;
            };

    private static I_OrganisationModifier MODIFY =
            (Organisation current, Organisation changed, User owner) -> {
                Organisation organisation = new Organisation();
                organisation.setId(current.getId());
                organisation.setName(current.getName());
                organisation.setOwner(owner);
                return organisation;
            };

    /**
     * This method is used to transform dto to entity.
     *
     * @param organisationDto
     * @param ownerDto
     * @return
     */
    public static final Organisation transform(OrganisationDto organisationDto, UserDto ownerDto){
        User user = UserTransformer.transformDtoToEntity(ownerDto);
        user.setType(PersonType.ORGANISATION_OWNER);
        return TRANSFORM_DTO_TO_ENTITY.transform(organisationDto,user);
    }

    /**
     * This method is used to transform entity to dto.
     *
     * @param current
     * @param changed
     * @param updatedOwner
     * @return
     */
    public static final Organisation modify(Organisation current, Organisation changed, User updatedOwner){
        return MODIFY.deepCopy(current,changed,updatedOwner);
    }
}
