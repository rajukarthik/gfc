package com.gfc.users.accounts.transformers;

import com.gfc.users.accounts.models.entities.Role;
import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.models.entities.UserTeamRole;

/**
 * Created by abhinav on 31/3/16.
 *
 * This is a functional interface defined for team
 */
@FunctionalInterface
public interface I_UserTeamRoleTransformer {
    public UserTeamRole transform(User user, Team team , Role role);
}
