package com.gfc.users.accounts.transformers;

import com.gfc.users.accounts.models.entities.Role;
import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.models.entities.UserTeamRole;

/**
 * Created by abhinav on 31/3/16.
 *
 * This class is used as transformer for team role
 */
public class UserTeamRoleTransformer {
    private static I_UserTeamRoleTransformer CREATE_USER_TEAM_ROLE_MAPPER
            = (User user, Team team, Role role) -> {
        UserTeamRole userTeamRole = new UserTeamRole();
        userTeamRole.setRoleId(role.getId());
        userTeamRole.setTeam(team);
        userTeamRole.setTeamId(team.getId());
        userTeamRole.setUser(user);
        userTeamRole.setUserId(user.getId());
        return userTeamRole;
    };

    /**
     * This method transform team role.
     *
     * @param user
     * @param team
     * @param role
     * @return
     */
    public static final UserTeamRole transform(User user, Team team, Role role){
        return CREATE_USER_TEAM_ROLE_MAPPER.transform(user,team,role);
    }
}
