package com.gfc.users.accounts.transformers;

import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.dto.request.UserDto;
import com.gfc.users.accounts.models.entities.User;

import java.util.function.Function;

/**
 * Created by abhinav on 31/3/16.
 *
 * This class is used to transform user.
 */
public class UserTransformer{

    private static final Function<UserDto, User> TRANSFORM_DTO_TO_ENTITY
            = new Function<UserDto, User>() {
        @Override
        public User apply(UserDto userDto) {
            User user = new User();
            user.setId(userDto.getId());
            user.setEmail(userDto.getEmail());
            user.setFirstName(userDto.getFirstName());
            user.setLastName(userDto.getLastName());
            user.setPassword(userDto.getPassword());
            user.setStatus(ActivityStatus.PENDING);
            user.setSubscriptionType(userDto.getType());
            //user.setIs_Deleted(false);
            return user;
        }
    };

    private static final Function<User, UserDto> TRANSFORM_ENTITY_TO_DTO
            = new Function<User, UserDto>() {
        @Override
        public UserDto apply(User user) {
            UserDto userDto = new UserDto();
            userDto.setId(user.getId());
            userDto.setEmail(user.getEmail());
            userDto.setFirstName(user.getFirstName());
            userDto.setLastName(user.getLastName());
            userDto.setPassword(user.getPassword());
            userDto.setType(user.getSubscriptionType());
            return userDto;
        }
    };

    @FunctionalInterface
    private interface I_TransformChangedObject{
        User deepCopy(User toBeModified, User changed);
    }

    private static I_TransformChangedObject MODIFY
            = (User current, User changed) -> {
      current.setEmail(changed.getEmail());
      current.setFirstName(changed.getEmail());
      current.setLastName(changed.getLastName());
      current.setType(changed.getType());
      return current;
    };

    /**
     * This method is used to transform dto to entity.
     *
     * @param userDto
     * @return
     */
    public static User transformDtoToEntity(UserDto userDto){
        return TRANSFORM_DTO_TO_ENTITY.apply(userDto);
    }

    /**
     * This method is used to transform entity to dto.
     *
     * @param user
     * @return
     */
    public static UserDto transformEntityToDto(User user){
        return TRANSFORM_ENTITY_TO_DTO.apply(user);
    }

    /**
     * This method is used to transform dto to entity.
     *
     * @param current
     * @param changed
     * @return
     */
    public static User transformDtoToEntity(User current, User changed){
        return MODIFY.deepCopy(current,changed);
    }
}
