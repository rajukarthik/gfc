package com.gfc.users.accounts.builders;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.models.entities.User;
import org.springframework.http.HttpStatus;

/**
 * Created by abhinav on 3/4/16.
 *
 * This class is used to build User response object.
 */
public class UserResponseBuilder {

    @FunctionalInterface
    private static interface I_ResponseBuilder{
        public ResponseDto apply(User user, String message, ResponseStatus status, HttpStatus httpStatus);
    }
    private static I_ResponseBuilder RESPONSE = (user, message, status, httpStatus) -> {
        ResponseDto response = new ResponseDto();
        response.setPerson(user);
        response.setStatus(status);
        response.setHttpStatus(httpStatus);
        response.setMessage(message);
        return response;
    };

    /**
     * This method is used to build User response object useing transformers.
     *
     * @param user
     * @param message
     * @param status
     * @param httpStatus
     * @return
     */
    public static ResponseDto transform(User user,String message,ResponseStatus status, HttpStatus httpStatus){
        return RESPONSE.apply(user,message,status,httpStatus);
    }
}
