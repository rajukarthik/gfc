package com.gfc.users.accounts.builders;

import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.models.dto.request.RegistrationDto;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.accounts.transformers.UserTransformer;
import org.springframework.http.HttpStatus;

import java.util.function.Function;

/**
 * Created by abhinav on 2/4/16.
 *
 * This class is used to build Registration response object.
 */
public class RegistrationResponseBuilder {

    private static final Function<RegistrationDto, ResponseDto> SUCCESS
            = new Function<RegistrationDto, ResponseDto>(){

        @Override
        public ResponseDto apply(RegistrationDto request){
           ResponseDto response = new ResponseDto();
           response.setPerson(UserTransformer.transformDtoToEntity(request.getUser()));
           response.setMessage(request.getUser().getType()+" REGISTERED SUCCESSFULLY");
           response.setStatus(ResponseStatus.SUCCESS);
           response.setHttpStatus(HttpStatus.OK);
           return response;
        }
    };

    private static final Function<RegistrationDto, ResponseDto> EMAIL_ALREADY_REGISTERED
            = new Function<RegistrationDto, ResponseDto>() {
        @Override
        public ResponseDto apply(RegistrationDto request) {
           ResponseDto response = new ResponseDto();
           response.setPerson(UserTransformer.transformDtoToEntity(request.getUser()));
           response.setMessage(request.getUser().getType().getType() + "REGISTRATION FAILED");
           response.setStatus(ResponseStatus.EMAIL_ALREADY_REGISTERED);
           response.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
           return response;
        }
    };

    private static final Function<RegistrationDto, ResponseDto> PASSWORD_MISMATCH
            = new Function<RegistrationDto, ResponseDto>() {
        @Override
        public ResponseDto apply(RegistrationDto request) {
            ResponseDto response = new ResponseDto();
            response.setPerson(UserTransformer.transformDtoToEntity(request.getUser()));
            response.setMessage(request.getUser().getType().getType() + "REGISTRATION FAILED");
            response.setStatus(ResponseStatus.PASSWORD_MISMATCH);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            return response;
        }
    };

    private static final Function<RegistrationDto, ResponseDto> PERSON_NOT_FOUND
            = new Function<RegistrationDto, ResponseDto>() {
        @Override
        public ResponseDto apply(RegistrationDto request) {
            ResponseDto response = new ResponseDto();
            response.setPerson(UserTransformer.transformDtoToEntity(request.getUser()));
            response.setMessage(request.getUser().getType().getType() + "REGISTRATION FAILED");
            response.setStatus(ResponseStatus.PASSWORD_MISMATCH);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            return response;
        }
    };

    private static final Function<RegistrationDto, ResponseDto> INTERNAL_ERROR
            = new Function<RegistrationDto, ResponseDto>() {
        @Override
        public ResponseDto apply(RegistrationDto request) {
            ResponseDto response = new ResponseDto();
            response.setPerson(UserTransformer.transformDtoToEntity(request.getUser()));
            response.setMessage(request.getUser().getType().getType() + "REGISTRATION FAILED");
            response.setStatus(ResponseStatus.INTERNAL_ERROR);
            response.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            return response;
        }
    };

    private static final Function<RegistrationDto, ResponseDto> INVALID_REQUEST
            = new Function<RegistrationDto, ResponseDto>() {
        @Override
        public ResponseDto apply(RegistrationDto request) {
            ResponseDto response = new ResponseDto();
            response.setPerson(UserTransformer.transformDtoToEntity(request.getUser()));
            response.setMessage(request.getUser().getType().getType() + "REGISTRATION FAILED");
            response.setStatus(ResponseStatus.INVALID_REQUEST);
            response.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            return response;
        }
    };

    /**
     * This method is used to build Registration response object based on status.
     *
     * @param request
     * @param status
     * @return
     */
    public static ResponseDto build(RegistrationDto request, ResponseStatus status){
        switch (status){
            case SUCCESS:
                return SUCCESS.apply(request);
            case EMAIL_ALREADY_REGISTERED:
                return EMAIL_ALREADY_REGISTERED.apply(request);
            case PASSWORD_MISMATCH:
                return PASSWORD_MISMATCH.apply(request);
            case PERSON_NOT_FOUND:
                return PERSON_NOT_FOUND.apply(request);
            case INTERNAL_ERROR:
                return INTERNAL_ERROR.apply(request);
            case INVALID_REQUEST:
                return INVALID_REQUEST.apply(request);
        }
        return null;
    }
}
