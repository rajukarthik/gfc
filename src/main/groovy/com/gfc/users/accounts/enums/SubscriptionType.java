package com.gfc.users.accounts.enums;

/**
 * Created by abhinav on 20/12/15.
 *
 * Enum class to handle Subscription types
 */
public enum SubscriptionType {
    INDIVIDUAL("INDIVIDUAL"),
    TEAM("TEAM"),
    ORGANISATION("ORGANISATION");

    private final String type;
    SubscriptionType(String type){
        this.type = type;
    }

    public String getType(){
        return this.type;
    }
}
