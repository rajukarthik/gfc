package com.gfc.users.accounts.enums;

/**
 * Created by abhinav on 20/12/15.
 *
 * Enum class to handle Person roles
 */
public enum PersonRole {
    INDIVIDUAL("INDIVIDUAL"),
    TEAM_ADMIN("TEAM_ADMIN"),
    ORGANISATION_ADMIN("ORGANISATION_ADMIN"),
    TEAM_MEMBER("TEAM_MEMBER"),
    SUPER_ADMIN("SUPER_ADMIN");

    private final String role;

    PersonRole(String role) {
        this.role = role;
    }

    public String getRole(){
        return this.role;
    }
}
