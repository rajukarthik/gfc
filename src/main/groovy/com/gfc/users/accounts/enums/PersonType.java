package com.gfc.users.accounts.enums;

/**
 * Created by abhinav on 21/12/15.
 *
 * Enum class to handle person types
 */
public enum PersonType {
    INDIVIDUAL("individual"),
    TEAM_MEMBER("team member"),
    TEAM_ADMIN("team admin"),
    ORGANISATION_OWNER("organisation owner");

    private final String type;

    PersonType(String type) {
        this.type = type;
    }

    public String getType(){
        return this.type;
    }
}