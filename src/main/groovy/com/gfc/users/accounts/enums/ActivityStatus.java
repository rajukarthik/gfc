package com.gfc.users.accounts.enums;

/**
 * Created by abhinav on 21/12/15.
 */
public enum ActivityStatus {
    ACTIVE("ACTIVE"),
    PENDING("PENDING"),
    INACTIVE("INACTIVE");

    private final String status;

    ActivityStatus(String status) {
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }
}
