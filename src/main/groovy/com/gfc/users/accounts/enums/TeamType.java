package com.gfc.users.accounts.enums;

/**
 * Created by abhinav on 21/12/15.
 *
 * Enum class to handle team types
 */
public enum TeamType {
    PRIVATE("private"),
    INDEPENDENT("independent");

    private final String type;

    TeamType(String type) {
        this.type = type;
    }

    public String getType(){
        return this.type;
    }
}
