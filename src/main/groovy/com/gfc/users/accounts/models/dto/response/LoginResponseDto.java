package com.gfc.users.accounts.models.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.enums.ResponseStatus;
import com.gfc.users.accounts.enums.SubscriptionType;
import com.gfc.users.accounts.models.entities.Organisation;
import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import lombok.*;

import java.util.List;

/**
 * Created by abhinav on 3/1/16.
 *
 * This class is used as response dto for login request.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ToString
public class LoginResponseDto {

    private ResponseStatus status;
    private User people;
    private SubscriptionType subscriptionType;
    private Organisation organisation;
    private List<Team> team;
    @JsonIgnore
    private String authToken;
}
