package com.gfc.users.accounts.models.dto.request;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gfc.users.accounts.enums.SubscriptionType;
import com.gfc.users.accounts.models.entities.Organisation;
import com.gfc.users.accounts.models.entities.Team;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by abhinav on 12/3/16.
 *
 * This class is used as dto for account details
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Setter
@Getter
@ToString
public class AccountDetailsDto {

    @JsonCreator
    public AccountDetailsDto(
            @JsonProperty("changeSet") AccountDetailsDto changedDetails,
            @JsonProperty("id") Integer userId,
            @JsonProperty("type") SubscriptionType type,
            @JsonProperty("team") Team team,
            @JsonProperty("organisation") Organisation organisation
    ){
        this.changedDetails = changedDetails;
        this.userId = userId;
        this.team = team;
        this.organisation = organisation;
        this.type = type;
    }

    @JsonBackReference("changeSet")
    private AccountDetailsDto changedDetails;
    private String firstName;
    private String lastName;
    @JsonProperty("id")
    private Integer userId;
    private SubscriptionType type;
    private Team team;
    private Organisation organisation;
}
