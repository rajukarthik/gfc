package com.gfc.users.accounts.models.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.users.accounts.models.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by abhinav on 25/12/15.
 *
 * This class is used as dto for user detail request.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserResponseDto {
    private String message;
    private User people;
}
