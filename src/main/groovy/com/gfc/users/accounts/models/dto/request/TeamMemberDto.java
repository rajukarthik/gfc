package com.gfc.users.accounts.models.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.gfc.users.accounts.enums.PersonType;
import com.gfc.users.accounts.enums.SubscriptionType;
import com.gfc.validators.Email;
import com.gfc.validators.Name;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Arun Kumar on 09-08-2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TeamMemberDto {
    @JsonProperty
    private Integer id;

    @Name(message = "error.name")
    @JsonProperty("firstName")
    private String firstName;

    @Name(message = "error.name")
    @JsonProperty("lastName")
    private String lastName;

    @Email(message = "error.email")
    @JsonProperty("email")
    private String email;

    @JsonProperty("teamId")
    private String teamId;

    @JsonProperty("teamName")
    private String teamName;

}
