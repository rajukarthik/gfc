package com.gfc.users.accounts.models.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.models.domain.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

/**
 * Created by abhinav on 19/12/15.
 *
 * This class is used as database entity for organisations
 */
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "gfc_organisations")
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class Organisation extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "gfc_organisations_teams",
            joinColumns = {@JoinColumn(name = "organisation_id" , referencedColumnName = "id")},
            inverseJoinColumns={@JoinColumn(name = "team_id" , referencedColumnName = "id", unique = true)}
    )
    private List<Team> teams;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "people_id"  ,referencedColumnName = "id")
    private User owner;

    public Organisation(String name, List<Team> teams, User owner) {
        this.name = name;
        this.teams = teams;
        this.owner = owner;
    }
}
