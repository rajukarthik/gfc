package com.gfc.users.accounts.models.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by abhinav on 21/12/15.
 *
 * This class is used as database entity for user team role ids
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class UserTeamRoleId implements Serializable {
    private Integer userId;
    private Integer teamId;
}
