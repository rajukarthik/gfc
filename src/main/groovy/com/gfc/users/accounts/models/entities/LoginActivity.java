
package com.gfc.users.accounts.models.entities;

import com.gfc.models.domain.BaseEntity;
import com.gfc.users.accounts.enums.ActivityStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by abhinav on 3/1/16.
 *
 * This class is used as database entity for login activity
 */
@Entity
@Table(name = "gfc_login_activities")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LoginActivity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "auth_token")
    private String authToken;

    @Enumerated(EnumType.STRING)
    private ActivityStatus status;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "people_id", referencedColumnName = "id")
    private User people;
}
