package com.gfc.users.accounts.models.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by abhinav on 13/3/16.
 *
 * This class used as dto object for password change
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Setter
@Getter
@ToString
public class PasswordRequestDto {
    @JsonProperty("id")
    private Integer userId;
    private String currentPassword;
    private String  newPassword;
    private String  repeatPassword;
}
