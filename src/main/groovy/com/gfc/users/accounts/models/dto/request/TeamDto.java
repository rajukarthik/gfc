package com.gfc.users.accounts.models.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.users.accounts.enums.TeamType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by abhinav on 23/12/15.
 *
 * This class is used as dto for team registration
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TeamDto {

//    @Size(min = 2, max = 50, message = "error.registration.password.size")
//    @Name
    private Integer id;
    private String name;
    private TeamType type;
}
