package com.gfc.users.accounts.models.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.models.domain.BaseEntity;
import com.gfc.users.accounts.enums.TeamType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhinav on 19/12/15.
 *
 * This class is used as database entity for teams
 */
@Entity
@Table(name = "gfc_teams")
@NoArgsConstructor
@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Team extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TeamType type;

    @OneToMany(mappedBy = "team")
    private List<UserTeamRole> peopleTeamsRoles = new ArrayList();

    public Team(String name, TeamType type, List<UserTeamRole> usersTeamsRoles) {
        this.name = name;
        this.type = type;
        this.peopleTeamsRoles = usersTeamsRoles;

    }

//    public UserTeamRole addTeamUserRoleAssociation(Role role, User user) {
//        System.out.println("*************************");
//        System.out.println(user.getId()+"  "+this.getId());
//        System.out.println("*************************");
//        UserTeamRole userTeamRole = new UserTeamRole();
//        userTeamRole.setRoleId(role.getId());
//        userTeamRole.setTeamId(getId());
//        userTeamRole.setPersonId(user.getId());
//        userTeamRole.setPeople(user);
//        userTeamRole.setTeam(this);
//        this.peopleTeamsRoles = this.peopleTeamsRoles == null ? Lists.newArrayList(userTeamRole): this.peopleTeamsRoles;
//        this.peopleTeamsRoles.add(userTeamRole);
//        if(user.getPeopleTeamsRoles() == null){
//            user.setPeopleTeamsRoles(Lists.newArrayList());
//        }
//        user.getPeopleTeamsRoles().add(userTeamRole);
//        return userTeamRole;
//    }
}