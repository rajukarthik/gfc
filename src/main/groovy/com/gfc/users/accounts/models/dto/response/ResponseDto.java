package com.gfc.users.accounts.models.dto.response;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.enums.ResponseStatus;
import com.gfc.enums.SearchStatus;
import com.gfc.models.dto.MessageDto;
import com.gfc.users.accounts.models.entities.User;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * Created by abhinav on 19/12/15.
 *
 * This class is a generic response dto.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class ResponseDto {

    private String message;
    private ResponseStatus status;
    private List<MessageDto> errors;
    private SearchStatus searchStatus;

    @JsonIgnore
    private HttpStatus httpStatus;
    private User person;

    public ResponseDto(
            String message,
            ResponseStatus responseStatus,
            List<MessageDto> errors,
            SearchStatus searchStatus,
            User person
    ){
        this.message = message;
        this.errors = errors;
        this.searchStatus = searchStatus;
        this.status = responseStatus;
        this.person = person;
    }
}
