package com.gfc.users.accounts.models.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.validators.Email;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by abhinav on 3/1/16.
 *
 * This class is used as dto for login request
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LoginRequestDto {

    @Email(message = "error.email")
    private String email;

    @NotNull(message = "error.registration.password.notNull")
    @Size(min = 1, max = 50, message = "error.registration.password.size")
    private String password;

    private ActivityStatus status = ActivityStatus.ACTIVE;

}
