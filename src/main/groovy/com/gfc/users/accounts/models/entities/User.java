package com.gfc.users.accounts.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.enums.PersonType;
import com.gfc.users.accounts.enums.SubscriptionType;
import com.gfc.utils.EncryptUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by abhinav on 19/12/15.
 *
 * This class is used as database entity for user details
 */
@Entity
@Table(name = "gfc_people")
@NoArgsConstructor
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ToString
public class User{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "email", unique = true)
	private String email;

	@JsonIgnore
	@Column(name = "password")
	private String password;


	@Column(name = "is_deleted")
	private boolean is_deleted;

	@JsonIgnore
	@OneToMany(mappedBy = "user")
	private List<UserTeamRole> peopleTeamsRoles = new ArrayList();

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private PersonType type;

	@Column(name = "subscription_type")
	@Enumerated(EnumType.STRING)
	private SubscriptionType subscriptionType;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private ActivityStatus status;

	@Embedded
	@JsonUnwrapped
	private TimeEntity time;

	public User(String firstName, String lastName, String email, String password,
				List<UserTeamRole> peopleTeamsRoles, PersonType type, SubscriptionType subscriptionType,ActivityStatus status,boolean is_deleted) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.peopleTeamsRoles = peopleTeamsRoles;
		this.type = type;
		this.status = status;
		this.is_deleted = is_deleted;
		this.subscriptionType = subscriptionType;
	}

	public User(Integer id , String firstName, String lastName, String email, String password) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		//this.is_deleted=is_deleted;
	}


	@PrePersist
	public void preCreate(){
		Date date = new Date();
		this.time = new TimeEntity();
		this.getTime().setCreatedDt(date);
		this.getTime().setUpdatedDt(date);
		this.getTime().setUpdatedBy("master");
		this.getTime().setCreatedBy("master");
		this.setPassword(EncryptUtils.encryptMD5(this.getPassword()));
	}

	@PreUpdate
	public void preUpdate(){
		Date date = new Date();
		this.getTime().setUpdatedDt(date);
		this.getTime().setUpdatedBy("master");
	}
}
