package com.gfc.users.accounts.models.entities;

import com.gfc.models.domain.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

/**
 * Created by abhinav on 20/12/15.
 *
 * This class is used as database entity for user team roles
 */

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "gfc_people_teams_roles")
@IdClass(UserTeamRoleId.class)
@ToString
public class UserTeamRole extends BaseEntity {

    @Id
    @Column(name = "people_id")
    private Integer userId;

    @Id
    @Column(name = "team_id")
    private Integer teamId;

    @Column(name = "role_id")
    private Integer roleId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "people_id" , referencedColumnName = "id",insertable=false, updatable=false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "team_id" , referencedColumnName = "id",insertable=false, updatable=false)
    private Team team;


//    @ManyToOne
//    @JoinColumn(name = "role_id" , referencedColumnName = "id",insertable=false, updatable=false)
//    private Role role;
}
