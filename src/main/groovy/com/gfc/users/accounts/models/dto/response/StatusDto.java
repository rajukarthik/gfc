package com.gfc.users.accounts.models.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.enums.ResponseStatus;
import lombok.*;
import org.springframework.http.HttpStatus;

/**
 * Created by abhinav on 9/3/16.
 *
 * This class is used as status dto.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class StatusDto {

    private String message;
    private ResponseStatus status;

    @JsonIgnore
    private HttpStatus httpStatus;
}
