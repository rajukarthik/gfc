package com.gfc.users.accounts.models.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.gfc.users.accounts.models.entities.Organisation;
import com.gfc.users.accounts.models.entities.Team;
import com.gfc.users.accounts.models.entities.User;
import lombok.*;

/**
 * Created by abhinav on 9/3/16.
 *
 * This class is used as dto for profile details.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class ProfileResponseDto {

    @JsonUnwrapped
    private StatusDto status;

    private Team team;

    private Organisation organisation;

    @JsonUnwrapped
    private User user;
}
