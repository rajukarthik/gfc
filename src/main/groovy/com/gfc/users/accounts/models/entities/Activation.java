package com.gfc.users.accounts.models.entities;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.utils.TokenGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by gurinder on 22/12/15.
 *
 * This class is used as database entity for activation.
 */
@Entity
@Table(name = "gfc_people_activation")
@Getter
@Setter
@NoArgsConstructor
public class Activation{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "people_id"  ,referencedColumnName = "id")
    private User people;

    @Column(name = "auth_token")
    private String authToken;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;

    @Embedded
    @JsonUnwrapped
    private TimeEntity time;

    public Activation(User people, String authToken, ActivityStatus status) {
        this.people = people;
        this.authToken = authToken;
        this.status = status;
    }

    public Activation(User people, ActivityStatus status) {
        this.people = people;
        this.status = status;
    }

    @PrePersist
    public void preCreate(){
        this.authToken = TokenGenerator.getToken();
    }


}
