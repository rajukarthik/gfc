package com.gfc.users.accounts.models.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;

/**
 * Created by abhinav on 20/12/15.
 *
 * This class is used as dto for registration
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegistrationDto {
    @Valid
    @JsonUnwrapped
    private UserDto user;

    @Valid
    @JsonProperty("team")
    private TeamDto team;

    @JsonProperty("organisation")
    private OrganisationDto organisation;

    @JsonDeserialize
    public void setUser(UserDto user){
        this.user = user;
    }
}
