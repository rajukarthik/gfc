package com.gfc.users.accounts.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by abhinav on 5/3/16.
 *
 * This class is used as database entity for time
 */
@Embeddable
@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@Getter
public class TimeEntity {
    @Column(name = "created_dt")
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date createdDt;

    @Column(name = "updated_dt")
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date updatedDt;

    @Column(name = "created_by")
    @JsonIgnore
    private String createdBy;

    @Column(name = "updated_by")
    @JsonIgnore
    private String updatedBy;
}
