package com.gfc.users.accounts.models.entities;

import com.gfc.models.domain.BaseEntity;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.enums.PersonRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by abhinav on 20/12/15.
 *
 * This class is used as database entity for roles
 */
@Entity
@Table(name = "gfc_roles")
@Getter
@Setter
@NoArgsConstructor
public class Role extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "code")
    @Enumerated(EnumType.STRING)
    private PersonRole code;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;

    public Role(PersonRole code, String description, ActivityStatus status) {
        this.code = code;
        this.description = description;
        this.status = status;
    }
}
