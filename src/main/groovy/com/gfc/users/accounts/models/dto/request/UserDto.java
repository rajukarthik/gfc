package com.gfc.users.accounts.models.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gfc.users.accounts.enums.SubscriptionType;
import com.gfc.validators.Email;
import com.gfc.validators.Name;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by abhinav on 30/3/16.
 *
 * This class is used as dto for User details.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserDto {

    @JsonProperty
    private Integer id;

    @Name(message = "error.name")
    @JsonProperty("firstName")
    private String firstName;

    @Name(message = "error.name")
    @JsonProperty("lastName")
    private String lastName;

    @Email(message = "error.email")
    @JsonProperty("email")
    private String email;

    @NotNull(message = "error.registration.password.notNull")
    @Size(min = 2, max = 50, message = "error.registration.password.size")
    @JsonProperty("password")
    private String password;

    @NotNull(message = "error.registration.password.notNull")
    @Size(min = 2, max = 50, message = "error.registration.password.size")
    @JsonProperty("repeatPassword")
    private String repeatPassword;

    @JsonProperty("type")
    private SubscriptionType type;
}
