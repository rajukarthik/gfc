package com.gfc.users.activities.repositories;

import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.activities.models.entities.Clarification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by gurinder on 13/2/16.
 */
@Repository
public interface ClarificationRepository extends JpaRepository<Clarification, Long> {

    public Clarification findBySectionIdAndPeopleAndGuestEmail(String sequenceId, User people, String guestEmail);

    public Clarification findByGuestTokenAndStatus(String guestToken, ActivityStatus status);

    public Clarification findById(long clarificationId);

    public List<Clarification> findByPeople(User people);
    
    public List<Clarification> findByPeopleOrderByUpdatedDtDesc(User people);


}