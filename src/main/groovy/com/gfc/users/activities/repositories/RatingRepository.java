package com.gfc.users.activities.repositories;

import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.activities.models.entities.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by abhinav on 5/3/16.
 */
@Repository
public interface RatingRepository extends JpaRepository<Rating, Integer> {

	Rating findByUserAndQuestionAndSectionId(User user, String question, String sectionId);
}
