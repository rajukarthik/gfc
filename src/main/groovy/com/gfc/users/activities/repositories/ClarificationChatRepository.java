package com.gfc.users.activities.repositories;

import com.gfc.users.activities.models.entities.Clarification;
import com.gfc.users.activities.models.entities.ClarificationChatHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by gurinder on 14/2/16.
 */
public interface ClarificationChatRepository extends JpaRepository<ClarificationChatHistory, Integer> {
    List<ClarificationChatHistory> findByClarificationIdOrderByCreatedDtAsc(Clarification clarificationId);
}
