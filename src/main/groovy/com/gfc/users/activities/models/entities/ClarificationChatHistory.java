package com.gfc.users.activities.models.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.models.domain.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Shantanu Singh on 12-Feb-16.
 */
@Entity
@Table(name = "gfc_clarification_chat_history")
@NoArgsConstructor
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ClarificationChatHistory extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 4096)
    private String comment;

    @Column(name = "user_name")
    private String userName;

    @ManyToOne
    @JoinColumn(name = "clarification_id", referencedColumnName = "id")
    private Clarification clarificationId;
}
