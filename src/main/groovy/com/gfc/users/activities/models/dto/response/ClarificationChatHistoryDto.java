package com.gfc.users.activities.models.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.models.dto.ClarificationChatDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Shantanu Singh on 18-Mar-16.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ClarificationChatHistoryDto {

    private String sectionId;
    private String sectionTitle;
    private Long clarificationId;
    private String userName;
    private String guestUserName;
    private List<ClarificationChatDto> clarificationChats;

}
