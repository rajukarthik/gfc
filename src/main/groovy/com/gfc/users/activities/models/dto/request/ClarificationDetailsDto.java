package com.gfc.users.activities.models.dto.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by gurinder on 13/2/16.
 */
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class ClarificationDetailsDto {
    @JsonProperty("recipientEmail")
    private List<String> recipientEmails;

    @JsonProperty("comment")
    private String comment;

    @JsonProperty("sectionId")
    private String sectionId;

    @JsonProperty("sectionTitle")
    private String sectionTitle;

    @JsonProperty("clarificationId")
    private Long clarificationId;

    @JsonProperty("commentedBy")
    private String commentedBy;
}
