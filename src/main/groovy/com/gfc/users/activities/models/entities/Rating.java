package com.gfc.users.activities.models.entities;

import com.gfc.users.accounts.models.entities.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by abhinav on 3/3/16.
 */
@Entity
@Table(name = "gfc_ratings")
@NoArgsConstructor
@Getter
@Setter
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "value")
    private Integer value;

    @Column(name = "question")
    private String question;

    @Column(name = "section_id")
    private String sectionId;
}
