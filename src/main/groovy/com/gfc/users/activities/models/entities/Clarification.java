package com.gfc.users.activities.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfc.models.domain.BaseEntity;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Shantanu Singh on 12-Feb-16.
 */
@Entity
@Table(name = "gfc_clarification")
@NoArgsConstructor
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Clarification extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "section_sequence_id")
    private String sectionId;

    @Column(name = "section_sequence_title")
    private String sectionTitle;

    @Column(name = "guest_email")
    private String guestEmail;

    @Column(name = "guest_token")
    private String guestToken;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;

    @Column(name = "question")
    private String question;
    
    @Column(name = "is_read")
    private boolean read;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "people_id", referencedColumnName = "id")
    private User people;


    @JsonIgnore
    @OneToMany(mappedBy = "clarificationId", fetch = FetchType.LAZY)
    List<ClarificationChatHistory> clarificationChatList;
}
