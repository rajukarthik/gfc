package com.gfc.users.activities.services;

import com.gfc.client.RankerClient;
import com.gfc.client.WatsonClient;
import com.gfc.enums.ResponseStatus;
import com.gfc.response.WatsonRankerResponse;
import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.activities.models.dto.request.RatingRequestDto;
import com.gfc.users.activities.models.dto.response.RatingResponseDto;
import com.gfc.users.activities.models.entities.Rating;
import com.gfc.users.activities.repository.services.RatingRepositoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;

/**
 * Created by abhinav on 5/3/16.
 */
@Service
public class RatingService {

	private final String NEW_LINE_SEPARATOR = "\n";
	private final String STRING_SUFFIX = "\"";
	private final String STRING_PREFIX = "\"";
	
    private RatingRepositoryService ratingRepositoryService;

    private RankerClient rankerClient;
    private WatsonClient watsonClient;
    
    @Autowired
    public RatingService(RatingRepositoryService ratingRepositoryService, RankerClient rankerClient, WatsonClient watsonClient) {
        this.ratingRepositoryService = ratingRepositoryService;
        this.rankerClient = rankerClient;
        this.watsonClient = watsonClient;
    }

    /**
     * This method is used to rate an answer of searched question.
     * @param request
     * @return
     */
    public ResponseDto rate(RatingRequestDto request){
        ResponseDto response = new ResponseDto();
        try {
            if (ratingRepositoryService.add(request)) {
                response.setMessage("RATINGS ADDED FOR USER WITH ID " + request.getUser().getId());
                response.setStatus(ResponseStatus.SUCCESS);
                response.setHttpStatus(HttpStatus.OK);
            } else {
                response.setMessage("RATINGS ADDITION FAILED");
                response.setStatus(ResponseStatus.INTERNAL_ERROR);
                response.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }catch (Exception e){
            e.printStackTrace();
            response.setMessage("RATINGS ADDITION FAILED");
            response.setStatus(ResponseStatus.INTERNAL_ERROR);
            response.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        }
      return response;
    }
    
    /**
     * This method is used to fetch relevant answers ratings for the logged in user.
     * @param userId
     * @param question
     * @param sectionId
     * @return
     */
    public RatingResponseDto findRateByUserAndQuestionAndSection(int userId, String question, String sectionId){
    	RatingResponseDto response = new RatingResponseDto();
        try {
        	int value = ratingRepositoryService.findRateByUserAndQuestionAndSection(userId, question, sectionId);
            response.setHttpStatus(HttpStatus.OK);     
            response.setValue(value);
        }catch (Exception e){
            e.printStackTrace();            
            response.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        }
      return response;
    }
    
    public List<Rating> findAll() {
    	return ratingRepositoryService.findAll();
    }
    
    public void ingestDataInWatson(String clusterId, String rankerId) {
    	List<Rating> ratings = ratingRepositoryService.findAll();
    	String fileName = System.getProperty("user.home")+"/ranker_feed.csv";
    	File file = new File(fileName);//transformRatingDataForIngestion(ratings);
    	
    	generateAndFeedRankerData(file, clusterId, rankerId);
  	
    }
    public File transformRatingDataForIngestion(List<Rating> ratings) {
    	
        FileWriter writer = null;
        File file = null;
    	String fileName = System.getProperty("user.home")+"/ranker_feed.csv";
    	Map<String, Map<String, Integer>> questionRatings = new HashMap<String, Map<String, Integer>>();
        try {
            writer = new FileWriter(fileName);
            
            for (Rating rating : ratings) {
            	if(!questionRatings.containsKey(rating.getQuestion())) {
            		Map<String, Integer> answers = new HashMap<String, Integer>();
            		answers.put(rating.getSectionId(), rating.getValue());
            		questionRatings.put(rating.getQuestion(), answers);
            	} else {
            		Map<String, Integer> answers = questionRatings.get(rating.getQuestion());
            		if(answers.containsKey(rating.getSectionId())) {
            			Integer value = (int) ( ((double) answers.get(rating.getSectionId()) + (double) rating.getValue()) / 2);
            			questionRatings.get(rating.getQuestion()).put(rating.getSectionId(), value);
            		} else {
            			questionRatings.get(rating.getQuestion()).put(rating.getSectionId(), rating.getValue());
            		}            		
            	}
            }
            
            for (String key : questionRatings.keySet()) {
            	Map<String, Integer> answers = questionRatings.get(key);
            	if(!answers.isEmpty()) {
            		StringBuilder sb = new StringBuilder();
            		sb.append(STRING_PREFIX + key + STRING_SUFFIX)
            			.append(",");
            	    
            	    for (String sectionKey : answers.keySet()) {
            	    	sb.append(STRING_SUFFIX + sectionKey + STRING_SUFFIX)
            	    		.append(",")
            	    		.append(STRING_SUFFIX + String.valueOf(answers.get(sectionKey)) + STRING_SUFFIX)
            	    		.append(",");            	    	
            	    }            	    
            	    writer.append(sb.substring(0, sb.length() - 1) + NEW_LINE_SEPARATOR);
            	}            	
            }
            System.out.println("CSV file was created successfully !!!");
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
            	if(null != writer) {
            		writer.flush();
            		writer.close();	
            	}        
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter !!!");
                e.printStackTrace();
            }
        }
        
        file = new File(fileName);
        if(file.exists()) {
        	return file;
        }
        return null;
    }
    
    private void generateAndFeedRankerData(File csvRankerFile, String clusterId, String rankerId) {
    	
    	boolean addHeader = true;

    	BufferedReader br = null;
    	String line = "";
    	String cvsSplitBy = ",";

    	String CREDS="572fdbbd-251c-4a7c-b115-8a2ce2692282:fOoK8OrI7DHb";
		String CLUSTER="sc4afeee7a_3358_4410_9843_9ca067837e09";
		String COLLECTION="gfc-syn";
		String RELEVANCE_FILE="";
		String RANKERNAME="gfc-ranker-syn";
		String ROWS="10";
		boolean DEBUG=false;
		String VERBOSE="";
    			
		String BASEURL = "https://gateway.watsonplatform.net/retrieve-and-rank/api/v1/";
		String SOLRURL = BASEURL + String.format("solr_clusters/%s/solr/%s/fcselect", CLUSTER, COLLECTION);
		String RANKERURL = BASEURL + "rankers";
				
    	try {

    		List<WatsonRankerResponse> rankerQuestionInput = new ArrayList<WatsonRankerResponse>();
    		br = new BufferedReader(new FileReader(csvRankerFile));
    		while ((line = br.readLine()) != null) {
    		        // use comma as separator
    			String[] rankerRow = line.split(cvsSplitBy);

    			String question = rankerRow[0];
    			String relevance = StringUtils.join(Arrays.copyOfRange(rankerRow, 1, rankerRow.length), ",");
    			
    			WatsonRankerResponse response = watsonClient.findRelevanceCollection(clusterId, "gfc-syn", question, relevance, addHeader, ROWS, true, "json");

    			rankerQuestionInput.add(response);
    			
    		}
    		
    		System.out.println(rankerQuestionInput);

    		List<String> trainingDataList = new ArrayList<>();
    		
    		for(WatsonRankerResponse response : rankerQuestionInput) {
    			String rsInputString = response.getRSInput();
    			String[] rows =  rsInputString.split("\n");
    			
    			if(trainingDataList.isEmpty()) {
    				trainingDataList.add(rows[0]);
    			}
    			trainingDataList.addAll(Arrays.asList(Arrays.copyOfRange(rows, 1, rows.length)));
    		}

    		String trainingDataString = StringUtils.join(trainingDataList, "\n");
    		
    		rankerClient.ingest(trainingDataString, "gfc-ranker-syn");
    		System.out.println(trainingDataList);
    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (br != null) {
    			try {
    				br.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}    	
    	
    	
    }
}
