package com.gfc.users.activities.services;

import com.gfc.enums.EmailType;
import com.gfc.enums.SearchStatus;
import com.gfc.exceptions.EmailFailureException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.exceptions.RegistrationException;
import com.gfc.services.EmailService;
import com.gfc.users.accounts.models.entities.LoginActivity;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.LoginActivityRepository;
import com.gfc.users.activities.models.dto.request.ClarificationDetailsDto;
import com.gfc.users.activities.models.dto.response.ClarificationChatHistoryDto;
import com.gfc.users.activities.models.entities.Clarification;
import com.gfc.users.activities.repository.services.ClarificationRepositoryService;
import com.gfc.utils.TokenGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by gurinder on 13/2/16.
 */
@Service
public class ClarificationService {

    private EmailService emailService;

    private LoginActivityRepository loginActivityRepository;

    private ClarificationRepositoryService clarificationRepositoryService;

    @Autowired
    public ClarificationService(EmailService emailService, LoginActivityRepository loginActivityRepository, ClarificationRepositoryService clarificationRepositoryService) {
        this.emailService = emailService;
        this.loginActivityRepository = loginActivityRepository;
        this.clarificationRepositoryService = clarificationRepositoryService;
    }

    public void sendClarificationMail(ClarificationDetailsDto clarificationDetailsDto, String authToken)
            throws PersonNotFoundException, RegistrationException, EmailFailureException {
        LoginActivity currentUser = loginActivityRepository.findByAuthToken(authToken);

        if (currentUser == null) {
            throw new PersonNotFoundException(SearchStatus.INVALID_AUTH_TOKEN.getStatus());
        }

        User loggedInUser = currentUser.getPeople();

        sendEmailAndSave(clarificationDetailsDto, loggedInUser);
    }

    private void sendEmailAndSave(ClarificationDetailsDto clarificationDetailsDto, User loggedInUser) throws EmailFailureException {
        List<String> receiverEmailList = clarificationDetailsDto.getRecipientEmails();


        if (receiverEmailList != null && receiverEmailList.size() > 0) {
            Iterator<String> iterator = receiverEmailList.iterator();

            while (iterator.hasNext()) {
                String recipientEmail = iterator.next();

                if (StringUtils.isBlank(recipientEmail)) {
                    continue;
                }

                String token = TokenGenerator.getToken();
                String userName = loggedInUser.getFirstName() + " " + loggedInUser.getLastName();
                emailService.sendEmail(userName, recipientEmail, clarificationDetailsDto.getComment(), token, EmailType.CLARIFICATION);

                clarificationRepositoryService.save(clarificationDetailsDto, loggedInUser, recipientEmail, token);
            }
        }
    }

    public Long checkGuestUserToken(String token) throws PersonNotFoundException {
        return clarificationRepositoryService.checkGuestUserToken(token);
    }

    public ClarificationChatHistoryDto getChatConversationHistory(long clarificationId) {
        ClarificationChatHistoryDto chatHistory = clarificationRepositoryService.getChatConversationHistory(clarificationId);

        return chatHistory;
    }

    public void insertCommentAndExpireToken(Long clarificationId, String commentedBy, String comment) throws EmailFailureException {
        Clarification commentedByOwner = clarificationRepositoryService.isCommentedByOwner(clarificationId, commentedBy);

        if (commentedByOwner != null) {
            ClarificationDetailsDto clarificationDetailsDto = new ClarificationDetailsDto();
            clarificationDetailsDto.setSectionTitle(commentedByOwner.getSectionTitle());
            clarificationDetailsDto.setSectionId(commentedByOwner.getSectionId());
            clarificationDetailsDto.setComment(comment);
            User people = commentedByOwner.getPeople();
            ArrayList<String> recipientEmailList = new ArrayList<>();
            recipientEmailList.add(commentedByOwner.getGuestEmail());
            clarificationDetailsDto.setRecipientEmails(recipientEmailList);

            sendEmailAndSave(clarificationDetailsDto, people);
        } else {
            clarificationRepositoryService.insertCommentAndExpireToken(clarificationId, commentedBy, comment);
        }
    }

    public List<Clarification> getAllChatsByUser(Integer id) {
        List<Clarification> clarifications = clarificationRepositoryService.findByPeopleId(id);
        return clarifications;
    }
    
    public void markRead(Long clarificationId) {
    	clarificationRepositoryService.markRead(clarificationId);
    }
}
