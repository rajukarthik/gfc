package com.gfc.users.activities.mappers;

/**
 * Created by abhinav on 5/3/16.
 */
public interface Mapper {
    public Object mapRequestToEntity() throws Exception;
}
