package com.gfc.users.activities.mappers;

import com.gfc.users.accounts.exceptions.NotFoundException;
import com.gfc.users.activities.models.dto.request.RatingRequestDto;
import com.gfc.users.activities.models.entities.Rating;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by abhinav on 5/3/16.
 */
/*@Component
@Scope("prototype")*/
public class RatingsMapper implements Mapper {

    private @Setter RatingRequestDto request;
    private @Getter
    Rating rating;

    public Rating mapRequestToEntity() throws NotFoundException {
        if(rating==null){
            rating = new Rating();
        }
        this.rating.setQuestion(request.getQuestion());
        this.rating.setSectionId(request.getSectionId());
        this.rating.setUser(request.getUser());
        this.rating.setValue(request.getValue());
        return this.rating;
    }
}
