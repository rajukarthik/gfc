package com.gfc.users.activities.repository.services;

import com.gfc.users.accounts.exceptions.NotFoundException;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.services.UserRepositoryService;
import com.gfc.users.activities.mappers.RatingsMapper;
import com.gfc.users.activities.models.dto.request.RatingRequestDto;
import com.gfc.users.activities.models.entities.Rating;
import com.gfc.users.activities.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by abhinav on 5/3/16.
 */
@Service
@Transactional
public class RatingRepositoryService {

    private RatingRepository ratingRepository;
    /*private RatingsMapper mapper;*/
    private UserRepositoryService personRepositoryService;

    @Autowired
    public RatingRepositoryService(
            RatingRepository ratingRepository,
            /*RatingsMapper mapper,*/
            UserRepositoryService personRepositoryService
    ) {
        this.ratingRepository = ratingRepository;
        /*this.mapper = mapper;*/
        this.personRepositoryService = personRepositoryService;
    }

    public boolean add(RatingRequestDto request){
        try {
            User user = personRepositoryService.findById(request.getUser().getId());
            RatingsMapper mapper = new RatingsMapper();
            
            Rating rating = ratingRepository.findByUserAndQuestionAndSectionId(user, request.getQuestion(), request.getSectionId());
            if(null == rating) {
            	mapper.setRequest(request);
            	rating = mapper.mapRequestToEntity();
            	rating.setUser(user);
            } else {
            	rating.setValue(request.getValue());
            }            
            
            ratingRepository.save(rating);
            return true;
        }catch (NotFoundException e){
          return false;
        }
    }
    
    public int findRateByUserAndQuestionAndSection(int userId, String question, String sectionId){
    	int value = 0;
        try {
            User user = personRepositoryService.findById(userId);            
            Rating rating = ratingRepository.findByUserAndQuestionAndSectionId(user, question, sectionId);
            if(null != rating) {
            	value = rating.getValue();
            }
        }catch (Exception e){
        }
        return value;
    }  
    
    public List<Rating> findAll() {
    	return ratingRepository.findAll();
    }
}
