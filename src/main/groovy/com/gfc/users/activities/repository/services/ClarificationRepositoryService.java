package com.gfc.users.activities.repository.services;

import com.gfc.enums.EmailType;
import com.gfc.enums.SearchStatus;
import com.gfc.exceptions.EmailFailureException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.models.dto.ClarificationChatDto;
import com.gfc.services.EmailService;
import com.gfc.users.accounts.enums.ActivityStatus;
import com.gfc.users.accounts.models.entities.User;
import com.gfc.users.accounts.repositories.services.UserRepositoryService;
import com.gfc.users.activities.models.dto.request.ClarificationDetailsDto;
import com.gfc.users.activities.models.dto.response.ClarificationChatHistoryDto;
import com.gfc.users.activities.models.entities.Clarification;
import com.gfc.users.activities.models.entities.ClarificationChatHistory;
import com.gfc.users.activities.repositories.ClarificationChatRepository;
import com.gfc.users.activities.repositories.ClarificationRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

//import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;

/**
 * Created by gurinder on 13/2/16.
 */

@Service
public class ClarificationRepositoryService {

    private ClarificationRepository clarificationRepository;

    private ClarificationChatRepository clarificationChatRepository;

    private UserRepositoryService userRepositoryService;

    private EmailService emailService;
    
    @Autowired
    public ClarificationRepositoryService(
            ClarificationRepository clarificationRepository,
            ClarificationChatRepository clarificationChatRepository,
            UserRepositoryService userRepositoryService, EmailService emailService) {
        this.clarificationRepository = clarificationRepository;
        this.clarificationChatRepository = clarificationChatRepository;
        this.userRepositoryService = userRepositoryService;
        this.emailService = emailService;
    }

    public void save(ClarificationDetailsDto clarificationDetailsDto, User user, String guestEmail, String token) {

        Clarification clarification =
                clarificationRepository.findBySectionIdAndPeopleAndGuestEmail(clarificationDetailsDto.getSectionId(),
                        user, guestEmail);

        if (clarification == null) {
            clarification = new Clarification();
            clarification.setStatus(ActivityStatus.ACTIVE);
            clarification.setPeople(user);
            clarification.setGuestEmail(guestEmail);
            clarification.setSectionId(clarificationDetailsDto.getSectionId());
            clarification.setSectionTitle(clarificationDetailsDto.getSectionTitle());
            clarification.setGuestToken(token);
            clarification.setQuestion(clarificationDetailsDto.getComment());
            clarification.setRead(true);
        } else {
            clarification.setGuestToken(token);
            clarification.setStatus(ActivityStatus.ACTIVE);
        }

        Clarification clarifyEntity = clarificationRepository.save(clarification);

        ClarificationChatHistory clarificationChatHistory = new ClarificationChatHistory();
        clarificationChatHistory.setClarificationId(clarifyEntity);
        clarificationChatHistory.setComment(clarificationDetailsDto.getComment());
        clarificationChatHistory.setUserName(user.getEmail());        
        clarificationChatRepository.save(clarificationChatHistory);

        List<ClarificationChatHistory> chats = clarifyEntity.getClarificationChatList();
        if(chats == null){
            chats = Lists.newArrayList();
        }
        chats.add(clarificationChatHistory);
        clarificationRepository.save(clarifyEntity);
    }

    public Long checkGuestUserToken(String token) throws PersonNotFoundException {
        Clarification clarification = clarificationRepository.findByGuestTokenAndStatus(token, ActivityStatus.ACTIVE);
        boolean flag = clarification != null ? (clarification.getStatus().equals(ActivityStatus.ACTIVE) ? true : false) : false;

        if (flag) {
            return clarification.getId();
        } else {
            throw new PersonNotFoundException(SearchStatus.INVALID_AUTH_TOKEN.getStatus());
        }
    }

    public ClarificationChatHistoryDto getChatConversationHistory(long clarificationId) {
        Clarification clarification = clarificationRepository.findById(clarificationId);
        List<ClarificationChatHistory> clarificationChatEntities = clarificationChatRepository.findByClarificationIdOrderByCreatedDtAsc(clarification);

        ClarificationChatHistoryDto chatHistoryDto = new ClarificationChatHistoryDto();
        chatHistoryDto.setClarificationId(clarification.getId());
        chatHistoryDto.setSectionId(clarification.getSectionId());
        chatHistoryDto.setSectionTitle(clarification.getSectionTitle());
        chatHistoryDto.setUserName(clarification.getPeople().getEmail());
        chatHistoryDto.setGuestUserName(clarification.getGuestEmail());

        List<ClarificationChatDto> chatEntityList = new ArrayList<ClarificationChatDto>();
        Iterator<ClarificationChatHistory> iterator = clarificationChatEntities.iterator();

        while (iterator.hasNext()) {
            ClarificationChatHistory clarificationChatHistory = iterator.next();
            ClarificationChatDto clarificationChatDto = new ClarificationChatDto();
            clarificationChatDto.setChatId(clarificationChatHistory.getId());
            clarificationChatDto.setComment(clarificationChatHistory.getComment());
            clarificationChatDto.setUserName(clarificationChatHistory.getUserName());
            chatEntityList.add(clarificationChatDto);
        }

        chatHistoryDto.setClarificationChats(chatEntityList);

        return chatHistoryDto;
    }

    public Clarification isCommentedByOwner(Long clarificationId, String userName) {
        Clarification clarification = clarificationRepository.findById(clarificationId);
        User user = clarification.getPeople();

        if (userName.equalsIgnoreCase(user.getEmail())) {
            return clarification;
        } else {
            return null;
        }
    }

    public void insertCommentAndExpireToken(Long clarificationId, String commentedBy, String comment) throws EmailFailureException {
        Clarification clarification = clarificationRepository.findById(clarificationId);

        ClarificationChatHistory clarificationChatHistory = new ClarificationChatHistory();
        clarificationChatHistory.setClarificationId(clarification);
        clarificationChatHistory.setComment(comment);
        clarificationChatHistory.setUserName(commentedBy);        

        clarificationChatRepository.save(clarificationChatHistory);

        clarification.setStatus(ActivityStatus.INACTIVE);
        clarification.setRead(false);
        clarification.setUpdatedDt(new Date());
        clarificationRepository.save(clarification);
        
        emailService.sendEmail(clarification.getPeople().getFirstName() + " " + clarification.getPeople().getLastName(), clarification.getPeople().getEmail(), comment, "", EmailType.CLARIFICATION_FEEDBACK);
    }

    @Transactional(readOnly = true)
    public List<Clarification> findByPeopleId(Integer userId){
      User user = userRepositoryService.findById(userId);
      List<Clarification> clarifications = clarificationRepository.findByPeopleOrderByUpdatedDtDesc(user);
      return clarifications;
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public void markRead(Long clarificationId) {
    	Clarification clarification = clarificationRepository.findOne(clarificationId);
    	clarification.setRead(true);
    	//clarification.getClarificationChatList().stream().forEach(chat -> chat.setRead(true));
    	clarificationRepository.save(clarification);
    }
}
