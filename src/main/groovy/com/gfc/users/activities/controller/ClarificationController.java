package com.gfc.users.activities.controller;

import com.gfc.enums.ResponseStatus;
import com.gfc.exceptions.EmailFailureException;
import com.gfc.exceptions.HistoryNotFoundException;
import com.gfc.exceptions.PersonNotFoundException;
import com.gfc.exceptions.RegistrationException;
import com.gfc.models.dto.MessageDto;
import com.gfc.users.activities.models.dto.request.ClarificationDetailsDto;
import com.gfc.users.activities.models.dto.response.ClarificationChatHistoryDto;
import com.gfc.users.activities.models.entities.Clarification;
import com.gfc.users.activities.services.ClarificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by gurinder on 13/2/16.
 */
@RequestMapping("/api")
@RestController
public class ClarificationController {

    private ClarificationService clarificationService;

    @Autowired
    public ClarificationController(ClarificationService clarificationService) {
        this.clarificationService = clarificationService;
    }

    @RequestMapping(value = "/secure/user/clarification/send", method = RequestMethod.POST)
    public ResponseEntity clarify(@RequestBody ClarificationDetailsDto clarificationDto,
                                  @RequestHeader("Auth-Token") String authToken) throws HistoryNotFoundException {
        try {
            clarificationService.sendClarificationMail(clarificationDto, authToken);
        }catch (EmailFailureException | PersonNotFoundException | RegistrationException e){
            return new ResponseEntity(e.getMessage(),HttpStatus.EXPECTATION_FAILED);
        }

        return new ResponseEntity(ResponseStatus.SUCCESS,HttpStatus.OK);
    }

    @RequestMapping(value = "/user/clarification/guest/chat/history", method = RequestMethod.GET)
    public ResponseEntity guestChatConversationHistory(@RequestParam(value = "token", required = true) String token) throws IOException {
        ResponseEntity responseEntity = null;

        try {
            Long clarificationId = clarificationService.checkGuestUserToken(token);
            responseEntity = chatConversationHistory(clarificationId);
        } catch (PersonNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }

        return responseEntity;
    }

    @RequestMapping(value = "/secure/user/clarification/chat/history", method = RequestMethod.GET)
    public ResponseEntity userChatConversationHistory(@RequestHeader("Auth-Token") String authToken,
                                                  @RequestParam(value = "clarificationId", required = true) long clarificationId) throws IOException {
        return chatConversationHistory(clarificationId);
    }

    private ResponseEntity chatConversationHistory(@RequestParam(value = "clarificationId", required = true) long clarificationId) {
        ClarificationChatHistoryDto chatHistory = clarificationService.getChatConversationHistory(clarificationId);

        if (chatHistory != null) {
            return new ResponseEntity(chatHistory, HttpStatus.ACCEPTED);
        } else {
            MessageDto messageDto = new MessageDto("No chat history found", ResponseStatus.INVALID_REQUEST);
            return new ResponseEntity(messageDto, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/user/clarification/comment", method = RequestMethod.POST)
    public ResponseEntity comment(@RequestBody ClarificationDetailsDto clarificationDto) {
        MessageDto messageDto = null;

        try {
            clarificationService.insertCommentAndExpireToken(clarificationDto.getClarificationId(),
                    clarificationDto.getCommentedBy(), clarificationDto.getComment());

            messageDto = new MessageDto("Thanks for commenting", ResponseStatus.SUCCESS);
        } catch (EmailFailureException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }

        return new ResponseEntity(messageDto, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/secure/user/{id}/clarification/chat/all", method = RequestMethod.GET)
    public ResponseEntity getAllChatsByUser(@PathVariable("id") Integer id){
        List<Clarification> clarifications = null;
        try {
            clarifications = clarificationService.getAllChatsByUser(id);
        }catch (Exception e){
           e.printStackTrace();
            return new ResponseEntity(clarifications, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(clarifications, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/secure/user/clarification/{clarificationId}", method = RequestMethod.PUT)
    public ResponseEntity markRead(@PathVariable("clarificationId") Long clarificationId){
        try {
        	clarificationService.markRead(clarificationId);
        }catch (Exception e){
           e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(ResponseStatus.SUCCESS,HttpStatus.OK);
    }    
}