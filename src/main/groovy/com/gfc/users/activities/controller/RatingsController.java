package com.gfc.users.activities.controller;

import com.gfc.users.accounts.models.dto.response.ResponseDto;
import com.gfc.users.activities.models.dto.request.RatingRequestDto;
import com.gfc.users.activities.models.dto.response.RatingResponseDto;
import com.gfc.users.activities.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * Created by abhinav on 5/3/16.
 */
@RestController
@RequestMapping("/api/secure/search/answer")
public class RatingsController {

    private RatingService ratingService;

    @Autowired
    public RatingsController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @RequestMapping(value = "/ratings", method = RequestMethod.POST)
    public ResponseEntity ratings(@RequestBody @NotNull RatingRequestDto request){
        ResponseDto response = ratingService.rate(request);
        return new ResponseEntity(response,response.getHttpStatus());
    }
    
    @RequestMapping(value = "/ratings", method = RequestMethod.GET)
    public ResponseEntity list(	@RequestParam(value = "userId", required = true) int userId, 
					    		@RequestParam(value = "question", required = true) String question,
					    		@RequestParam(value = "sectionId", required = true) String sectionId){
	    	RatingResponseDto response = ratingService.findRateByUserAndQuestionAndSection(userId, question, sectionId);
        return new ResponseEntity(response, response.getHttpStatus());
    }    
}
