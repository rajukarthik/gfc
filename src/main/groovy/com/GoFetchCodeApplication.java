package com;

import com.gfc.filters.AuthenticationAdvice;
import freemarker.template.utility.XmlEscape;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.web.context.WebApplicationContext;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

//import org.flywaydb.core.Flyway;

@SpringBootApplication
@EnableFeignClients
@Configuration
@EnableAutoConfiguration
@ComponentScan({"com"})
@EnableScheduling
@EnableJpaRepositories({"com"})
@EnableAspectJAutoProxy
public class GoFetchCodeApplication extends SpringBootServletInitializer {

	@Value("${mail.smtp.encoding}")
	private String encoding;
		 
	@Value("${mail.smtp.host}")
	private String host;
	
	@Value("${mail.smtp.port}")
	private String port;
	
	@Value("${mail.smtp.from}")
	private String fromAddress;
	
	@Value("${mail.smtp.replyTo}")
	private String replyTo;
	
	@Value("${mail.smtp.username}")
	private String username;
	
	@Value("${mail.smtp.password}")
	private String password;


	/**
	 * This method is used to load message properties.
	 * @return
	 */
    @Bean(name = "messageSource")
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageBundle
                = new ReloadableResourceBundleMessageSource();
        messageBundle.setBasename("classpath:messages/messages");
        messageBundle.setDefaultEncoding("UTF-8");
        return messageBundle;
    }


    public static void main(String[] args) {
        SpringApplication.run(GoFetchCodeApplication.class, new String[]{"-debug"});
    }

    /**
     * This method is used to create a mail session based on SMTP server details.
     * @return
     */
    @Bean
    public Session mappingMailDetails() {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.from", fromAddress);
        properties.put("mail.smtp.starttls.enable","true");
        properties.put("mail.smtp.auth", "true");
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
            	return new PasswordAuthentication(username, password);
            }
        });
        return session;
    }    

    /**
     * This method is used to create a flyway bean for automatic SQL scripts migrations.
     * @param theDataSource
     * @return
     */
    @Bean
    public Flyway flyway(DataSource theDataSource) {
        Flyway flyway = new Flyway();
        flyway.setValidateOnMigrate(true);
        flyway.setDataSource(theDataSource);
        flyway.setLocations("classpath:db/migration");
//        flyway.clean();
        flyway.migrate();
        return flyway;
    }

    private volatile static WebApplicationContext webApplicationContext;

    @Override
    protected WebApplicationContext createRootApplicationContext(ServletContext servletContext) {
        webApplicationContext = super.createRootApplicationContext(servletContext);
        return webApplicationContext;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public FilterRegistrationBean urlFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(urlFilter());
        registration.addUrlPatterns("/*");
        registration.addInitParameter("paramName", "paramValue");
        registration.setName("urlFilter");
        return registration;
    }

    @Bean(name = "urlFilter")
    public Filter urlFilter() {
        return new AuthenticationAdvice();
    }
    
    /**
     * This method is used to create Freemarker factory bean for using freemarker templates.
     * @return
     */
    @Bean
	public FreeMarkerConfigurationFactoryBean freemarkerConfiguration() {
    	FreeMarkerConfigurationFactoryBean configurer = new FreeMarkerConfigurationFactoryBean();
		  configurer.setTemplateLoaderPath("classpath:templates" + File.pathSeparator + "ftl");
		  Map<String, Object> map = new HashMap<>();
		  map.put("xml_escape", new XmlEscape());
		  configurer.setFreemarkerVariables(map);
	  return configurer;
	}    
}