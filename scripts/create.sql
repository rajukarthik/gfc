DROP database gfc;

CREATE database gfc;

GRANT ALL ON gfc.* TO 'gfc'@'%' IDENTIFIED BY 'gfc';

GRANT ALTER ROUTINE, ALTER ROUTINE, EXECUTE ON gfc.* TO gfc@localhost IDENTIFIED BY 'G0f3TcH$Code';

USE gfc;



CREATE TABLE gfc_users(
id INT NOT NULL AUTO_INCREMENT,
created_by VARCHAR(100),
created_dt DATE,
updated_by VARCHAR(100),
updated_dt DATE,
email VARCHAR(100) NOT NULL,
first_name VARCHAR(100) NOT NULL,
last_name VARCHAR(100) NOT NULL,
password VARCHAR(100) NOT NULL,
status int(11),
type int(11),
isDeleted VARCHAR(100) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE gfc_organisations(
id INT NOT NULL AUTO_INCREMENT,
created_by VARCHAR(100),
created_dt DATE,
updated_by VARCHAR(100),
updated_dt DATE,
name VARCHAR(100) NOT NULL,
owner_id int(11),
PRIMARY KEY (id)
);

CREATE TABLE gfc_roles(
id INT NOT NULL AUTO_INCREMENT,
created_by VARCHAR(100),
created_dt DATE,
updated_by VARCHAR(100),
updated_dt DATE,
code VARCHAR(100) NOT NULL,
description VARCHAR(100) NOT NULL,
status VARCHAR(100) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE gfc_teams(
id INT NOT NULL AUTO_INCREMENT,
created_by VARCHAR(100),
created_dt DATE,
updated_by VARCHAR(100),
updated_dt DATE,
name VARCHAR(100) NOT NULL,
type VARCHAR(100) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE gfc_users_teams_roles(
id INT NOT NULL AUTO_INCREMENT,
created_by VARCHAR(100),
created_dt DATE,
updated_by VARCHAR(100),
updated_dt DATE,
auth_token VARCHAR(100) NOT NULL,
activation_status VARCHAR(100) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE gfc_users_activation(
user_id INT NOT NULL,
team_id INT NOT NULL,
role_id INT ,
created_by VARCHAR(100),
created_dt DATE,
updated_by VARCHAR(100),
updated_dt DATE,
PRIMARY KEY (user_id,team_id)
);


