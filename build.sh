#!/usr/bin/env bash

./gradlew clean minBuild \
&& find build/unpacked/lib/ | xargs touch -t 0000000000.00 \
&& docker build -t asheibani/gfc . \
&& docker push asheibani/gfc